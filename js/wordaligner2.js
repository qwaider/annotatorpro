

var down = null;
var WHITE = "";
var COLOR = "red";


$('div').mousedown( function () {
	if (this.id != "" && this.id.indexOf(".") > 0) {
		down = this.id;
	}
});
    
$('div').mouseup( function (event) {
	if (event.which==3 || (event.ctrlKey && event.which==1)) {
		//alert("OUTPUTID "+OUTPUTID+ ", "+isSelected(OUTPUTID) +", "+isSelected("s"+OUTPUTID));
		if (OUTPUTID != null && isSelected(OUTPUTID) == 1 && isSelected("s"+OUTPUTID) == 1) {
			moveObject('errortypes',event); 
			$("#errortypes").show(100);
			return false;
		}
	}
	
	if (this.id != "") {
	  if (this.id.indexOf(".") > 0) {
		var up = this.id;
		//$("#log").html("SET " +down + " " +up + " "+OUTPUTID);
		if (down != null && up != null) {
			var changed;
			var sentid = down.replace(/\..+/g,"");
			var sentidup = up.replace(/\..+/g,"");
			
 			//check if the annotation is done within a sentence
			if (sentid != sentidup) {
				return;
			}
			
			down = down.replace(/.+\./g,"");
			up = up.replace(/.+\./g,"");
			
			if (parseInt(down) >= parseInt(up)) {
				var tmpdown = up;
				up = down;
				down = tmpdown;
			}
			
			changed = selectTokens(sentid, down, up); 
			if (changed) {
    			showAction(sentid, event);
			} 
	    	clearSelectedText();
    		down = null;
    		up = null;
		}
	  }
    }
});


function showAction(id, event) {
	id = id.replace(/^s/,"");
	if (OUTPUTID != null && OUTPUTID != id) {
		cleanBgColor(OUTPUTID);
		cleanBgColor("s"+OUTPUTID);
	}
	OUTPUTID = id;
			
    //moveObject('errortypes',event); 
    return false;
}

function cleanBgColor (sentid) {
	i=1;
	while (true) {
		var el = document.getElementById(sentid+"."+i);
    	if (el != null) {
    		el.style.backgroundColor = WHITE;
    	} else {
    		break;
    	}
    	//check spaces
		//el = document.getElementById(sentid+"."+i+"-"+(i+ 1));
		//if (el != null) {
		//	el.style.backgroundColor = WHITE;
		//}
    	i++;	
	}
}

function cleanAnnotation (sentid,errid) {
	var i=1;
	while (true) {
		//source
		var el = document.getElementById("bs"+sentid+"."+i);
		if (el != null) {
			if (errid == 1) {
				el.style.borderTop = "";
			} else {
				el.style.borderBottom = "";
			}
		} else {
    		break;
    	}
    	i++;
    }
    
    i=1;
	while (true) {				
		//target
		el = document.getElementById("b"+sentid+"."+i);
		if (el != null) {
			if (errid == 1) {
				el.style.borderTop = "";
			} else {
				el.style.borderBottom = "";
			}
		} else {
    		break;
    	}
    	i++;
    }				
}

function selectTokens (sentid, down, up) {
    color = null;
    var changed = false;
	var askedconfirm = false;
	//check the spaces
	if (down.indexOf("-") >0 && down==up) {
		//do nothing on space???
	} else {		
	    for (var i=parseInt(down); i<=parseInt(up); i++) {
    		var el = document.getElementById(sentid+"."+i);
    		//alert(i + " " + up+"/"+down +"  " +el);
    		if (el != null) {
    			if (COLOR == WHITE && el.style.backgroundColor == COLOR) {
    				continue;
    			}
    			changed = true;
    			if (color == null) {
    				if (el.style.backgroundColor == COLOR) {
    					/*
    					if (!askedconfirm) {
    						askedconfirm = true;
							if (confirm("ATTENTION! Do you want to REMOVE this annotation?")) {
								//continue;
							} else {
								return false;
							}
						}
						*/
						color = WHITE;
    				} else {
    					color = COLOR;
    				}
	    		}
	    		
	    		/*
	    		var allowSelection=1;
	    		var bel = document.getElementById("b"+sentid+"."+i);
    			if (bel != null) {
    				if (bel.style.borderBottom != "" && bel.style.borderTop != "") {
    					allowSelection=0;
    				}
    			}
    			*/
    			//alert(HASH_ANNOTATION[sentid+"."+i]);		
    			//alert(i + " " +bcolor +" (" +el.style.backgroundColor +") " + color);    			
    			//if (allowSelection == 1) {
    			//cancello tutti tokens precedenti selezionati (ce ne deve essere solo 1)
				//cleanBgColor(sentid);
 			
 					el.style.backgroundColor = color;
    			//} 
    		}
    		
    	}

    	if (color==WHITE) {
			el = document.getElementById(sentid+"."+(parseInt(down)-1)+"-"+down);
    		if (el != null) {
    			el.style.backgroundColor = color;   		
    		} 
	    }
	}
	return changed;
}

function clearSelectedText() {
	if (window.getSelection) {
		window.getSelection().removeAllRanges();
	} else if (document.getSelection) {
		document.getSelection().empty();
	} else {
		var selection = document.selection && document.selection.createRange();
		if (selection.text) {
			selection.collapse(true);
			selection.select();
		}
	}
}
