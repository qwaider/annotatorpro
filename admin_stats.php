<style>
.tbl {height: 7px; 
	border-top: 1px solid #000; 
	width: 100%}
.tokend {display: inline-block; 
		font-size: 16px; 
		border-top: 2px solid #999; 
		border-bottom: 1px solid #000; 
		border-left: 1px solid #000; 
		border-right: 1px solid #000; 
		margin-bottom: 1px}
		
.tok {display: inline-block; 
		font-size: 16px; 
		border-top: 2px solid #999; 
		border-bottom: 1px solid #000; 
		border-left: 1px solid #000; 
		margin-bottom: 1px}
</style>
<script>
function getAgreement(taskid) {
	$('.spinner').show();
	$.ajax({
		url: 'agreement.php',
		type: 'GET',
		data: {id: taskid},
		async: true,
		cache:false,
		crossDomain: false,
		success: function(response) {
			$('.spinner').hide();
			$("#agreement").html(response);
			
		},
  		error: function(response, xhr,err ) {
			alert("WARNING! An error occured on the server.");
		}
  	});
}
</script>
<div style='margin: 10px; padding-bottom: 30px; vertical-align: top; top: 0px; display:block'>
<form action="admin.php?section=stats" method=GET>
<input type=hidden name=section value="stats" />
Choose a task: <select onChange="submit()" name='id'><option value=''>
<?php
if (isset($show_disagreement)) {
        $mysession["show_disagreement"] = $show_disagreement;
        $_SESSION["mysession"] = $mysession;
}

$show_disagreement=0;
if (isset($mysession)) { 
	if (isset($mysession["show_disagreement"])) {
		$show_disagreement=$mysession["show_disagreement"];
	}
	$tasks = getTasks($mysession["userid"]);
	
	$ttype = "";
	while (list ($tid,$tarr) = each($tasks)) {
		if ($tarr[1] != $ttype) {
			$ttype = $tarr[1];
			print "<option value='' disabled='disabled'>--- ".$taskTypes[$ttype] ." tasks --- \n";
		}
		print "<option value='$tid'";
		if (isset($id) && $id == $tid) {
			print " selected";
		} 
		print "> &nbsp;".$tarr[0]."\n";
	}	
}
?>
</select>
</form>

<?php
$wordaligner_color = array(
"04B404", "0080FF","DBA901","DF013A","BF00FF",
"04B404", "0080FF","DBA901","DF013A","BF00FF");
$wordaligner_lightcolor =array(
"01DF01","81BEF7","F7D358","F7819F","D0A9F5",
"01DF01","81BEF7","F7D358","F7819F","D0A9F5");

$anntot=array();
$annid="";
$annotators=array();
$statsTable = "";
$done=0;
if (isset($id)) {
	$taskinfo = getTaskInfo($id);
	
	if (count($taskinfo) > 0) {
		$hash_report = getAnnotationReport($id);
		$systems = array();
		$average = array();
		$statsTable = "<table cellspacing=1 cellpadding=0 border=0><tr bgcolor=#ccc><td align=center>Annotation type</td><td colspan=4 align=center>Task Corpus</td></tr><tr><td></td><td bgcolor='#000'><img width=1></td>";
		while (list ($eval,$counters) = each($hash_report)) {
			$values = explode(",", $counters);
			foreach ($values as $val) {
				$items = explode(" ", $val);
				if (!array_key_exists($items[1],$annotators)) {
					$userinfo = getUserInfo($items[1]);
					$annotators[$items[1]] = $userinfo["username"];
				}
				if (!isset($systems[$items[0]])) {
					$systems[$items[0]]=0;
					//$statsTable .= "<th>&nbsp;&nbsp;".$items[0]."</th>";
					$statsTable .= "<th>&nbsp;&nbsp;Annotations"."</th>";

				}

				if (isset($anntot[$eval."-".$items[0]])) {
					$anntot[$eval."-".$items[0]] += $items[2];
				} else {
					$anntot[$eval."-".$items[0]] = $items[2];
				}
				if ($taskinfo["type"] == "quality" && is_numeric($eval)) {
					if (isset($average[$items[0]])) {
						$average[$items[0]] += $items[2]*$eval;
					} else {
						$average[$items[0]] = $items[2]*$eval;
					}
				}
				$systems[$items[0]] += $items[2];				
			}							
		}
		
		$statsTable .= "</tr>\n";
		$statsTable .= "<tr><td></td><td colspan=".(count($systems)+1)." height=1 bgcolor='#000'><img width=1></td></tr>";
						
		reset($hash_report);
		$colorValue=array();
		$ranges = rangesJson2Array($taskinfo["ranges"]);
		while (list ($evalid, $attrs) = each($ranges)) {
			$statsTable .= "<tr align=right><th bgcolor='".$attrs[1]."'>".$attrs[0]."&nbsp;&nbsp;</th><td bgcolor='#000'><img width=1></td>\n";
			$colorValue[$attrs[1]]=$attrs[0];
			reset($systems);
			while (list ($system,$tot) = each($systems)) {
				$statsTable .= "<td>";
				if (isset($anntot[$evalid."-".$system])) {
					$counters = $hash_report[$evalid];
					$values = explode(",", $counters);
					$annotationdetail = "";
					foreach ($values as $val) {
						$items = explode(" ", $val);
						if ($items[0] == "$system") {
							$annotationdetail .= $annotators[$items[1]].": ".$items[2]." annotations\n";
						}
					}
					$statsTable .= "&nbsp;&nbsp;".$anntot[$evalid."-".$system]."|<img title='".$annotationdetail."' width=12 src='img/user.png' border=0></td>";
				} else {
					$statsTable .= "-&nbsp;</td>";
				}
			}
			$statsTable .= "</tr>";
		}
	
		#sum
		$statsTable .= "<tr><td colspan=".(count($systems)+2)." height=1 bgcolor='#000'><img width=1></td></tr>";
		$statsTable .= "<tr align=right><td align=right>Total:</td><td width=1 bgcolor='#000'><img width=1></td>";
		reset($systems);
		while (list ($system, $tot) = each($systems)) {
			$statsTable .= "<th>&nbsp;$tot&nbsp;&nbsp;&nbsp;&nbsp;</th>";
		}	
		$statsTable .= "</tr>";
	
		#average
		if ($taskinfo["type"] == "quality" && count($average) > 0) {
			$statsTable .= "<tr><td colspan=".(count($systems)+2)." height=1 bgcolor='#666'><img width=1></td></tr>";
			$statsTable .= "<tr bgcolor=#fefefe style='display: none;' align=right><td>Average:</td><td bgcolor='#000'><img width=1></td>";
			while (list ($sysname,$counters) = each($average)) {
				if (isset($systems[$sysname]) && $systems[$sysname] > 0) {
					$statsTable .= "<td>&nbsp;".round($average[$sysname]/$systems[$sysname],2) ."&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				}
			}
			$statsTable .= "</tr>";
		}
		
		$statsTable .= "</table>";	
		if(count($annotators)>0){
		print "<strong>&nbsp;&nbsp;This task has been annotated by <b>".count($annotators) ."</b> users</strong></br>";
		}else{
		print "<strong>&nbsp;&nbsp;This task has been annotated by <b>".count($annotators) ."</b> users</strong></br>";
		}
		if (count($annotators) > 0) {	
			$currentRow="";
			$sentid = "";
			$sourceid="";
			$userid="";
			$user_agreements="";
			$annotation_data=array();
			$user_annotations=array();
			$aligned_source_tokens=array();
			$word_alignment=array();
			$annotated_sentences = getAgreementSentences($id);
			$source_sentences = getSourceSentences($id);
			$outputNum = 0;
			$count=0;
			$num=0;
			$max=100;
			if (!isset($from)) {
				$from=1;
			} else {
				if ($from < 0) {
					$from = 1;
				}
			}
			
			$outputText = "";
			$tokens = array();
			#$hashusers = getUserStats($mysession["userid"],$mysession["status"]);
			
			while ($row = mysql_fetch_array($annotated_sentences)) {
				if (isDone($row["linkto"],$row["user_id"]) != 1) {
					continue;
				}
				$done++;
				//print "ID: ".$row["output_id"].", linkTo: ".$row["linkto"]. " - user: " .$row["user_id"]. "==".isDone($row["linkto"],$row["user_id"]). " --- ev: ".$row["eval"]. ", evalids:". $row["evalids"]."<br>";
				
				
				if ($currentRow == "") {
					$currentRow = $row["user_id"]."-".$row["output_id"];
					$userid = $row["user_id"];
				} else if ($currentRow != $row["user_id"]."-".$row["output_id"]) {
					//print row							
					if (count($user_annotations) > 0) {
						if ($taskinfo["type"] == "wordaligner") {
							$annotation_data{$userid} = $user_annotations;
							$aligned_source_tokens{$userid} = $word_alignment;
						} else {
							$auser = "<tr height=3>";
							for ($i=0; $i < count($tokens); $i++) {
								if (isset($user_annotations["-1"])) {
									//$auser .= "<td><table width=100% border=0 cellspacing=0 cellpadding=2><td bgcolor=".$user_annotations["-1"]." title='".$annotators[$items[1]].": ".$colorValue[$user_annotations["-1"]]."'></td></table>";
									$auser .= "<td><table width=100% border=0 cellspacing=0 cellpadding=2><td bgcolor=".$user_annotations["-1"]." title='".$annotators[$userid].": ".$colorValue[$user_annotations["-1"]]."'>".$annotators[$userid]."</td></table>";
								} else if (isset($user_annotations[$i+1])) {
									#$auser .= "<td bgcolor=".$user_annotations[$i+1]."></td>";
									$auser .= "<td><table width=100% border=0 cellspacing=0 cellpadding=2>";
									$cols = explode(" ", trim($user_annotations[$i+1]));
									foreach ($cols as $col) {
										$auser .= "<td bgcolor=".$col." title='".$annotators[$userid].": ". $colorValue[$col]."'></td>";
									}
									$auser .= "</table></td>";
								} else {
									$auser .= "<td></td>";
								}
							}
							$user_agreements .= $auser ."</tr>\n";
						}
					}
					$user_annotations=array();
					$word_alignment=array();
					$currentRow = $row["user_id"]."-".$row["output_id"];
					$userid = $row["user_id"];					
				} 
				
				// start a new source
				if ($sourceid != $row["linkto"]) {
					
					$outputNum = 0;
					$num++;
					if ($num >= $from) {
						if ($num > ($from + $max -1)) {
							$num++;
							break;
						}
						//$sourceid = $row["linkto"];							
					} else {
						$sourceid = $row["linkto"];
						continue;
					}
					
					if ($user_agreements != "") {
						//print user annotation for a previuos source
						$outputText .= "\n$user_agreements\n</table><br>";
						$user_agreements = "";
					}
					
					if ($taskinfo["type"] == "wordaligner") {
						//print collected data
						if (count($annotation_data) > 0) {
							for ($i=0; $i < count($tokens); $i++) {
								if ($i>=count($tokens)-1) {
									$outputText .= "<div class=tokend>";
								} else {
									$outputText .= "<div class=tok>";
								}	
								$outputText .= "&nbsp;".$tokens[$i]."&nbsp;";
								reset($annotation_data);
								while (list ($uid,$annotations) = each($annotation_data)) {
									//print "User: $uid<br>";
									//$outputText .= '<table cellspacing=0 cellpadding=0 class=tbl><td bgcolor=red></td></table><table cellspacing=0 cellpadding=0 class=tbl><td bgcolor=#ccc>&nbsp;d</td><td>&nbsp;ds</td></table><table cellspacing=0 cellpadding=0 class=tbl><td>sdfsdf</td></table>';
									$outputText .= "<table cellspacing=0 cellpadding=0 class=tbl";
									if (isset($annotations["-1"])) {
										$outputText .= "><td bgcolor=".$annotations["-1"]." title='".$annotators[$uid].": ".$colorValue[$annotations["-1"]]."'></td>";
									} else if (isset($annotations[$i+1])) {
										$outputText .= " onmouseover=\"javascript:colorSourceTokens('$sourceid','".$aligned_source_tokens{$uid}[$i+1]."');\" onmouseout=\"javascript:cleanSourceBackground('$sourceid',". count($source_tokens). ");\">";
										
										$cols = explode(" ", trim($annotations[$i+1]));
										foreach ($cols as $col) {
											$outputText .= "<td bgcolor=".$col." title='".$annotators[$uid].": ";
											if (in_array($col, $wordaligner_color)) {
												$outputText .= "Sure alignment";
											} else if (in_array($col, $wordaligner_lightcolor)) {
												$outputText .= "Possible alignment";
											}
											$outputText .= "'></td>";
										}
										
									} else {
										$outputText .= "><td bgcolor=#fff></td>";
									}								
									$outputText .= "</table>";
									
								}
								$outputText .= "</div>";
							}
							$annotation_data = array();
						}
					}
					
					
					$outputText .= "<hr><a href='".$taskinfo["type"].".php?id=".$row["linkto"]."&taskid=".$id ."'><li><i>document id: ".$source_sentences[$row["linkto"]][3]." - document name: ".$row["id"]." - DB num: ".$row["sentence_num"]. "</i></a><br>";	
					$sourceid = $row["linkto"];	
						
					if ($taskinfo["type"] == "wordaligner") {												
						$source_tokens = getTokens($source_sentences[$sourceid][0], 			$source_sentences[$sourceid][1], $row["tokenization"]);
						$outputText .= "<div style='margin: 0px; background: #ddd; padding: 0px'>SOURCE:</div>";
						$t=0;
						foreach ($source_tokens as $tkn) {
							$outputText .= "<div id='s.$sourceid.$t' ";
							if ($t>=count($source_tokens)-1) {
								$outputText .= "class=tokend>";
							} else {
								$outputText .= "class=tok>";
							}
							$outputText .= "&nbsp;$tkn&nbsp;<div style='border-top: 1px solid #999; background: #{$wordaligner_color[($t++ % 10)]}; padding:0px; height: 3px'></div></div>";
						}
						$outputText .= "<p>";
					}
				} 
				
				if ($sentid != $row["output_id"] && $num >= $from) {
					if ($user_agreements != "") {
						$outputText .= $user_agreements . "\n</table><br>";						
					}
					
					$outputNum++;
					$sentid = $row["output_id"];
					$user_agreements ="";
					if ($num>=$from) {
						$tokens = getTokens($row["lang"], $row["text"], $row["tokenization"]);
						if ($taskinfo["type"] == "wordaligner") {
							$outputText .= "<div style='margin: 0px; padding: 0px'>OUTPUT $outputNum:</div>";
						} else {
							$outputText .= "OUTPUT $outputNum:<br><table cellspacing=0 cellpadding=0 border=1><tr bgcolor=#fff><td style='padding: 1px'>".join("</td><td style='padding: 2px'>", $tokens)."</td></tr>\n";
						}
					}
				}
				
										
				if (trim($row["evalids"]) == "") {
					$user_annotations["-1"]=$ranges[$row["eval"]][1];
				} else {
					$tokenids = preg_split("[ |,]", trim($row["evalids"]));
					if (count($tokenids) > 0) {
						foreach ($tokenids as $tid) {
							if (strpos($tid,'-') !== false) {
								if ($taskinfo["type"] == "wordaligner") {
									$sid = preg_replace('/-.*$/',"",$tid) - 1;
									$tid = preg_replace('/^.*-/',"",$tid);
									 
								} else {
									$tid = preg_replace('/-.*$/',"",$tid); 
								}
							}
						
							#print  $sentid  ."# " .$tid ." ## ". $ranges[$row["eval"]][0] ."<br>";	
							if ($taskinfo["type"] == "wordaligner") {
								$color = "";
								if ($row["eval"] == 1) {
									$color = $wordaligner_color[($sid % 10)];
								} else {
									$color = $wordaligner_lightcolor[($sid % 10)];
								}
								if (!isset($user_annotations[$tid])) {
									$user_annotations[$tid]=$color;
									$word_alignment[$tid]=$sid;
								} else {					
									$user_annotations[$tid].=" ".$color;
									$word_alignment[$tid].=" ".$sid;
								}								
							} else {
								if (!isset($user_annotations[$tid])) {
									$user_annotations[$tid]=$ranges[$row["eval"]][1];
								} else {					
									$user_annotations[$tid].=" ".$ranges[$row["eval"]][1];
								}
							}
						}
					}
				} 
			}
			//end while
		
		
			if ($taskinfo["type"] == "wordaligner") {
				//print collected data
				if (!isset($annotation_data{$userid})) {
					$annotation_data{$userid} = $user_annotations;
					$aligned_source_tokens{$userid} = $word_alignment;
				}
				if (count($annotation_data) > 0) {
					for ($i=0; $i < count($tokens); $i++) {
						if ($i>=count($tokens)-1) {
							$outputText .= "<div class=tokend>";
						} else {
							$outputText .= "<div class=tok>";
						}	
						$outputText .= "&nbsp;".$tokens[$i]."&nbsp;";
						reset($annotation_data);
						while (list ($uid,$annotations) = each($annotation_data)) {
							//print "User: $uid<br>";
							//$outputText .= '<table cellspacing=0 cellpadding=0 class=tbl><td bgcolor=red></td></table><table cellspacing=0 cellpadding=0 class=tbl><td bgcolor=#ccc>&nbsp;d</td><td>&nbsp;ds</td></table><table cellspacing=0 cellpadding=0 class=tbl><td>sdfsdf</td></table>';
							$outputText .= "<table cellspacing=0 cellpadding=0 class=tbl";
							if (isset($annotations["-1"])) {
								$outputText .= "><td bgcolor=".$annotations["-1"]." title='".$annotators[$uid].": ".$colorValue[$annotations["-1"]]."'></td>";
							} else if (isset($annotations[$i+1])) {
								$outputText .= " onmouseover=\"javascript:colorSourceTokens('$sourceid','".$aligned_source_tokens{$uid}[$i+1]."');\" onmouseout=\"javascript:cleanSourceBackground('$sourceid',". count($source_tokens). ");\">";
								$cols = explode(" ", trim($annotations[$i+1]));
								foreach ($cols as $col) {
									$outputText .= "<td bgcolor=".$col." title='".$annotators[$uid].": ";
									if (in_array($col, $wordaligner_color)) {
										$outputText .= "Sure alignment";
									} else if (in_array($col, $wordaligner_lightcolor)) {
										$outputText .= "Possible alignment";
									}
									$outputText .= "'></td>";
								}
							} else {
								$outputText .= "><td bgcolor=#fff></td>";
							}								
							$outputText .= "</table>";
						}
						$outputText .= "</div>";
					}
					
				}
			} else {						
				if (count($user_annotations) > 0) {
					$auser = "<tr height=3>";
					for ($i=0; $i < count($tokens); $i++) {
						if (isset($user_annotations["-1"])) {
							$auser .= "<td><table width=100% border=0 cellspacing=0 cellpadding=2><td bgcolor=".$user_annotations["-1"]." title='".$annotators[$userid].": ".$colorValue[$user_annotations["-1"]]."'></td></table>";
						} else if (isset($user_annotations[$i+1])) {
							#$auser .= "<td bgcolor=".$user_annotations[$i+1]."></td>";
							$auser .= "<td><table width=100% border=0 cellspacing=0 cellpadding=2>";
							$cols = explode(" ", trim($user_annotations[$i+1]));
							foreach ($cols as $col) {
								$auser .= "<td bgcolor=".$col." title='".$annotators[$userid].": ".$colorValue[$col] ."'></td>";
							}
							$auser .= "</table></td>";
						} else {
							$auser .= "<td></td>";
						}
					}
					$user_agreements .= "\n$auser</tr>";
				}	
				$outputText .= "$user_agreements\n</table><br>";
			}		
			
			if ($done == 0) {
				print " &nbsp;&nbsp;<b>...but no annotation has been confirmed yet!</b>";
			}
			if ($outputText != "") {
				$paging = "";
				if ($from > 1) {
					$paging .= "<button onclick=\"location.href='admin.php?section=stats&id=$id&from=".($from-$max)."'\">prev</button>";
				}
				if ($num > $from+$max) {
					$paging .= " <button onclick=\"location.href='admin.php?section=stats&id=$id&from=".($from+$max)."'\">next</button>";
				}
				/*$paging .= " &nbsp;<input type='checkbox' name='showdisa'";
				if ($show_disagreement != 0) {
         			  $paging .= " checked";
        			}
				$paging .= ">Show just disagreement";*/

				print "<div style='margin:5px; padding: 4px; border: 1px solid #000; width: 90%'>";
				print "<div style='display: inline-block; border-right: 1px solid #000'>$statsTable</div>";
				if ($taskinfo["type"] == "wordaligner" && count($annotators) > 1) {						
						
					print "<span style='display:none; float: right' id='spinner' class='spinner'><img width=18 src='img/spinner.gif' valign=bottom></span><div id=agreement style='display: inline-block; float: right'><button onclick=\"javascript:getAgreement('$id');\">calculate the agreement</button></div>";
				}
				print "</div><br><strong>Annotated sentences: </strong>";
				print $paging;
				print $outputText;
				print "<hr><center>$paging</center>";
				
			} 
		}
	} else {
		print "WARNING! This task is not valid.";
	}	
}
?>
</div>

<script>
function colorSourceTokens(sentenceId, tokens) {
	var tokenArray = tokens.split(/(\s+)/);
	for(var i=0; i < tokenArray.length; i++){
		el = document.getElementById("s."+sentenceId+"."+tokenArray[i]);
    	if (el != null) {
    		el.style.backgroundColor = "#F78181";
    	}
    }
}

function cleanSourceBackground(sentenceId, tokencounter) {
	for(var c=0; c<tokencounter; c++) {
		el = document.getElementById("s."+sentenceId+"."+c);
      	if (el != null) {
      		el.style.backgroundColor = "#fff";
      	}
	}
}
</script>
