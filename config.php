<?php 

//session_start();
secure_session_start();
include("initdb.conf");
			  
if (isset($_REQUEST)) {
    $PHP_SELF = $_SERVER['PHP_SELF'];
    $REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];
    $REMOTE_HOST = "";
    while (list($key,$val) = each($_REQUEST)) {
		#print "$key = ";
		if (is_array($val) && count($val) > 0) {
		    #print "L: $key = " . implode(" ",$val) ."<br>";
	    	eval("$\$key = array('" .implode("','",$val) . "');");
		} else {
		    #print "V: $key = $val<br>";
	    	eval("$\$key = '" . addslashes($val) . "';");
		}
    }
}
   
if (isset($_SESSION) && isset($_SESSION["mysession"])) {
	$mysession = $_SESSION["mysession"];
}


#debugging layer
if (DEBUG == "yes") {
	print"<div class=debug>";
	if (isset($mysession)) {
		while (list ($key, $value) = each($mysession)) {
			print $key.": <b>" .$value."</b><br>\n";
		}
	}
	print "</div>";
}	


# db connection	
$db = @mysql_pconnect(DB_HOST,DB_USER,DB_PASSWORD) or die("<center><br><h3>Fatal Error: unable to connect to the AnnotatorPro database server (attempted on ". DB_HOST . ").<br>Please contact ".SYSADMIN.".</h3></center>");  
    
if (!mysql_ping ($db)) {
	//here is the major trick, you have to close the connection (even though its not currently working) for it to recreate properly.
	mysql_close($db);
	$db = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD) or die("Error: cannot connect to " . DB_HOST);
}
if ( !@mysql_select_db(DB_NAME,$db) ) {
	#echo "<p>Unable to find the ".DB_NAME." database on ".DB_HOST.". Please contact ".SYSADMIN.".<p>";
	echo "<p>Unable to find the AnnotatorPro database. Please contact ".SYSADMIN.".<p>";
    #echo mysql_errno($db) . ": " . mysql_error($db). "\n";
	exit();
}  


function secure_session_start() {
        $session_name = 'mtequal_session_id'; // Imposta un nome di sessione
        $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
        $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all'id di sessione.
        ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
        $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
        session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
        session_start(); // Avvia la sessione php.
        //session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}
?>
