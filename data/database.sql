-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: annotatorpro
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `al_memory`
--

DROP TABLE IF EXISTS `al_memory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `al_memory` (
  `entity` varchar(500) NOT NULL,
  `history` text,
  PRIMARY KEY (`entity`),
  UNIQUE KEY `entity_UNIQUE` (`entity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `al_memory`
--

LOCK TABLES `al_memory` WRITE;
/*!40000 ALTER TABLE `al_memory` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_memory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `al_sentence`
--

DROP TABLE IF EXISTS `al_sentence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `al_sentence` (
  `file` varchar(500) NOT NULL,
  `sentence_num` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `filename` varchar(500) NOT NULL,
  PRIMARY KEY (`sentence_num`,`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `al_sentence`
--

LOCK TABLES `al_sentence` WRITE;
/*!40000 ALTER TABLE `al_sentence` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_sentence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `al_window`
--

DROP TABLE IF EXISTS `al_window`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `al_window` (
  `window` text NOT NULL,
  `sentence_num` int(11) NOT NULL,
  `file` varchar(500) NOT NULL,
  `used` int(11) NOT NULL,
  `confidence` double NOT NULL,
  `filename` varchar(500) NOT NULL,
  `entitypos` varchar(500) NOT NULL,
  `entity` varchar(500) NOT NULL,
  PRIMARY KEY (`sentence_num`,`filename`,`entitypos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `al_window`
--

LOCK TABLES `al_window` WRITE;
/*!40000 ALTER TABLE `al_window` DISABLE KEYS */;
/*!40000 ALTER TABLE `al_window` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annotation`
--

DROP TABLE IF EXISTS `annotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotation` (
  `sentence_num` int(100) NOT NULL,
  `output_id` varchar(100) NOT NULL,
  `user_id` int(10) NOT NULL,
  `eval` int(4) NOT NULL DEFAULT '-1',
  `evalids` text NOT NULL,
  `evaltext` text NOT NULL,
  `lasttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `conoscenza` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sentence_num`,`output_id`,`user_id`,`eval`),
  KEY `sentence_num` (`sentence_num`,`output_id`,`user_id`),
  KEY `eval` (`eval`),
  KEY `lasttime` (`lasttime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annotation`
--

LOCK TABLES `annotation` WRITE;
/*!40000 ALTER TABLE `annotation` DISABLE KEYS */;
/*!40000 ALTER TABLE `annotation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `annotationsave`
--

DROP TABLE IF EXISTS `annotationsave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotationsave` (
  `sentence_num` int(100) NOT NULL,
  `output_id` varchar(100) NOT NULL,
  `user_id` int(10) NOT NULL,
  `eval` int(4) NOT NULL,
  `evalids` text NOT NULL,
  `evaltext` text NOT NULL,
  `lasttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sentence_num`,`output_id`,`user_id`,`eval`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annotationsave`
--

LOCK TABLES `annotationsave` WRITE;
/*!40000 ALTER TABLE `annotationsave` DISABLE KEYS */;
/*!40000 ALTER TABLE `annotationsave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clusters`
--

DROP TABLE IF EXISTS `clusters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clusters` (
  `name` varchar(10) NOT NULL,
  `maxlimit` int(11) NOT NULL DEFAULT '0',
  `counter` int(11) NOT NULL DEFAULT '0',
  `cycle` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clusters`
--

LOCK TABLES `clusters` WRITE;
/*!40000 ALTER TABLE `clusters` DISABLE KEYS */;
/*!40000 ALTER TABLE `clusters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `sentence_num` int(100) NOT NULL,
  `user_id` int(10) NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`sentence_num`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `done`
--

DROP TABLE IF EXISTS `done`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `done` (
  `sentence_num` int(100) NOT NULL,
  `user_id` int(10) NOT NULL,
  `completed` enum('Y','N') DEFAULT 'N',
  `lasttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `exported` enum('Y','N') DEFAULT 'N',
  `trained` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`sentence_num`,`user_id`),
  KEY `sentence_num` (`sentence_num`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `completed` (`completed`),
  KEY `lasttime` (`lasttime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `done`
--

LOCK TABLES `done` WRITE;
/*!40000 ALTER TABLE `done` DISABLE KEYS */;
/*!40000 ALTER TABLE `done` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `errorList`
--

DROP TABLE IF EXISTS `errorList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `errorList` (
  `error` varchar(500) NOT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `avgwrong` double NOT NULL DEFAULT '0',
  `avgright` double NOT NULL DEFAULT '0',
  `confidence` double NOT NULL DEFAULT '0',
  `wrongcases` double NOT NULL DEFAULT '0',
  `right_cases` double NOT NULL DEFAULT '0',
  `error_counter` int(11) NOT NULL DEFAULT '0',
  `selection_counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`error`),
  UNIQUE KEY `error_UNIQUE` (`error`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `errorList`
--

LOCK TABLES `errorList` WRITE;
/*!40000 ALTER TABLE `errorList` DISABLE KEYS */;
/*!40000 ALTER TABLE `errorList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f1measures`
--

DROP TABLE IF EXISTS `f1measures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f1measures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f1measures`
--

LOCK TABLES `f1measures` WRITE;
/*!40000 ALTER TABLE `f1measures` DISABLE KEYS */;
/*!40000 ALTER TABLE `f1measures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `query` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `error` varchar(4) NOT NULL,
  `lasttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sentence`
--

DROP TABLE IF EXISTS `sentence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sentence` (
  `num` int(100) NOT NULL AUTO_INCREMENT,
  `id` varchar(100) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `type` varchar(20) NOT NULL,
  `task_id` int(4) NOT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `linkto` varchar(100) DEFAULT NULL,
  `lasttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tokenization` int(2) NOT NULL,
  `finished` enum('Y','N','C','W','P') NOT NULL DEFAULT 'N',
  `annotators` varchar(100) NOT NULL DEFAULT '',
  `gold` enum('Y','N') NOT NULL DEFAULT 'N',
  `features` text NOT NULL,
  `cluster` varchar(45) NOT NULL,
  `confidence` double NOT NULL,
  `category` varchar(45) NOT NULL,
  PRIMARY KEY (`num`),
  KEY `id` (`id`),
  KEY `type` (`type`),
  KEY `task_id` (`task_id`),
  KEY `lang` (`lang`),
  KEY `linkto` (`linkto`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sentence`
--

LOCK TABLES `sentence` WRITE;
/*!40000 ALTER TABLE `sentence` DISABLE KEYS */;
/*!40000 ALTER TABLE `sentence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` enum('quality','errors','wordaligner','docann') DEFAULT NULL,
  `descr` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `instructions` text NOT NULL,
  `ranges` text NOT NULL,
  `randout` enum('Y','N') DEFAULT 'N',
  `owner` int(10) NOT NULL,
  `showmetadata` enum('Y','N') NOT NULL DEFAULT 'Y',
  `modalityperdoc` enum('multiperdoc','oneperdoc') NOT NULL DEFAULT 'oneperdoc',
  `agreementmodality` enum('majorityagreed','nannotatorsagreed') NOT NULL DEFAULT 'nannotatorsagreed',
  `annagreed` int(11) NOT NULL DEFAULT '1',
  `taskphase` enum('annotationphase','revisionphase') NOT NULL DEFAULT 'annotationphase',
  `selectionmodality` enum('dborder','randomlargedataset','allargedataset') NOT NULL DEFAULT 'dborder',
  `goldtestEveryAnno` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `owner` (`owner`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `team` varchar(10) NOT NULL,
  `activated` enum('Y','N') DEFAULT 'Y',
  `registered` date NOT NULL,
  `notes` text NOT NULL,
  `refuser` int(10) NOT NULL,
  `viewagree` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usertask`
--

DROP TABLE IF EXISTS `usertask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usertask` (
  `user_id` int(4) NOT NULL,
  `task_id` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`task_id`),
  KEY `user_id` (`user_id`),
  KEY `task_id` (`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usertask`
--

LOCK TABLES `usertask` WRITE;
/*!40000 ALTER TABLE `usertask` DISABLE KEYS */;
/*!40000 ALTER TABLE `usertask` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-05 23:04:25
