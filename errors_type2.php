<?php
header("Content-type: text/html; charset=utf-8");
include("config.php");
include("functions.php");

if (!isset($userid)) {
	$userid = $mysession['userid'];
} 
	
if (!isset($taskid)) {
	$taskid = $mysession["taskid"];
}

if (isset($monitoring) && $monitoring == 1) {
	$sentidx=-1;
} else {
	$monitoring=0;
}

if (!isset($onlyreset)) {
	$onlyreset = "";
} 

$ranges = $mysession["taskranges"];
$errors = getErrors($id,$targetid,$userid);
$hash_target = getSystemSentences($id,$taskid);
$hash_source = getSentence($id, $taskid);
$source_tokens = getTokens($hash_source["source"][0], $hash_source["source"][1], 1);
$target_tokens = getTokens($hash_target[$targetid][0], $hash_target[$targetid][1], $hash_target[$targetid][2]);

$i=0;
while (list ($sentence_id, $sentence_item) = each($hash_target)) {
	if ($sentence_id==$targetid) {
		break;
	}
	$i++;
}
				
$checkid = 0;
while (list ($val,$attrs) = each($ranges)) {
	if ($val == 0) {
		if (count($errors) == 0 || isset($errors[0])) {
			$color="#".$attrs[1];
			$bordercolor="4px solid ".$color;
			if (isset($errors[$val])) {
				$bordercolor="4px solid red";
			}
			if ($val == 0) {
				print "<table cellspacing=4>";
			} 
			print "<td style='padding: 1px; background: $color; border: $bordercolor; box-shadow: 2px 2px 2px #888; font-size:13px' id='check.$i.$checkid' align=center onmouseover='fadeIn(this);'  onmouseout='fadeOut(this,\"".$attrs[1]."\");' onClick=\"check('$id','$targetid',$userid,$val,$checkid,".count($ranges).",$i,".count($hash_target).");\" nowrap>".$attrs[0] ."</td></tr>";
			if ($val == 1) {
				print "</table>";
			}
			$checkid++;
		} 
	} 
}		
while (list ($errID, $errARRAY) = each($errors)) {
	if ($errID > 0) {
	  	$tokenids = explode(",",$errARRAY[0]);
		//$texts = explode("__BR__",$errARRAY[1]);
		$hash_annotations= array();
		for($r=0; $r<count($tokenids); $r++) {
			$delicon = "";
			if (count($ranges) > 1) {
				if ($monitoring==0) {	
					$delicon = "<a href=\"javascript:removeAnnotation($id,$targetid,'".$tokenids[$r]."',$errID);\"><img src='img/delete.png' width=12></a>";
				}
			}
			
			//if (trim($texts[$r]) == "") {
			//	$annotations .= "<small>_SPACE_</small>";
			//} else {
			//	$annotations .= $texts[$r];
			//}
			$token_ids = array_unique(preg_split("/ /", preg_replace("/\-[0-9]+/","",$tokenids[$r])));
			sort($token_ids);
			$keyann = count($hash_annotations); //$token_ids[0].".".$token_ids[1];
			$hash_annotations{$keyann} = "<div style='white-space: nowrap; display: inline; font-size: 13px' onmouseover=\"javascript:showRange(this,$targetid,'".$tokenids[$r]."');\" onmouseout=\"javascript:hideRange(this,$targetid,'".$tokenids[$r]."');\"  onclick=\"javascript:goto($targetid,".preg_replace("/ .*/","",$tokenids[$r]).");\">&nbsp;$delicon&nbsp;";
			foreach ($token_ids as $tid) {
				$hash_annotations{$keyann} .= $source_tokens[$tid-1]." ";
			}
			$hash_annotations{$keyann} .= "⇒ ";
			$token_ids = array_unique(preg_split("/ /", preg_replace("/[0-9]+\-/","",$tokenids[$r])));
			ksort($token_ids);
			foreach ($token_ids as $tid) {
				$hash_annotations{$keyann} .= $target_tokens[$tid-1]." ";
			}
			$hash_annotations{$keyann} .="</div><br>";
		}
		
		print "<div style='margin-top: 2px; border-bottom: 4px solid #".$ranges[$errID][1]."; white-space: nowrap; background: #fff;'>";
		if ($monitoring==0) {	
			print "&nbsp;<button id='reset.$i.$errID' onclick=\"javascript:reset2('$id','$targetid',$taskid,$userid,$errID,".count($ranges).",".count($hash_target).",'".$ranges[$errID][1]."','".$onlyreset."');\">reset</button>";
		}
		print "&nbsp; <i><b>".$ranges[$errID][0]."</b></i> &nbsp;</div>";
		if ($onlyreset != "yes") {
			ksort($hash_annotations);
			foreach($hash_annotations as $k => $annotatedWords) {         
       			print $annotatedWords; 
    		} 
    	}
    	print "<br>"; 
    }
}	  
?>
