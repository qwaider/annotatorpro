<?php
header("Content-type: text/html; charset=utf-8");

include("config.php");
include("functions.php");
include("menu.php");

?>
<link href="css/mtequal.css" rel="styleSheet" type="text/css">
<div class=index>
<center>
<h3>Contacts</h3>
<table width=50% cellspacing=10 cellpadding=10>
<tr><td valign=top nowrap>
	<b>Bernardo Magnini</b><br>
	Site: <a href="https://hlt-nlp.fbk.eu/people/profile/magnini">https://hlt-nlp.fbk.eu/people/profile/magnini</a><br>
</td><td valign=top nowrap>
	<b>Mohammed R. H. Qwaider</b><br>
	Site: <a href="https://hlt-nlp.fbk.eu/people/profile/qwaider">https://hlt-nlp.fbk.eu/people/profile/qwaider</a><br>
</td></tr><tr><td valign=top nowrap>
	<b>Anne-Lyse Minard</b><br>
	Site: <a href="https://sites.google.com/site/minardannelyse/">https://sites.google.com/site/minardannelyse/</a><br>
</td><td valign=top nowrap>
	<b>Manuela Speranza</b><br>
	Site: <a href="https://hlt-nlp.fbk.eu/people/profile/manspera">https://hlt-nlp.fbk.eu/people/profile/manspera</a><br>
	Contact : manspera<img src="img/witat.gif">fbk.eu<br></tr>
<tr><td colspan=3 align=center nowrap>
<br>
<table>
<tr>
<th>
<a href="http://www.fbk.eu/"><img src="img/logo_FBK.gif" width=100 height=70 title="FBK" border=0></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</th>
<th><a href="http://hlt-nlp.fbk.eu/"><img valign=bottom src="https://hlt-nlp.fbk.eu/sites/hlt-nlp.fbk.eu/themes/fbkunit/logo-en.png" align=top height=67 title="HLT-NLP - Human Language Technology Natural Language Processing " border=0></a>
</th>
</tr>
<tr>
<th>
FBK - Fondazione Bruno Kessler &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</th><th> Human Language Technology - Natural Language Processing Group</th></tr></table>

<br>via Sommarive, 18 - 38100 Trento, Italy
</td><tr>
<tr><td colspan=3 align=center nowrap>
<hr><br><br>
AnnotatorPro was built on top of the MT-EQuAl toolkit (https://github.com/hltfbk/MT-EQuAl). <br>
<br>
The work has been partially funded by the EUCLIP_RES project ("Bando Innovazione 2016" of the autonomous Province of Bolzano). </td>
</tr>
</table>
</center>
</div>
