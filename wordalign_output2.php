<html>
<head>
<link href="css/mtequal.css" rel="styleSheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/mtequal.js"></script>
	
<?php
header("Content-type: text/html; charset=utf-8");
include("config.php");
include("functions.php");

if (!isset($userid)) {
	$userid = $mysession['userid'];
} 
	
if (!isset($taskid)) {
	$taskid = $mysession["taskid"];
}

if (isset($monitoring) && $monitoring == 1) {
	$sentidx=-1;
} else {
	$monitoring=0;
}
?>

<script type="text/javascript">
/* Default value of choice */
var selectionFrom = 0;
var selectionTo = 1;

function getColor (idx) {
	return "#ccc";
}

function goto(el,interval) {
	var leftEl = document.getElementById("LeftPane");
	var tokenEl = document.getElementById(el+"."+interval);
	
	if (leftEl != null && tokenEl != null) {
		if (tokenEl.offsetTop > 100) {
			leftEl.scrollTop = tokenEl.offsetTop-100;
		} else {
			leftEl.scrollTop = 0;
		}
	}
}

function showRange(el,sentid,range) {
	el.style.borderBottom = "solid #000 2px";
	el.style.cursor = "nw-resize";
	var ids = range.split(" ");
	for (var i in ids) {
		//source token
    	var el = document.getElementById("s"+sentid+"."+ids[i].replace(/\-.*/g,''));
    	if (el != null) {
    		if (el.style.backgroundColor == COLOR) {
				el.style.borderTop = "2px solid "+COLOR;
			}
			el.style.backgroundColor = "#bbb";
    	}
    	//output token
    	var el = document.getElementById(sentid+"."+ids[i].replace(/.*\-/g,''));
    	if (el != null) {
    		if (el.style.backgroundColor == COLOR) {
				el.style.borderTop = "2px solid "+COLOR;
			}
			el.style.backgroundColor = "#bbb";
    	}
    }
}

function hideRange(el,sentid,range) {
	el.style.borderBottom = "none";
	el.style.cursor = "default";
	var ids = range.split(" ");
	for (var i in ids) {
		//source token
    	var el = document.getElementById("s"+sentid+"."+ids[i].replace(/\-.*/g,''));
    	if (el != null) {
    		if (el.style.borderTop == "2px solid "+COLOR) {
				el.style.backgroundColor = COLOR;
				el.style.borderTop = "none";
			} else {
				el.style.backgroundColor = WHITE;
			}
		}
		//output token
		var el = document.getElementById(sentid+"."+ids[i].replace(/.*\-/g,''));
    	if (el != null) {
    		if (el.style.borderTop == "2px solid "+COLOR) {
				el.style.backgroundColor = COLOR;
				el.style.borderTop = "none";
			} else {
				el.style.backgroundColor = WHITE;
			}
		}
    }
}

function removeAnnotation(id,targetid,ranges,errid) {
  //alert(id+","+targetid+","+ranges);
  if (confirm("Do you really want to cancel this annotation?")) {
  	$.ajax({
  		url: 'update.php',
  		type: 'GET',
  		data: "id="+id+"&targetid="+targetid+"&userid=<?php echo $userid;?>&check="+errid+"&action=remove&alignids="+ranges+"&taskid=<?php echo $taskid;?>&words=",
  		async: false,
  		cache:false,
  		crossDomain: true,
  		success: function(response) {
  			//$("#output"+targetid).html(response);
  			eval(response);
    		
  			$.ajax({
  				url: 'errors_type2.php',
 				type: 'GET',
  				data: "id="+id+"&targetid="+targetid+"&userid=<?php echo $userid;?>",
  				async: false,
  				cache:false,
  				crossDomain: true,
  				success: function(response) {
  					$("#errors"+targetid).html(response);
  					var range_ids = ranges.split(" ");
  					//remove borders bottom
					for	(x = 0; x < range_ids.length; x++) {
						var ids = range_ids[x].split("-");
						//source
						el = document.getElementById("bs"+targetid+"."+ids[0]);
						if (el != null) {
							if (errid == 1) {
								el.style.borderTop = "";
							} else {
								el.style.borderBottom = "";
							}
						}
					
						//target
						el = document.getElementById("b"+targetid+"."+ids[1]);
						if (el != null) {
							if (errid == 1) {
								el.style.borderTop = "";
							} else {
								el.style.borderBottom = "";
							}
						}
					}
				}
			});
  		},
  		error: function(response, xhr,err ) {
        	//alert(err+"\nreadyState: "+xhr.readyState+"\nstatus: "+xhr.status+"\nresponseText: "+xhr.responseText);
        	switch(xhr.status) {
				case 200: 
					alert("Sorry! Data has not been saved!");
			}
		}
  	});
	//window.open("errors_output.php?id="+id+"&sentidx=<?php echo $sentidx; ?>","_self");
                        
  }	
}

$(document).ready(function() {
	try {
		$(document).bind("contextmenu", function(e) {
			e.preventDefault();
			/*if (OUTPUTID != null && isSelected(OUTPUTID) == 1) {	
				$("#errortypes").css({ top: (e.pageY-2) + "px", left: (e.pageX-20) + "px" }).show(100);
			} else {
				$("#noselection").css({top: (e.pageY-2) + "px", left: (e.pageX-20) + "px" }).show(100);
				var el = document.getElementById("noselection");
    			el.style.visibility = "visible";

			}*/
		});
		$(document).mouseup(function(e) {
			var container = $("#errortypes");
			if (container.has(e.target).length == 0) {
				container.hide();
				//container.show();
				//hideErrorMenu();
				//alert(container.has(e.target).length);
			}
			//showErrorMenu();
		});
	} catch (err) {
		alert(err);
	}
});

</script>
</head>

<body>
<div id="errortypes" onclick="this.style.visibility='hidden';" style="font-size: 10px">
<table width=200 border=0 cellspacing=0 cellpadding=2 style='background-color: #ddd; color: #000; font-size: 16px; box-shadow: 3px 3px 3px #888; '>
<?php
	$ranges = $mysession["taskranges"];
	while (list ($val,$attrs) = each($ranges)) {
		if ($val > 0) {
			print "<tr><td onclick=\"javascript:saveAnnotationRanges($val,'{$attrs[1]}');\" onmouseover=\"this.className='yellow'\" onmouseout=\"this.className='whitebg'\">".$attrs[0]."</td></tr>";
		}
	}
?>	
</table>
</div>

<!--
<div id="noselection" onclick="this.style.visibility='hidden';" style="position: fixed; visibility: hidden; box-shadow: 2px 2px 2px #888; font-size: 14px; padding-left: 20px; width: 140px; border: 1px solid #000; background: lightyellow">
<img src='img/bullet_error.png'> No selection!
</div>
-->

<?php
$prevAndnextIDs = getPrevNext($taskid, $id);	
if ($sentidx != -1) {
 	$sentidx = $prevAndnextIDs[2];
}
print "<div class=donebottom>";
$prevpage = "wordaligner.php?id=".$prevAndnextIDs[0]."&taskid=$taskid&sentidx=".($sentidx-1);
$nextpage = "wordaligner.php?id=".$prevAndnextIDs[1]."&taskid=$taskid&sentidx=".($sentidx+1);
print "<button id=prev name=prev onclick=\"javascript:next('$prevpage');\">&nbsp;« prev&nbsp;</button> &nbsp;";
print "<button style='width: 170' id=done name=done onclick=\"javascript:doneAndIndex('$id','$userid',this);\" disabled></button> &nbsp;";
print "<button id=next name=next onclick=\"javascript:next('$nextpage');\">&nbsp;next »&nbsp;</button>";
		
print "</div>";
if (empty($mysession["status"])) {
	print "<script>window.open('index.php','_self');</script>";
}

if (!isset($errorid)) {
	$errorid = "";
} 

if ($taskid > 0 && isset($id) && isset($userid)) {
	$hash_target = getSystemSentences($id,$taskid);
	$hash_source = getSentence($id, $taskid);

	$i = 0;
	$checked = 0;
	print "<div style='width: 100%; position: relative; height: 100%; margin-top:0px; margin-left: -28px;  padding-right: 46px; margin-bottom: auto; overflow-y: auto;'>";
	if (count($hash_target) > 0) {
	  while (list ($sentence_id, $sentence_item) = each($hash_target)) {
		$errors = getTargetAlignment($id, $sentence_id, $userid);
		$source_errors = getSourceAlignment($id, $sentence_id, $userid);
		if (count($errors) > 0) {
			$checked++;
		}
		print "<table cellpadding=0 cellspacing=2 border=0> <td valign=top>";
		print "<div>";
		//print "<div style='display: table-cell; float: left; width: 666px'>";
		
		print "<div style='display: block; width: 100%; float: left; left: 0px;'><div style='background: #eee; padding-bottom: 6px; padding-top:3px'> <div class=label>SOURCE: </div>" . showAlignedSentence("wordaligner",$hash_source["source"][0], $hash_source["source"][1], "outputsource", 1, "s".$sentence_id, $source_errors, $ranges)."</div><div>";

		//Add output row
		//print "<br>&nbsp;&nbsp;ERRORs ".var_dump($errors); 
		//print "<br>&nbsp;&nbsp;RANGEs ".var_dump($ranges); 
		$sent = showAlignedSentence ("wordaligner", $sentence_item[0], $sentence_item[1], "output", $sentence_item[2], $sentence_id, $errors, $ranges);
		//ripristino eventuali errori nei carattri con lastring vuota se non sono stati fatte delle anotazioni
		#if(count($errors) == 0) {
		#	$sent = preg_replace("/<img src='img\/check_error.png' width=16>/","",$sent);
		#}
		print "<div class=row><div class=label>OUTPUT <b>".($i+1)."</b>: </div><div class=cell id='output".$sentence_id."'>$sent</div></div>";
		//end output row
		
		//Add comment row
		print "<div class=row><div class=cell>";
		$comment = getComment($sentence_id,$userid);
		
		if ($monitoring==0) {		
		?>
		<!-- comment -->
		<a href='#comm<?php echo $sentence_id; ?>' class='nav-toggle'><img src='img/addcomment.png' style='vertical-align: top; float: right;' width=80></a>&nbsp;</div>
			<div id="comm<?php echo $sentence_id; ?>" style="display:none; font-size: 12px;">
				<textarea id=comm<?php echo $sentence_id; ?>_text rows=2 cols=75 style='background: lightyellow'><?php echo $comment; ?></textarea>
			</div>
			<div style="display: inline-block; top: 10px;" id=comm<?php echo $sentence_id; ?>_label><?php echo $comment; ?></div>		
		
		<?php
		} else {
			if ($comment !="") {
				print "<small><i>Comment:</i></small> </div>$comment<div class=cell>";
			}
		}
		?>
		<!-- fine comment -->
		<?php
		print "</div>";
		//end comment row	
			
		print "</div>";
		//end cell (output+comment)
		
		//start error cell 
		print "<td valign=top>";
		//print "<div class='cell right' id='errors".$sentence_id."'>";
		print "<div style='font-size:12px; padding-bottom: 2px; margin-right: -40px; max-height: 180px; overflow-y: auto; border: 1px solid #000; max-width: 220px; margin-top: -3px' id='errors".$sentence_id."'>";
		
		reset($ranges);
		/* non mostro i bottoni
		$checkid = 0;
		while (list ($val,$attrs) = each($ranges)) {
			if ($val <= 1) {
				if (count($errors) == 0 || isset($errors[0]) || isset($errors[1])) {
					$color="#".$attrs[1];
					$bordercolor="4px solid ".$color;
					if (isset($errors[$val])) {
						$bordercolor="4px solid red";
					}
					if ($val == 0) {
						print "<table cellspacing=4>";
					} 
					print "<td style='padding: 1px; background: $color; border: $bordercolor; box-shadow: 2px 2px 2px #888; font-size:13px' id='check.$i.$checkid' align=center onmouseover='fadeIn(this);'  onmouseout='fadeOut(this,\"".$attrs[1]."\");' onClick=\"check('$id','$sentence_id',$userid,$val,$checkid,".count($ranges).",$i,".count($hash_target).");\" nowrap>".$attrs[0] ."</td></tr>";
					if ($val == 1) {
						print "</table>";
					}
				} 
			$checkid++;
			} 
		}
		*/
			
		print "</div></td>";
		//end error cell
		
		print "</table><div style='display: inline-block; border-top: dashed #666 1px; width: 100%'>&nbsp;</div>";
		$i++;
		
		?>	
		<script> $.ajax({
	url: 'errors_type2.php',
  	type: 'GET',
  	data: "id=<?php echo $id;?>&targetid=<?php echo $sentence_id;?>&userid=<?php echo $userid;?>",
  	async: false,
  	cache:false,
  	crossDomain: true,
  	success: function(response) {
  		$("#errors<?php echo $sentence_id;?>").html(response);  
	}});
	</script>
	
	<?php	
	  }
	
	  #print count($hash_target) ."!= $checked || ".isDone($id,$userid);
	  if (isDone($id,$userid) > 0) {
		print "<script>alreadyDone();</script>";
	  } else {
		if ($checked != count($hash_target)) {
	 		print "<script>notDoneYet();</script>";
		} else {
			print "<script>activateDone(".$monitoring.");</script>";
		}
	  } 
	} else {
		print "<h3><font color=red>No output found!</font></h3>";
	}
} 
?>
<div class=log id=log></div>       


<script type="text/javascript" src="js/wordaligner2.js"></script>
<script type="text/javascript" charset="utf-8">
var ERRORID="<?php echo $errorid; ?>";
var ERRORCOLOR = "red";
var OUTPUTID = null;
var HASH_ANNOTATION = new Object();

function saveAnnotationRanges(errid, color) {
	ERRORID = errid;
	var elid = 1;
	var ranges = "";
	var source_ranges = [];
	var output_ranges = [];
	
	while(true) {
		//alert(OUTPUTID+" "+elid);
		el = document.getElementById(OUTPUTID+"."+elid);
		//alert(ERRORID+" "+OUTPUTID+"."+elid + " " + el.style.backgroundColor);
		if (el == null) {
			break;
		} else {
			if (el.style.backgroundColor != WHITE) {
				output_ranges.push(elid);
			}
		}	
    	elid++;	
	}
	
	elid=1;
	while(true) {
		//alert("s"+OUTPUTID+" "+elid);
		el = document.getElementById("s"+OUTPUTID+"."+elid);
		//alert(ERRORID+" s"+OUTPUTID+"."+elid + " " + el.style.backgroundColor);
		if (el == null) {
			break;
		} else {
			if (el.style.backgroundColor != WHITE) {
				source_ranges.push(elid);
			}
		}	
    	elid++;	
	}
	
	//alert("ID: <?php echo $id;?>, OUTPUTID: "+OUTPUTID+", ERRORID: "+ERRORID+", RANGES: "+ranges);
	for	(x = 0; x < source_ranges.length; x++) {
		for	(y = 0; y < output_ranges.length; y++) {
			ranges += " "+source_ranges[x]+"-"+output_ranges[y];
			//HASH_ANNOTATION["s"+OUTPUTID+"."+source_ranges[x]]=output_ranges[y];
			//HASH_ANNOTATION[OUTPUTID+"."+output_ranges[y]]=source_ranges[x];
		}
	}
	ranges = ranges.replace(/^\s*/,"");
	//alert("RANGES: " + ranges + " color:" + color);
 //if (send) { // && trim(ranges) != "") { 	
 
 $.ajax({
	url: 'update.php',
	type: 'GET',
	data: "id=<?php echo $id;?>&targetid="+OUTPUTID+"&userid=<?php echo $userid;?>&action=add&check="+ERRORID+"&alignids="+ranges+"&taskid=<?php echo $taskid;?>",
	async: false,
	cache:false,
	crossDomain: true,
	success: function(response) {
		if (response == "error") {
  			//$("#log").html("");
			alert("Warning! A problem occurred during saving the data. Try again later!");
		} else {
			//update list of annotated tokens		
			//$("#output"+OUTPUTID).html(response);
			cleanBgColor(OUTPUTID);
			cleanBgColor("s"+OUTPUTID);
			//source border bottom
			for	(x = 0; x < source_ranges.length; x++) {
				el = document.getElementById("bs"+OUTPUTID+"."+source_ranges[x]);
				if (el != null) {
					if (ERRORID == 1) {
						el.style.borderTop = "4px solid #"+color;
					} else {
						el.style.borderBottom = "4px solid #"+color;
					}
				}
			}
			//output border bottom
			for	(y = 0; y < output_ranges.length; y++) {
				el = document.getElementById("b"+OUTPUTID+"."+output_ranges[y]);
				if (el != null) {
					if (ERRORID == 1) {
						el.style.borderTop = "4px solid #"+color;
					} else {
						el.style.borderBottom = "4px solid #"+color;
					}
				}
			}
    		
    		//update done bottom
    		eval(response);
    		
			$.ajax({
  				url: 'errors_type2.php',
 				type: 'GET',
  				data: "id=<?php echo $id;?>&targetid="+OUTPUTID+"&userid=<?php echo $userid;?>",
  				async: false,
  				cache:false,
  				crossDomain: true,
  				success: function(response) {
  					$("#errors"+OUTPUTID).html(response);
				}
			});
		}
 	},
  	error: function(response, xhr,err ) {
        //alert(err+"\nreadyState: "+xhr.readyState+"\nstatus: "+xhr.status+"\nresponseText: "+xhr.responseText);
        //alert(ERRORID);
        switch(xhr.status) {
			case 200: 
				$("#log").html('<font color=gray>Data saved!</font>');
      			break;
    		case 404:
      			$("#log").html('<font color=red>Could not contact server.</font>');
      			break;
    		case 500:
      			$("#log").html('<font color=red>A server-side error has occurred.</font>');
      			break;
    		}   
    	setTimeout(function(){$("#log").html('')}, 3000);		
    }
});
 
 
ERRORID = "";
}

function getType (color) {
	if (color == ERRORCOLOR) {
		return ERRORID;
	} 
	return color;
}

function setColor(selectel, index, id)  {
	var color = selectel.value;
    if (selectel.id == "corefcolor" || selectel == "corefcolor") {
    	ERRORID = id;
    	COLOR = ERRORCOLOR;
    	color = COLOR;
    	//$("#color").prop("selectedIndex",0);
    } else {
    	//ERRORID = selectel.options[selectel.options.selectedIndex].text;
    	//ERRORID = ERRORID.substr(0,3).toUpperCase();
    	ERROR = null;
    	COLOR = color;
    	//$("#corefcolor").prop("selectedIndex",0);
    }
}

$(document).ready(function() {
  	$('.nav-toggle').click(function() {
		//get collapse content selector
		var collapse_content_selector = $(this).attr('href');					
		//make the collapse content to be shown or hide
		var toggle_switch = $(this);
		$(collapse_content_selector).toggle(function(){
			if ($(this).css('display')=='none'){
				//change the button label to be 'Show'
				if (this.id.indexOf("comm") == 0) {
					toggle_switch.html("<img src='img/addcomment.png' style='vertical-align: top; float: right;' width=80>");
					
					el = document.getElementById(this.id+"_text");
					if (el != null) {
						save_comment(this.id,<?php echo $userid ?>,el.value);
						$("#"+this.id+"_label").html(el.value);
						elComment = document.getElementById(this.id+"_label");
						elComment.style.visibility = "visible";
						//activateDone();
					} else {
						alert("Error while saving the comment! Please contact the administrator. (code: 1001)");
					}	
				} else {
					toggle_switch.html('read more');
				}
				
			}else{
				//change the button label to be 'Hide'
				if (this.id.indexOf("comm") == 0) {
					$("#"+this.id+"_text").focus();
					elabel = this.id.replace(/_label/,"");
					elComment = document.getElementById(elabel+"_label");
					//alert(el.id);
					if (elComment != null) {
						elComment.style.visibility = "hidden";
					}
					toggle_switch.html("<img src='img/savecomment.png' style='vertical-align: top; float: right;' width=40>");
				} else {
					toggle_switch.html('close');
				}
			}
		});
	});
});	


/*(function($){
    var methods = {
            disable: function() {
              return $(this)
                .attr('unselectable', 'on')
                .attr('disabled','disabled')
                .css({
                   '-user-select': 'none',
                   '-webkit-user-select': 'none',
                   '-khtml-user-select': 'none',
                   '-moz-user-select': 'none',
                   '-o-user-select': 'none'
                })
                .each(function() { 
                   this.onselectstart = function() { return false; };
                });
            }
        };
    
    $.fn.textSelect = function( method ) {
        // Method calling logic
        if ( methods[method] ) {
          return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else {
          $.error( 'Method ' +  method + ' does not exist on jQuery.textSelect' );
        }    
    }

        
})(jQuery);
$('.unselect').textSelect('disable');
*/

//if IE4+
//document.onselectstart=new Function ("return false")

//if NS6
//if (window.sidebar){
//document.onmousedown=disableselect
//document.onclick=reEnable
//}
</script>
<br></br>

</div>
</body>
</html>
