
<?php
set_time_limit(0);
//include("config.php");
//include("functions.php");
$id=158; // taskid
?>
<script type="text/javascript" src="js/cohens_kappa.js"></script>
<script>
var Cohen = new Cohen();
</script>
<?php
//get all users for this task id from usertask, join it to get the users whom their task is annotator or admin => all the users who could annotate this task
//give the option to include or exclude one annotator from IAA calculation

 $query = "SELECT usertask.user_id, user.username,user.status FROM usertask,user WHERE usertask.user_id= user.id AND usertask.task_id = '$id'";
 $resultUsers = safe_query($query); 
 $annotators=array();
 $managers=array();//which has gold annotation 
 echo "</br></br></br><b>&nbsp&nbsp".mysql_num_rows($resultUsers)." users provided annotations for this task.</b></br></br>";
?>
 <div id="annot" >
        <table  style="border: 1px solid #000; background: #fff">
        <tr bgcolor=#ccc>
        <th>Annotator</th>
		<th>#Annotated Docs</th>
		<th>Role</th>
		</tr>

<?php  while($row = mysql_fetch_row($resultUsers)) {
  	$ann = array();
  	$ann['id'] = $row[0];
  	$ann['username'] = $row[1];
  	$ann['status']=$row[2]; //annotator = annotator | admin = manager = gold annotation
  	$ddd =safe_query("SELECT count(num) FROM sentence, annotation , done WHERE sentence.gold='N' and task_id = '$id' AND num=annotation.sentence_num and num = done.sentence_num and annotation.user_id= done.user_id and done.user_id in (".$ann['id'].") and done.completed = 'Y'");
  	$docount= 0;
  	if (mysql_num_rows($ddd) > 0) {
		$dddrow = mysql_fetch_row($ddd);
		$docount= $dddrow[0];
	}
  	print '<tr><td>'.$ann['username'].'</td><td><center>'.$docount .'</center></td><td>'.$ann['status'].'</td></tr>';
  	if($ann['status']=='annotator'){
  	 array_push($annotators,$ann);
  	}else{
  		array_push($managers,$ann);
  	}
  }
  print "</table></div>";
   //	var_dump($annotators); print "</br>";
   	for ($x = 0; $x <= sizeof($annotators); $x++) {
    	for ($i = ($x+1); $i < sizeof($annotators); $i++) {
    		print '</br></br>'.'<b>Agreement for ('.$annotators[$x]['username'].', '.$annotators[$i]['username'].')</b></br>';
    		$annotatorsarr=array($annotators[$x]['id'],$annotators[$i]['id']);
    		getIAA($id,$annotatorsarr);
    	}
	} 
	 print '</br></br>';
	
		foreach($annotators as $ann){
		?>
		<hr>
 		<b>Average agreement for <?php print $ann['username']; ?>:</b></br>
 		<div  style="bgcolor:#ccc" id="agree-dice-avg-<?php print $ann['id']; ?>"></div>
 		<div  style="bgcolor:#ccc" id="agree-cohen-avg-<?php print $ann['id']; ?>"></div>
		</br>
 		<?php		  
		 }
		 
		
 
$usrs="";

/* IAA between all the annotators majority measure.
$annotatorsarr=array();
for ($i = 0; $i < sizeof($annotators); $i++) {
if(strlen($usrs)>0){
  $usrs.=",";
  }
  $usrs.=$annotators[$i]['username'];
   array_push($annotatorsarr,$annotators[$i]['id']);
}
print '</br></br>Agreement for ('.$usrs.')</br>';
getIAA($id,$annotatorsarr);

*/






 //getIAA($id,$uin);
 
 //	$resultUsers = safe_query("SELECT COUNT( DISTINCT annotation.sentence_num, annotation.user_id) as count FROM annotation,sentence WHERE annotation.sentence_num = sentence.num AND user_id = '$id' GROUP BY sentence_num, user_id"); 
  	//if(token level divide by the number of unique eval ids, to get the correct list of documents, 
  //	if document level, as each document has just one annotation then no need to divide.
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	

  	
  	
function getIAA($id,$annotators){
 $uin="";
 foreach($annotators as $ann){
  if(strlen($uin)>0){
  $uin.=",";
  }
  $uin.=$ann;
 }

$query = "SELECT num,annotation.user_id ,eval FROM sentence, annotation , done WHERE sentence.gold= 'N' and task_id = '$id' AND num=annotation.sentence_num and num = done.sentence_num and annotation.user_id= done.user_id and done.user_id in (".$uin.") and done.completed = 'Y'";
//print '</br>'.$query .'</br>';
 $result = safe_query($query); 
$uniqueDocs=array();  //$uniqueDocs[docId][userID]=tag
$uniqueUsrs=array();  //$uniqueUsrs[userId]=docId

  if (mysql_num_rows($result) > 0) {
     while($row = mysql_fetch_row($result)) {
     if(!isset($uniqueDocs[$row[0]])){
            $uniqueDocs[$row[0]]=array();
     }
     array_push($uniqueDocs[$row[0]],$row[2]);
    /* if(!isset($uniqueDocs[$row[0]][$row[2]])){
            $uniqueDocs[$row[0]][$row[2]]=1;
     }else{
     	 $uniqueDocs[$row[0]][$row[2]]=$uniqueDocs[$row[0]][$row[2]]++;
     }*/
     
     if(!isset($uniqueUsrs[$row[1]])){
       $uniqueUsrs[$row[1]]=array();
     }
     array_push($uniqueUsrs[$row[1]],$row[0]);
      // var_dump($row);
     //  print "</br>";

    }
  }
  
 // var_dump($uniqueDocs);
 //     print "</br>";print "</br>";print "</br>";
  //select common documents
  $common = array();
  foreach($uniqueDocs as $docid=>$eval){
    if(sizeof($eval)==sizeof($annotators)){
     // $tt=array($docid,$eval);
    //  if(!isset($common[$docid])){
    	//array_push($common,$docid);
    	//}
    	$common[$docid]=$eval;
    }
  }
  
    //var_dump($common);
  //     print "</br>";
  
  if(count($common)>0){
    echo "There are ".sizeof($common)." documents in common.";

 // echo "This IAA based on ".sizeof($annotators)." user(s) annotated this task with a total of ".sizeof($common)." common document(s).";
       //print "</br>Users: ";
       //var_dump($uniqueUsrs);

	  // print "</br>Documents: ";
      // var_dump($uniqueDocs);
      //  print "</br>"; print "</br>";
        $agree=0;$disagree=0;
        ?>
        <div id="divCheckbox" style="display: none;">
        <table  style="border: 1px solid #000; background: #fff">
        <tr bgcolor=#ccc>
        <th>DocID</th>
        
        <?php if(sizeof($annotators)>2){ ?>
        <th>Majority-Tag</th>
        <th># Majority-Tag</th>
        <th># Annotators</th>
        <?php }?>
        <th>IAA</th>
        </tr>
       
        <?php 
        $taskinfo = getTaskInfo($id);
        $ranges = rangesJson2Array($taskinfo["ranges"]);

        $sumall=0;$sharedDocs=0;$commonAgreement=0;
       foreach($common as $docid=>$doc){
       //var_dump($doc);
        //	if(sizeof($doc)==2){
        	$sharedDocs++;
       		$keys=array();
       		foreach($doc as $k=>$v){
       		 if(!isset($keys[$v])){
       			//array_push($keys,$v);
       			$keys[$v]=1;
       		}else{
       			$keys[$v]++;
       		}
       		}
       		$sum=0;
       		$majority=-1;$majority_key="";
       		//var_dump($keys);
       		foreach($keys as $k=>$v){
       			$sum += $v;
       			if($v>$majority){
       			$majority=$v;
       			$majority_key=$k;
       			}
       		}
       	//	var_dump($doc); print "</br>";
       		
       		$doc['iaa']= number_format((float)($majority/($sum)), 2, '.', '');
       		$doc['majority']= $majority;
       		$doc['sum']=$sum;
       		if($doc['iaa'] == 1)
       		   $commonAgreement++;
       		$sumall+=$doc['iaa'];
       		print " <tr>";
       		print "<td>".$docid."</td>";
       	 if(sizeof($annotators)>2){ 
       		print "<td bgcolor='".$ranges[$majority_key][1]."'>". $ranges[$majority_key][0]."</td><td>".$majority."</td>"."<td>".$doc['sum']."</td>";
       		}
       		print "<td>".$doc['iaa']."</td></tr>";
      // 	}
        } ?>

         <tr bgcolor=#ccc>
          <?php if(sizeof($annotators)>2){ ?>
                      <th></th>
      				  <th></th>
      				  <th></th>
   		     <?php } ?>
       
        <th>Avg Co-Efficient IAA:</th>
        <th> <?php echo number_format((float)($sumall/$sharedDocs), 2, '.', ''); ?> </th>
        </tr>
        </table>
        </div>
        <div id="divCheckbox2" style="display: none;">
        <table  style="border: 1px solid #000; background: #fff">
        <tr bgcolor=#ccc>
        <th>DocID</th>
        
        <?php if(sizeof($annotators)>2){ ?>
        <th>Majority-Tag</th>
        <th># Majority-Tag</th>
        <th># Annotators</th>
        <?php } ?>
        <th>IAA</th>
        </tr>
        
		<?php 
		$allDocs=array();
		$myquery="select sentence.num from sentence where (SELECT COUNT(*) from annotation,done,usertask,user where usertask.user_id= user.id and user.id=annotation.user_id and user.status='annotator' and usertask.task_id='".$id."' and annotation.sentence_num=sentence.num and sentence.num = done.sentence_num and annotation.user_id= done.user_id and done.completed = 'Y' )>1 and sentence.task_id='".$id."' and type='source' and sentence.gold='N'";
        //and annotation.user_id in (".$uin.")
       $myresult = safe_query($myquery);	
       $tre=0;
		if (mysql_num_rows($myresult) > 0) {
			while($myrow = mysql_fetch_row($myresult)) {
				$allDocs[$myrow[0]]="";
			}
			$tre= sizeof($allDocs);
		}
		
		
		$myquery="select sentence_num from annotation where annotation.user_id in (".$uin.")";
       $myresult = safe_query($myquery);	
       $treB=0;
       $docAnnotated=array();
		if (mysql_num_rows($myresult) > 0) {
		 while($myrow = mysql_fetch_row($myresult)) {
				$docAnnotated[$myrow[0]]="";
		}
			foreach($allDocs as $k=>$v){
				if(isset($docAnnotated[$k])){
				 $treB++;
				}
			}
		}
		?>
		 <tr bgcolor=#ccc>
          <?php if(sizeof($annotators)>2){ ?>
                      <th></th>
      				  <th></th>
      				  <th></th>
   		     <?php } ?>
       
        <th>Weight:</th>
        <th> <?php echo sizeof($common)."/".$tre."=".number_format((float)(sizeof($common)/$tre), 2, '.', ''); ?> </th>
        </tr>
		
		 <tr bgcolor=#ccc>
          <?php if(sizeof($annotators)>2){ ?>
                      <th></th>
      				  <th></th>
      				  <th></th>
   		     <?php } ?>
       
        <th>WeightB:</th>
        <th> <?php echo sizeof($common)."/".$treB."=".number_format((float)(sizeof($common)/$treB), 2, '.', ''); ?> </th>
        </tr>

<?php 
	//IAA Cohen Kappa
		
		$mm="";$mm2="";
		$uin="";
 		foreach($annotators as $ann){
		  $uin.='a'.$ann;
		 }
		 
		 
		 ?>
		<tr bgcolor=#ccc>
          <?php if(sizeof($annotators)>2){ ?>
                      <th></th>
      				  <th></th>
      				   <th></th>
   		     <?php } ?>
       
        <th>Cohen's Kappa agreement:</th>
        <th> <div class="agreex" id="agreex-<?php echo $uin; ?>"></div> </th>
        </tr>
		</table>
		</div>
		</br>
		Dice coefficient: <?php echo $commonAgreement ."/".sizeof($common)." = <div id='dice-".$uin."a' style='display: inline' class='dice'>".number_format((float)($commonAgreement/sizeof($common)), 2, '.', '').'</div>'; ?>
		</br>
		<div class="agree" id="agree-<?php echo $uin; ?>">Cohen’s Kappa: </div>
		
		
		<?php 
		 print '<div id="cohendiv">';
		print '<div id ="data"><script>';
		foreach($common as $docid=>$doc){
		  if(strlen($mm)>0){
 			 $mm.=",";
  			}
  			if(strlen($mm2)>0){
 			 $mm2.=",";
  			}
  			$mm .= $docid.':"'.$ranges[$doc[0]][0].'"';
  			$mm2 .= $docid.':"'.$ranges[$doc[1]][0].'"';
		}
		print ' var '.$uin.'a1={'.$mm.'}; ';
		print ' var '.$uin.'a2={'.$mm2.'}; ';
		$kkk="";
		
		foreach($ranges as $k=>$v){
			if(strlen($kkk)>0){
 			 $kkk.=",";
  			}
  			$kkk.="'".$v[0]."'";
		}
		print " var cats = [".$kkk."]; ";
		print ' var numeric1 = Cohen.nominalConversion(cats, '.$uin.'a1); ';
		print ' var numeric2 = Cohen.nominalConversion(cats, '.$uin.'a2); ';
		print " var kappDiagnose = Cohen.kappa(numeric1, numeric2, ".sizeof($ranges).", 'none'); ";
		print ' console.log("Converted nominal diagnoses, unweighted annotators('.$uin.') : " + kappDiagnose); ';
		print " document.getElementById('agree-".$uin."').innerText = 'Cohen’s Kappa: '+kappDiagnose; ";
		print '</script></div>';
		print '</div>';
		//var_dump($common);
		
	}else{
	  echo "There are 0 documents in common.";
	}
} 

?>



 </br></br>
 <hr>
 <b>Average overall:</b></br>
 		<div  style="bgcolor:#ccc" id="agree-dice-avg"></div>
 		<div  style="bgcolor:#ccc" id="agree-avg"></div>
 		
		
		</br></br></br>
<script>
 var cusid_ele = document.getElementsByClassName('agree');
 var counter = 0;
for (var i = 0; i < cusid_ele.length; ++i) {
    var item = cusid_ele[i]; 
    var txt=item.innerText; 
   counter += parseFloat( txt.replace('Cohen’s Kappa: ','') );
}


 var diceelm = document.getElementsByClassName('dice');
 var counterdice = 0;
 var annotators = {};
 var annotatorstimes = {};
for (var i = 0; i < diceelm.length; ++i) {
    var item = diceelm[i]; 
    var txt=item.innerText; 
    var vals=parseFloat( txt );
   counterdice += vals;
   
  var annos = (item.id.replace('dice-','')).split('a');
  if(annotators[annos[1]]== undefined){
  annotators[annos[1]]=0;
  annotatorstimes[annos[1]]=0;
  }
  if(annotators[annos[2]]== undefined){
  annotators[annos[2]]=0;
  annotatorstimes[annos[2]]=0;
  }
   annotators[annos[1]]+=vals;
   annotators[annos[2]]+=vals;
   annotatorstimes[annos[1]]++;
   annotatorstimes[annos[2]]++;
}


for (var key in annotators) {
  // console.log("key " + key + " has value " + myArray[key]);
 
  console.log(annotators);
  console.log(annotatorstimes);
  var hhh= "Average Dice coefficient: "+Number(annotators[key]).toFixed(2)+"/"+annotatorstimes[key]+" = "+Number((annotators[key]/annotatorstimes[key])).toFixed(2);
  console.log(hhh);
  console.log(key);

  document.getElementById('agree-dice-avg-'+key).innerText = hhh;
}








document.getElementById('agree-dice-avg').innerText = "Average Dice coefficient: "+Number(counterdice).toFixed(2)+"/"+diceelm.length+" = "+Number((counterdice/diceelm.length)).toFixed(2);




 var diceelm = document.getElementsByClassName('agree');
 var counterdice = 0;
 var annotators = {};
 var annotatorstimes = {};
for (var i = 0; i < diceelm.length; ++i) {
    var item = diceelm[i]; 
    var txt=item.innerText; 
    var vals=parseFloat( txt.replace('Cohen’s Kappa: ','') );
   counterdice += vals;
   
  var annos = (item.id.replace('agree-','')).split('a');
  if(annotators[annos[1]]== undefined){
  annotators[annos[1]]=0;
  annotatorstimes[annos[1]]=0;
  }
  if(annotators[annos[2]]== undefined){
  annotators[annos[2]]=0;
  annotatorstimes[annos[2]]=0;
  }
   annotators[annos[1]]+=vals;
   annotators[annos[2]]+=vals;
   annotatorstimes[annos[1]]++;
   annotatorstimes[annos[2]]++;
}


for (var key in annotators) {
  // console.log("key " + key + " has value " + myArray[key]);
 
  console.log(annotators);
  console.log(annotatorstimes);
  var hhh= "Average Cohen’s Kappa: "+Number(annotators[key]).toFixed(2)+"/"+annotatorstimes[key]+" = "+Number((annotators[key]/annotatorstimes[key])).toFixed(2);
  console.log(hhh);
  console.log(key);

  document.getElementById('agree-cohen-avg-'+key).innerText = hhh;
}
document.getElementById('agree-avg').innerText = "Average Cohen’s Kappa: "+Number(counter).toFixed(2)+"/"+cusid_ele.length+" = "+Number((counter/cusid_ele.length)).toFixed(2);

</script>

