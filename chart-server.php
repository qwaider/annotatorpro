<!doctype html>
<html>
<head>
<title>Line Chart</title>
<script src="js/Chart.min.js"></script>
</head>
<body>
	<table>
		<tr>
			<th><img alt="" src="icons/it"></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table>
	<div style="width: 30%">
		<div>
			<canvas id="canvasP" height="450" width="600"></canvas>
			<b align="center">ITALIAN</b>
		</div>
	</div>
	<div style="width: 30%">
		<div>
			<canvas id="canvasR" height="450" width="600"></canvas>
			<b align="center">GERMAN</b>
		</div>
	</div>
	<div style="width: 30%">
		<div>
			<canvas id="canvas" height="450" width="600"></canvas>
			<b align="center">ENGLISH</b>
		</div>
	</div>

<?php
include ("config.php");
include ("functions.php");
$arr = get ( "f1measures" );
$arrrecallmeasures = get ( "recallmeasures" );
$arrprecisionmeasures = get ( "precisionmeasures" );
?>
	<script>
		var lineChartData = {
				
			labels : [<?php echo $arr['labels']; ?>],
			datasets : [
				{
					label: "Overall",
					fillColor : "rgba(191, 28, 196,0.2)",
					strokeColor : "rgba(191, 28, 196,1)",
					pointColor : "rgba(191, 28, 196,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [<?php echo $arr['vals']; ?>]
				}
				,
				{
					label: "PER category",
					fillColor : "rgba(196, 28, 101,0.2)",
					strokeColor : "rgba(196, 28, 101,1)",
					pointColor : "rgba(196, 28, 101,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [<?php echo $arr['valsP']; ?>]
				}
				,
				{
					label: "LOC category",
					fillColor : "rgba(73, 196, 28,0.2)",
					strokeColor : "rgba(73, 196, 28,1)",
					pointColor : "rgba(73, 196, 28,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [<?php echo $arr['valsL']; ?>]
				}
				,
				{
					label: "ORG category",
					fillColor : "rgba(145, 187, 232,0.2)",
					strokeColor : "rgba(145, 187, 232,1)",
					pointColor : "rgba(145, 187, 232,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [<?php echo $arr['valsO']; ?>]
				}
			]

		}


		var lineChartDatarecallmeasures = {
				
				labels : [<?php echo $arrrecallmeasures['labels']; ?>],
				datasets : [
					{
						label: "Overall",
						fillColor : "rgba(191, 28, 196,0.2)",
						strokeColor : "rgba(191, 28, 196,1)",
						pointColor : "rgba(191, 28, 196,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(220,220,220,1)",
						data : [<?php echo $arrrecallmeasures['vals']; ?>]
					}
					,
					{
						label: "PER category",
						fillColor : "rgba(196, 28, 101,0.2)",
						strokeColor : "rgba(196, 28, 101,1)",
						pointColor : "rgba(196, 28, 101,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(151,187,205,1)",
						data : [<?php echo $arrrecallmeasures['valsP']; ?>]
					}
					,
					{
						label: "LOC category",
						fillColor : "rgba(73, 196, 28,0.2)",
						strokeColor : "rgba(73, 196, 28,1)",
						pointColor : "rgba(73, 196, 28,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(151,187,205,1)",
						data : [<?php echo $arrrecallmeasures['valsL']; ?>]
					}
					,
					{
						label: "ORG category",
						fillColor : "rgba(145, 187, 232,0.2)",
						strokeColor : "rgba(145, 187, 232,1)",
						pointColor : "rgba(145, 187, 232,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(151,187,205,1)",
						data : [<?php echo $arrrecallmeasures['valsO']; ?>]
					}
				]

			}


		var lineChartDataprecisionmeasures = {
				
				labels : [<?php echo $arrprecisionmeasures['labels']; ?>],
				datasets : [
					{
						label: "Overall",
						fillColor : "rgba(191, 28, 196,0.2)",
						strokeColor : "rgba(191, 28, 196,1)",
						pointColor : "rgba(191, 28, 196,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(220,220,220,1)",
						data : [<?php echo $arrprecisionmeasures['vals']; ?>]
					}
					,
					{
						label: "PER category",
						fillColor : "rgba(196, 28, 101,0.2)",
						strokeColor : "rgba(196, 28, 101,1)",
						pointColor : "rgba(196, 28, 101,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(151,187,205,1)",
						data : [<?php echo $arrprecisionmeasures['valsP']; ?>]
					}
					,
					{
						label: "LOC category",
						fillColor : "rgba(73, 196, 28,0.2)",
						strokeColor : "rgba(73, 196, 28,1)",
						pointColor : "rgba(73, 196, 28,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(151,187,205,1)",
						data : [<?php echo $arrprecisionmeasures['valsL']; ?>]
					}
					,
					{
						label: "ORG category",
						fillColor : "rgba(145, 187, 232,0.2)",
						strokeColor : "rgba(145, 187, 232,1)",
						pointColor : "rgba(145, 187, 232,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(151,187,205,1)",
						data : [<?php echo $arrprecisionmeasures['valsO']; ?>]
					}
				]

			}
			
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myLine = new Chart(ctx).Line(lineChartData, {
			responsive: true
		});


		var ctx2 = document.getElementById("canvasP").getContext("2d");
		window.myLine = new Chart(ctx2).Line(lineChartDataprecisionmeasures, {
			responsive: true
		});

		
		var ctx3 = document.getElementById("canvasR").getContext("2d");
		window.myLine = new Chart(ctx3).Line(lineChartDatarecallmeasures, {
			responsive: true
		});
	}


	</script>
</body>
</html>
