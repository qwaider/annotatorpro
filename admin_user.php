

<head>
<style>
li.row {
	padding-top: 1px;
	border-bottom: 1px solid #fff;
}

li.row:hover {
	padding-top: 1px;
	border-bottom: 1px solid #090;
	background:#BAE3E0;
}
li.selected {
	background:#E0A4AA;
	padding-right:4px;
	border-bottom: 1px solid #5c0120;
	font-size:14px;
	color: #600;
}
</style>
</head>

<div style='margin: 10px; vertical-align: top; top: 0px; display: inline-block'>
<div style='float: left; position: absolute; vertical-align: top; left: 1px; top: 0px; display: inline-block; padding-right: 0px; margin: 0px'>
<span class='spinner'><img width=25 src='img/spinner.gif' valign=bottom></span>
<?php
if ($mysession["status"] == "root" || 
	$mysession["status"] == "manager" || 
	$mysession["status"] == "annotator") { 
	$userlist = getUserStats($mysession["userid"],$mysession["status"]);

	$sentlabel="Create";
	$cancelbutton="<input type=button onclick=\"javascript:window.open('admin.php?section=user','_self');\"  value='Cancel'> ";
	$visibility_tform="visible";
	$userinfo = array("name" => "",
				  "username" => "",
				  "password" => "",
				  "email" => "",
				  "status" => "",
				  "team" => "",
				  "activated" => "N",
				  "viewagree" => "N",
				  "notes" => "");
	
	if (isset($id) && ($id<0 || array_key_exists($id, $userlist))) {
		$query = "";
		if (isset($action) && $action="remove") {		
			if (removeUser($id) == 1) {
				$id=-1;
				print "<script>alertify.alert('The user information and all his annotations have been removed correctly.'); </script>"; 	
				print "<button style='white-space: nowrap; position: absolute; margin-left: 40px; top: 10px; float: left;' onclick=\"showForm(this);\">Create a new user</button>";
				$visibility_tform="hidden";
			}
		} else {
		    if ($id == -1 || (isset($update) && $update=="Update")) {
				while (list ($key,$val) = each($userinfo)) {
					if (isset($$key)) {
						if ($key == "activated" && $$key == "on") {
							$$key = "Y";
						} else if ($key == "viewagree" && $$key == "on") {
							$$key = "Y";
						}
						$userinfo[$key] = trim($$key);
						#print "$key: ".$userinfo[$key] ."<br>";
					}
					$query .= ",$key=\"".$userinfo[$key]."\"";				
				}
			} else {
				$userinfo = getUserInfo($id);
				$sentlabel = "Update";
				$cancelbutton = "<input type=button onclick=\"javascript:window.open('admin.php?section=user','_self');\"  value='Cancel'> ";	
			}
			
			if ($id == -1) {
				//controllo che non esista già un utente conlo stesso usernaem
				$query = "select id from user WHERE username='".$userinfo["username"]."'";
				$result = safe_query($query);	
				if (mysql_num_rows($result) > 0) {
					print "<div style='white-space: nowrap; float: left; border: 1px solid #444; background: lightyellow; display: inline; left: 10px; position: relative; top: 5px; left: 30px'>";
					print "<font color=red>WARNING!</font> This username is already present.<br>";
					print "</div>";
				} else {
				if ($userinfo["name"] != "" && $userinfo["username"] != "" && $userinfo["password"] != "") {
					$res = safe_query("INSERT INTO user (name,username,password,email,status,team,activated,notes,registered,refuser,viewagree) VALUES ('".$userinfo["name"]."','".$userinfo["username"]."','".$userinfo["password"]."','".$userinfo["email"]."','".$userinfo["status"]."','".$userinfo["team"]."','".$userinfo["activated"]."','".$userinfo["notes"]."',now(),".$mysession["userid"].",'".$userinfo["viewagree"]."');");
					
					//$res = safe_query("INSERT INTO user (name,registered,refuser) VALUES ('_NEW_',now(),".$mysession["userid"].");");
					if ($res == 1) {
						$id = mysql_insert_id();
						removeUserTask($id, 0);
						if (isset($utasks) && is_array($utasks)) {
							foreach ($utasks as $tid) {
								addUserTask($id ,$tid);
							}
						}
				
						print "<script>window.open(\"admin.php?section=user\", \"_self\");</script>"; 				
					} else {
						print "<img src='img/database_error.png'> ERROR! This user has not been saved correctly.<br>"; 
					}
				} else {
					print "<div style='white-space: nowrap; float: left; border: 1px solid #444; background: lightyellow; display: inline; left: 10px; position: relative; top: 5px; left: 30px'>";
					if ($userinfo["name"] == "") {
						print "<font color=red>WARNING!</font> The name is mandatory.<br>";
					} else if ($userinfo["username"] == "") {
						print "<font color=red>WARNING!</font> The username is mandatory.<br>";
					} else if ($userinfo["password"] == "") {
						print "<font color=red>WARNING!</font> The password is mandatory.<br>";
					} 
					print "</div>";
					$cancelbutton = "<input type=button onclick=\"javascript:window.open('admin.php?section=user','_self');\"  value='Cancel'> ";
				}
				}
			} else {
			    //update the user information
				$res = safe_query("UPDATE user SET name='".$userinfo["name"]."', username='".$userinfo["username"]."',password='".$userinfo["password"]."',email='".$userinfo["email"]."',status='".$userinfo["status"]."',team='".$userinfo["team"]."',activated='".$userinfo["activated"]."',viewagree='".$userinfo["viewagree"]."',notes='".$userinfo["notes"]."' WHERE id=$id;");
				if (isset($utasks) && $utasks != null) {
					removeUserTask($id, 0);
			    	if (isset($utasks) && is_array($utasks)) {
						foreach ($utasks as $tid) {
							addUserTask($id ,$tid);
						}
					}
				}
			}
			if ($id != -1) {
				$sentlabel = "Update";
				$cancelbutton = "<input type=button onclick=\"javascript:window.open('admin.php?section=user','_self');\"  value='Cancel'> ";	
			}
		}
	} else {
		$id = -1;
		print "<button id='newuser' style='white-space: nowrap; position: absolute; margin-left: 40px; top: 10px; float: left;' onclick=\"showForm(this);\" disabled>Create a new user</button>";
		$visibility_tform="hidden";
	}
?>
</div>
<div style='display: inline-block; float: right; right: 0px; padding: 2px; white-space: nowrap; max-height: 350px; overflow: auto; border: 1px solid #999; max-width: 200px; margin-right; -40px; margin-top: 25px'>
<!-- <div style="white-space: nowrap; float: left; left: 0px; padding-left: 2px; display: inline-block; position: relative; top: 20px"> -->
<?php	
$userlist = getUserStats($mysession["userid"],$mysession["status"]);
$prev_status = "";
if (count($userlist) > 0) {
	while (list ($uid,$uarr) = each($userlist)) {
		if ($uid == "") {
			continue;
		}
		$tasknum = count(getTasks($uid));
		if ($uarr[3] != $prev_status) {
			if ($prev_status != "") {
				print "<br>";
			}
			$prev_status = $uarr[3];
			print "<b>".ucfirst($prev_status)." users</b><hr>";
		}
		if (isset($id) && $id == $uid) {
			//print "<div style='position: absolute; padding-bottom: 3px; left:0px; background: #5c0120'>&nbsp;&nbsp;</div>
			print "<li type=square class=selected>";
		} else {
			print "<li type=square class=row>";
		}
		if ($mysession["userid"] != $uid) {
			print "<a href=\"javascript:delUser($uid);\"><img border=0 width=12 src='img/remove.png'></a> ";
		} else {
			print "&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		if ($uarr[2] == "N") {
			print "<s><font color=#666 title='this user is not active'>".$uarr[0]."</font></s>";
		} else {
			print $uarr[0];
		}
		print " - <a href='admin.php?section=user&id=$uid' title='tasks: $tasknum";
		if ($tasknum > 0) {
			$doneInfo = getUserLastDone($uid);
			print "\ndone: ".$doneInfo[0];
			if ($doneInfo[0] > 0) {
		 		print "\nlast: ".$doneInfo[1];
			}
		}
		print "'>". $uarr[1]."</a></li>";
		
	}	
}
?>

<br><br>
</div>
</div>
<div style='display: inline-block; padding-bottom: 2px;'>


<form id="tform" style='margin-right: -2px; border-bottom: 2px solid #5c0120; visibility: <?php echo $visibility_tform; ?>' name="tform" heigth=80 action="admin.php?section=user" method="post" enctype="multipart/form-data">
  <input type=hidden name="id" value="<?php echo $id; ?>">
  <table border=0 cellspacing=0 cellpadding=4>
  <tr><th bgcolor=#ddd align=right>Full name<font color=darkred>*</font>:</th><td><input TYPE=text name="name" size=30 value="<?php echo $userinfo['name']; ?>"></td></tr>
  <tr><th bgcolor=#ddd align=right>User name<font color=darkred>*</font>:</th><td><input TYPE=text name="username" size=15 value="<?php echo $userinfo['username']; ?>"></td></tr>
  <tr><th bgcolor=#ddd align=right>Password<font color=darkred>*</font>:</th><td><input TYPE=password name="password" size=15 value="<?php echo $userinfo['password']; ?>"></td></tr>
  <tr><th bgcolor=#ddd align=right>E-mail:</th><td><input TYPE=text name="email" size=30 value="<?php echo $userinfo['email']; ?>"></td></tr>
  <tr><th bgcolor=#ddd align=right>User type: </th><td><select name="status">
  <?php
  	
	foreach ($userPermissions[$mysession['status']] as $utype) {
		print "<option value='$utype'";
		if ($utype == $userinfo['status']) {
			print " selected";
		}
		print "> ".ucfirst(strtolower($utype));
	}
	if ($id == $mysession['userid'] && !in_array($mysession['status'],$userPermissions[$mysession['status']])) {
		print "<option value='{$mysession['status']}' selected> ". ucfirst(strtolower($mysession['status']));
	}
  ?>
  </select></td></tr>
 <tr><th bgcolor=#ddd align=right>Team:</th><td><input TYPE=text name="team" value="<?php echo $userinfo['team']; ?>"></td></tr>
 <tr><th bgcolor=#ddd align=right valign=top>Notes:</th><td> <textarea rows="4" cols="30" name="notes" value="<?php echo $userinfo['notes']; ?>"><?php if (isset($notes)) { echo $notes;} ?></textarea></td></tr>
 <tr><th bgcolor=#ddd align=right valign=top>Tasks:</th><td><i><font size=-1>(hold CTRL for multiple selection)</font></i><br>
  <select multiple="multiple" name="utasks[]" size=8 style='font-size: 13px'><option value=''>none

<?php	
	$allTasks = getTasks($id);
	
	$ttype = "";
	$tasklist = getTasks($mysession["userid"]);
  	while (list ($tid,$tarr) = each($tasklist)) {	
  		if ($tarr[1] != $ttype) {
			$ttype = $tarr[1];
			print "<option value='' disabled='disabled'>--- ".ucfirst($ttype) ." tasks --- \n";
		}
			
		print "<option value='$tid'";
		if (isset($allTasks[$tid]) || in_array($tid, $utasks)) {
			print " selected";
		}	
		print "> ".$tarr[0];
	}	
?>
</select></td></tr>
<tr><th bgcolor=#ddd align=right>Active:</th><td> <input type="checkbox" name="activated"
<?php
	if ($userinfo['activated'] == "Y") {
		print " checked";
	}
?>
></td></tr>
<tr><th bgcolor=#ddd align=right>Allow to see agreements:</th><td> <input type="checkbox" name="viewagree"
<?php
	if ($userinfo['viewagree'] == "Y") {
		print " checked";
	}
?>
></td></tr>
<tr><td align=right colspan=2 align=center><?php echo $cancelbutton; ?> <input type="submit" name=update value="<?php echo $sentlabel; ?>"></td></tr>

  </table>
</form>
</div>
<?php
	print "</div>";
} else {
	print "WARNING! You don't have enough permission to modify user accounts.";
}
?>
<script>
$('.spinner').hide();
var button = document.getElementById('newuser');
if (button != null) {
	button.disabled = false;
}
</script>