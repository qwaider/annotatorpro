<?php 
include("config.php");
include("functions.php");
$arr= getErrorList(); //id,error,confidence,error_counter
?>
<!doctype html>
<html>
	<head>
		<title>Error Queue</title>
	</head>
	<body>
	<table style="border: 1px solid #000; background: #fff">
	<tr bgcolor=#ccc><td align=center>memory error</td><td align=center>confidence</td><td align=center>#selections</td></tr>
		<?php
		  
		  foreach ( $arr as $entry){
		    print '<tr>';
		    
		    print '<td style="border: 1px solid black;">'.str_replace("\\b","",$entry[0]).'</td>';
		     print '<td style="border: 1px solid black;">'.str_replace("\\b","",number_format((float)$entry[1], 4, '.', '')).'</td>';

		    print '<td style="border: 1px solid black;">'.str_replace("\\b","",$entry[2]).'</td>';

		    
		    print '</tr>';
		  }
		
		?>
		</table>	
	</body>
</html>
