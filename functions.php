<?php


secure_session_start();
include("initdb.conf");
# get user annotation statistics
function getUserStats($user_id, $user_status) {
	$hash = array();
	if ($user_status != "annotator") {
		//get the tasks of the admin
		$admintasks = getUserTasks($user_id);
		$adminTaskIDs = array_keys($admintasks);
		if ($user_status == "manager" || $user_status == "annotator") {
			//se questa query ritorna un insieme vuoto vuol dire che non e' stato ancora associato nessun task
			$query = "select id,username,name,activated,status from user RIGHT JOIN (select distinct user_id from usertask RIGHT JOIN (select task_id from usertask where user_id=$user_id) AS b ON usertask.task_id=b.task_id) AS c ON user.id=c.user_id AND (user.status NOT LIKE '%manager%' AND user.id IS NOT NULL) OR refuser=$user_id OR user.id=$user_id ORDER BY status,username;";
		} else {
			$query = "select id,username,name,activated,status from user WHERE id IS NOT NULL order by status,username;";
		} 
		#$query = "select id,username,name,count(*),activated,status,refuser from user left join annotation on annotation.user_id=user.id group by id order by status,username;";
		$result = safe_query($query);	
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_row($result)) {
				if (!empty($row[0])) {
					$hash[$row[0]] = array($row[1],$row[2],$row[3],$row[4]);
				}	
			}
		}
	} 
	$query = "select id,username,name,activated,status from user WHERE id=$user_id or refuser=$user_id";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_row($result)) {
			if (!empty($row[0])) {
				$hash[$row[0]] = array($row[1],$row[2],$row[3],$row[4]);
			}	
		}
	}
	return $hash;
}

function getUserTasks($userid) {
	$query = "SELECT task_id,name,type FROM usertask LEFT JOIN task ON usertask.task_id=task.id WHERE user_id='$userid'";
	$result = safe_query($query);
	$hash = array();	
	while ($row = mysql_fetch_row($result)) {
		$hash[$row[0]] = array($row[1], $row[2]);
	}
	return $hash;
}

function getUserInfo($userid) {
	$query ="SELECT * FROM user WHERE id='$userid'";
	$result = safe_query($query);
	if (mysql_num_rows($result) == 1) {
		return mysql_fetch_array($result);
	}
	return array();
}

#remove user info, and all down annotations 
function removeUser($userid) {
	$query ="DELETE FROM annotation WHERE user_id=$userid";
	if (safe_query($query) == 1) {
		$query ="DELETE FROM comment WHERE user_id=$userid";
		if (safe_query($query) == 1) {
			$query ="DELETE FROM done WHERE user_id=$userid";
			if (safe_query($query) == 1) {
				$query ="DELETE FROM usertask WHERE user_id=$userid";
				if (safe_query($query) == 1) {
					$query ="DELETE FROM user WHERE id=$userid";
					if (safe_query($query) == 1) {
						return 1;
					}
				}
			}
		}	
	}
	return 0;
}

function getTaskInfo($taskid) {
	$query ="SELECT * FROM task WHERE id='$taskid'";
	$result = safe_query($query);
	if (mysql_num_rows($result) == 1) {
		return mysql_fetch_array($result);
	}
	return array();
}
	
#remove task info, sentences belongs to it and all user annotations 
function removeTask($taskid) {
	$query ="DELETE annotation FROM annotation left join sentence ON annotation.sentence_num=sentence.num WHERE task_id=$taskid";
	if (safe_query($query) == 1) {
		$query ="DELETE comment FROM comment left join sentence ON comment.sentence_num=sentence.num WHERE task_id=$taskid";
		if (safe_query($query) == 1) {
			$query ="DELETE done FROM done left join sentence ON done.sentence_num=sentence.num WHERE task_id=$taskid";
			if (safe_query($query) == 1) {
				$query ="DELETE FROM sentence WHERE task_id=$taskid";
				if (safe_query($query) == 1) {
					$query ="DELETE FROM usertask WHERE task_id=$taskid";
					if (safe_query($query) == 1) {
						$query ="DELETE FROM task where id=$taskid";
						if (safe_query($query) == 1) {
							return 1;
						}
					}
				}
			}
		}	
	}
	return 0;
}
	
function removeUserTask($userid, $taskid) {
	if ($userid > 0) {
		$query ="DELETE FROM usertask WHERE user_id=$userid";
		if (safe_query($query) == 1) {
			return 1;
		}
	} else if ($taskid > 0) {
		$query ="DELETE FROM usertask WHERE task_id=$taskid";
		if (safe_query($query) == 1) {
			return 1;
		}
	}
	return 0;
}

function addUserTask($userid, $taskid) {
	if (!empty($userid) && !empty($taskid)) {
		$query ="INSERT INTO usertask VALUES ($userid,$taskid) ON DUPLICATE KEY UPDATE task_id=$taskid";
		if (safe_query($query) == 1) {
			return 1;
		}
	}
	return 0;
}

function getAnnotationTaskStats() {
	$query = "select task_id,count(*) from annotation left join sentence on annotation.sentence_num=sentence.num group by task_id";
	
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = $row[1];
		}
	}
	return $hash;
}


#check if there are some annotation for a particular task
function isAnnotatedTask ($taskid, $user_id=null) {
	$query="select distinct sentence_num,user_id from annotation left join sentence on annotation.sentence_num=sentence.num WHERE task_id=$taskid";
	if ($user_id != null) {
		$query .= " AND user_id=$user_id";
	}
	
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		return 1;
	}
	return 0;
}

function getDoneTaskStats() {
	$query = "select task_id,count(*) from done left join sentence on done.sentence_num=sentence.num group by task_id";
	
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = $row[1];
		}
	}
	return $hash;
}

function getDoneUserStats() {
	$query = "select user_id,count(*) from done group by user_id";
	
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = $row[1];
		}
	}
	return $hash;
}



#get last N sentence annotated by the users
# foreach sentence returns the sentence ID (as key of the hash), and the task ID and the last modified time (as array)
function getUserLastAnnotations($user_id, $limit = 2) {
	$query = "SELECT DISTINCT sentence_num,task_id,annotation.lasttime from annotation LEFT JOIN sentence ON annotation.sentence_num=sentence.num WHERE user_id='$user_id' and task_id!='' group by sentence_num order by annotation.lasttime desc limit $limit;";
	
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = array($row[1], $row[2]);
		}
	}
	return $hash;
}

function getUserLastDone($user_id) {
	$query = "SELECT count(lasttime), max(lasttime) from done where user_id='$user_id' order by lasttime desc";
	
	$result = safe_query($query);	
	return mysql_fetch_row($result);
}

# get sentence mapping between the source ID and the internal MySQL one
function getSourceSentenceIdMapping($task_id) {
	$query = "SELECT id,num FROM sentence WHERE task_id=$task_id AND type='source'";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = $row[1];
		}
	}
	return $hash;
}

# getSentenceID
function getSentenceID($num) {
	$query = "SELECT id FROM sentence WHERE (num='$num' );";
	$result = safe_query($query);	
	$hash = "";
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_array($result)) {
			$hash=$row["id"];
		}
	}
	return $hash;
}
# get info about a source sentence: text, reference, ..
function getSentence($num, $taskid) {
	$query = "SELECT type,lang,text,tokenization,id FROM sentence WHERE (num='$num' OR (linkto='$num' AND type='reference')) AND task_id='$taskid';";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_array($result)) {
			$hash[$row["type"]] = array($row["lang"],$row["text"], $row["tokenization"], $row["id"]);
		}
	}
	return $hash;
}
function getErrorSelection($num) {
	$query = "SELECT errorDerived FROM sentence WHERE (num='$num' OR (linkto='$num' AND type='reference'));";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_array($result)) {
			return $row["errorDerived"];
		}
	}
	return "Random File";
}




#get annotaion task: quality
function getQuality($sentence_num,$output_id,$user_id) {
	$query = "SELECT eval FROM annotation WHERE sentence_num='$sentence_num' AND output_id='$output_id' AND user_id=$user_id;";
	$result = safe_query($query);	
	#print mysql_num_rows($result)  . " -- ".$query;
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_array($result);
		return $row["eval"];
	}
	return -1;
}

#get annotaion task: errors
function getErrors($sentence_num,$output_id,$user_id) {
	$query = "SELECT eval,evalids,evaltext FROM annotation WHERE sentence_num=$sentence_num AND output_id=$output_id AND user_id=$user_id order by eval;";
	$result = safe_query($query);	
	//print mysql_num_rows($result)  . " -- ".$query;
	$hash_error = array();
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			$hash_error[$row["eval"]] = array($row["evalids"],$row["evaltext"]);
		}
	}
	return $hash_error;
}

#get annotaion task: alignment
function getSourceAlignment($sentence_num,$output_id,$user_id) {
	$query = "SELECT eval,evalids,evaltext FROM annotation WHERE sentence_num=$sentence_num AND output_id=$output_id AND user_id=$user_id order by eval;";
	$result = safe_query($query);	
	//print mysql_num_rows($result)  . " -- ".$query;
	$hash_error = array();
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			$hash_error[$row["eval"]] = array(preg_replace("/\-[0-9]+/","",$row["evalids"]),$row["evaltext"]);
		}
	}
	return $hash_error;
}

#get annotaion task: alignment
function getTargetAlignment($sentence_num,$output_id,$user_id) {
	$query = "SELECT eval,evalids,evaltext FROM annotation WHERE sentence_num=$sentence_num AND output_id=$output_id AND user_id=$user_id order by eval;";
	$result = safe_query($query);	
	//print mysql_num_rows($result)  . " -- ".$query;
	$hash_error = array();
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			$hash_error[$row["eval"]] = array(preg_replace("/[0-9]+\-/","",$row["evalids"]),$row["evaltext"]);
		}
	}
	return $hash_error;
}

#count annotations for a target
function countAnnotations($sentence_num,$output_id,$user_id) {
	$query = "SELECT LENGTH(evalids) - LENGTH(REPLACE(evalids,',',''))+1 FROM annotation WHERE sentence_num=$sentence_num AND output_id=$output_id AND user_id=$user_id order by eval;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$sum=0;
		while ($row = mysql_fetch_row($result)) {
			$sum += $row[0];
		} 
		return $sum;
	}
	return 0;
}

#count the annotated targets
function countAnnotatedSentences($sentence_num,$user_id = null) {
	$query = "SELECT distinct output_id FROM annotation WHERE sentence_num='$sentence_num' AND eval >= 0";
	if ($user_id != null) {
		$query .= " AND user_id=$user_id;";
	}
	return mysql_num_rows(safe_query($query));
}
	
function getUsedValues ($taskid) {
	$values = array();
	$query = "SELECT DISTINCT eval FROM annotation LEFT JOIN sentence ON annotation.sentence_num=sentence.num WHERE task_id=$taskid";
	$result = mysql_query($query);	
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_row($result)) {
			array_push($values, $row[0]);
		}
	}
	return $values;
}

#duplicate annotations: copy the annotations from taskid and fromuserid to the current userid (=curruserid)
function copyAnnotations ($curruserid, $taskid, $fromuserid) {
	$query="INSERT INTO annotation SELECT sentence_num, output_id, $curruserid, eval, evalids, evaltext, now() FROM annotation LEFT JOIN sentence ON annotation.sentence_num=sentence.num WHERE user_id=$fromuserid AND task_id=$taskid";
	mysql_query($query) or print ("ERROR! Records copying failed. (" . mysql_error() .")");	
	return mysql_affected_rows();
}
	
function getAnnotatedTasks ($userid) { 
	$hash = array();
	#$query="select distinct task_id, count(*) from annotation left join sentence on annotation.sentence_num=sentence.num where user_id=$userid group by task_id";
	$query="SELECT task_id,sum(LENGTH(evalids) - LENGTH(REPLACE(evalids, ',', ''))+1) FROM annotation LEFT JOIN sentence ON annotation.sentence_num=sentence.num WHERE user_id=$userid group by task_id";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_row($result)) {
			if (!empty($row[0])) {
				$hash[$row[0]] = $row[1];
			}
		}
	}    
	return $hash;
}

#get uniq pair task,user for annotation	except the user user_id
function getTaskAndUsers ($user_id) { 
	$hash = array();
	$query="select distinct task_id,user_id from annotation,sentence where annotation.sentence_num=sentence.num and user_id != $user_id order by task_id, user_id";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			if (array_key_exists($row["task_id"], $hash)) {
				$hash[$row["task_id"]] .= " " .$row["user_id"];
			} else {
				$hash[$row["task_id"]] = $row["user_id"];
			}
		}
	}
	return $hash ;
}

#get all annotation divided by task/user key
# task/user id | count(*)
#     1-22     |   2505
function getTaskAndUserAnnotation () { 
	$hash = array();
	$query="select concat(task_id,'-',user_id) as tu, count(*) as count from annotation, sentence where annotation.sentence_num=sentence.num group by tu order by tu";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			$hash[$row["tu"]] = $row["count"];
		}
	}
	return $hash ;
}

#save annotation task: quality
function saveQuality($source_id,$output_id,$user_id,$eval,$action="") {
	$query="";
	if ($action == "remove") {
		$evalclause="";
		if ($eval > -1) {
			$evalclause="AND eval=$eval";
		}
		$query = "DELETE FROM annotation WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id $evalclause";		
	} else if (getQuality($source_id,$output_id,$user_id) < 0) {	
		$query = "INSERT INTO annotation (sentence_num,output_id,user_id,eval,lasttime) VALUES ('$source_id','$output_id',$user_id,$eval,now())";		
	} else {
		$query = "UPDATE annotation SET eval='$eval',lasttime=now() WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id;";
	}
	
	safe_query("UPDATE done SET completed='N',lasttime=now() WHERE sentence_num='$source_id' AND user_id=$user_id;");
	return safe_query($query);		
}

#save annotation task: error
function removeError($id,$targetid,$user_id,$eval,$item) {
	$query = "SELECT evalids,evaltext FROM annotation WHERE sentence_num=$id AND output_id='$targetid' AND user_id=$user_id AND eval=$eval";
	$result = safe_query($query);	
	//print mysql_num_rows($result)  . " -- ".$query;
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		
		$ids = explode(",",$row[0]);
		$texts = explode("__BR__",$row[1]);
		for ($i=0; $i<count($ids); $i++) {
			if ($ids[$i] == $item || empty($ids[$i])) {
				unset($ids[$i]);
				unset($texts[$i]);				
			}	
		}
		
		#saveLog("#".join("__BR__",array_filter($texts)) ."<br>!! ".join(",",array_filter($ids)));
		$ids = array_filter($ids);
		$texts = array_filter($texts);
		
		if (count($ids) == count($texts)) {
			if(count($ids) == 0) {
				$query = "DELETE FROM annotation WHERE sentence_num=$id AND output_id='$targetid' AND user_id=$user_id AND eval=$eval;";
			} else {
				$query = "UPDATE annotation SET evalids='".join(",",$ids)."',evaltext=\"".addslashes(join("__BR__",$texts))."\",lasttime=now() WHERE sentence_num=$id AND output_id='$targetid' AND user_id=$user_id AND eval=$eval;";
			}
			safe_query($query);
			
			safe_query("UPDATE done SET completed='N',lasttime=now() WHERE sentence_num='$id' AND user_id=$user_id");
	
		} else {
			return 0;
		}
	}
	return 1;
}

//reset all annotations of an error category type 
function resetErrors($id,$targetid,$user_id,$check) {
	$query = "DELETE FROM annotation WHERE sentence_num=$id AND output_id='$targetid' AND user_id=".$user_id." AND eval=$check";
	safe_query($query);
	
	safe_query("UPDATE done SET completed='N',lasttime=now() WHERE sentence_num=$id AND user_id=$user_id");
}

#save annotation task: error
function saveErrors($source_id,$output_id,$user_id,$eval,$evalids="",$evaltext="") {
	$query="";
	if ($eval == -1) {
		$query = "DELETE FROM annotation WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id";
		safe_query($query);
	} else {
		$hash = getErrors($source_id,$output_id,$user_id);
		if (!isset($hash[$eval])) {
			$evaltext = str_replace("\"","&quot;",stripslashes(trim($evaltext)));	
			$query = "INSERT INTO annotation (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$source_id','$output_id',$user_id,$eval,'$evalids',\"$evaltext\",now())";
		} else {
			$list_ids = explode(",", $evalids);
			$texts = explode("__BR__",$evaltext);
		
			$evalids="";
			$evaltext="";
			$list_saved_ids = explode(',', $hash[$eval][0]);
			$t=0;
			foreach ($list_ids as $sid) {
				if (!in_array($sid, $list_saved_ids)) {
					$evalids .= ",$sid";
					$evaltext .="__BR__".$texts[$t];
				}
				$t++;
			}
			if ($evalids != "") {
				$evaltext = str_replace("\"","&quot;",stripslashes(trim($hash[$eval][1]."$evaltext")));	
				$query = "UPDATE annotation SET evalids='".$hash[$eval][0].trim($evalids)."',evaltext=\"$evaltext\",lasttime=now() WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id AND eval=$eval;";
			}
		}
		#print "Q: $query<br>";
		safe_query($query);
		
		//delete previous "no errors" or "too many errors"
		$query = "DELETE FROM annotation WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id AND eval<2";
		#print "Q2: $query<br>";
		safe_query($query);	
	}
	
	safe_query("UPDATE done SET completed='N',lasttime=now() WHERE sentence_num='$source_id' AND user_id=$user_id");
	return 1;
}

function mergeAligmentAnnotations ($list_saved_ids,$list_ids, $action) {
	//print "mergeAligmentAnnotations ($list_saved_ids,$list_ids)<br>";

	$ids = explode(" ", $list_ids);
	$saved_ids = explode(',', $list_saved_ids);
	
	foreach ($ids as $id) {
		$points=preg_split("/-/",$id);
		for ($x=0; $x< count($saved_ids); $x++) {
			$found=0;
			$current_ids = " ".$saved_ids[$x]." ";
			if (preg_match("/ $id /",$current_ids)) {
				$found=1;
				if ($action == "remove") {
					$saved_ids[$x] = trim(preg_replace("/ $id /"," ", $current_ids));
					if ($saved_ids[$x] == "") {
						unset($saved_ids[$x]);
					} else {
						//ricalcolo i multitokens se in input mi e' arrivato da eliminare solo un token
						if (count($ids) == 1) {
							$first_multitokens = " ";
							$rest_multitokens = "";
						
							foreach (preg_split("/ /",$saved_ids[$x]) as $tmp_id) {
								$points2=preg_split("/-/",$tmp_id);
								if ($first_multitokens == " " || 
									preg_match("/ {$points2[0]}\-/", $first_multitokens) ||
									preg_match("/\-{$points2[1]} /",$first_multitokens)) {
									$first_multitokens .= "$tmp_id ";
								} else {
									$rest_multitokens .= "$tmp_id ";
								}
							}
							array_push($saved_ids,trim($first_multitokens));
							array_push($saved_ids,trim($rest_multitokens));
							unset($saved_ids[$x]);
						}
					}
				} 
			} else if (preg_match("/ {$points[0]}\-/",$current_ids) ||
				preg_match("/\-{$points[1]} /",$current_ids)) {
				$found=1;	
				if ($action == "add") {
					//sorting ids
					$multivalue_ids = array();
					foreach (preg_split("/ /", $saved_ids[$x]. " $id") as $temp_id) {
						$temp_point=preg_split("/-/",$temp_id);
						if (!isset($multivalue_ids[intval($temp_point[0])])) {
							$multivalue_ids[intval($temp_point[0])] = array();
						}
						array_push($multivalue_ids[intval($temp_point[0])], intval($temp_point[1]));					
					}
					
					ksort($multivalue_ids, SORT_NUMERIC );
					$temp_ids="";
					foreach($multivalue_ids as $s => $arrayT) {
						sort($arrayT, SORT_NUMERIC );
						foreach($arrayT as $t) {
							$temp_ids .= " $s-$t";
						}
					}
					$saved_ids[$x] = trim($temp_ids);
				  }
			}
		}
		if ($found == 0 && $action == "add") {
			array_push($saved_ids,$id);
		}
		//print $id . " " . join(",",$saved_ids) . "<br>";
	}	
	$saved_ids = array_filter($saved_ids);

	return join(",",$saved_ids);	
}


#save annotation task: alignment
function saveAligment($source_id,$output_id,$user_id,$eval,$evalids="",$action="add") {
	$query="";
	$evalids=trim($evalids);
	if ($evalids != "") {
	  if ($evalids == "-1") {
		$query = "DELETE FROM annotation WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id AND eval=$eval";
		safe_query($query);
	  } else {
		if ($action == "add") {
			$hash = getErrors($source_id,$output_id,$user_id);
			if ($eval == "1" && isset($hash["2"])) {
				saveAligment($source_id,$output_id,$user_id,"2",$evalids,"remove");
			} else if ($eval == "2" && isset($hash["1"])) {
				saveAligment($source_id,$output_id,$user_id,"1",$evalids,"remove");
			}
		}
		$hash = getErrors($source_id,$output_id,$user_id);
		if (!isset($hash[$eval])) {
			$query = "INSERT INTO annotation (sentence_num,output_id,user_id,eval,evalids,lasttime) VALUES ('$source_id','$output_id',$user_id,$eval,'$evalids',now())";
		} else {
			$evalids=mergeAligmentAnnotations($hash[$eval][0],$evalids,$action);
						
			if ($evalids != "") {
				$query = "UPDATE annotation SET evalids='".trim($evalids)."',lasttime=now() WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id AND eval=$eval;";
			} else {
				$query = "DELETE FROM annotation WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id AND eval=$eval";
			}
		}
		safe_query($query);
	  }
	}  
	#print "Q1: $query<br>";
	safe_query("UPDATE done SET completed='N',lasttime=now() WHERE sentence_num='$source_id' AND user_id=$user_id");
	
	//delete previous "no alignments"
	$query = "DELETE FROM annotation WHERE sentence_num='$source_id' AND output_id='$output_id' AND user_id=$user_id AND eval<1";
	#print "Q2: $query<br>";
	safe_query($query);	
	return 1;
}

#save comment
function saveComment($sentence_num,$user_id,$comment) {
	$query = "DELETE FROM comment WHERE sentence_num=$sentence_num AND user_id=$user_id;";
	safe_query($query);
	$comment = str_replace("\"","&quot;",stripslashes(trim($comment)));
	if (!empty($comment)) {
		$query = "INSERT INTO comment VALUES ($sentence_num, $user_id, \"$comment\")";
		#saveLog($query);
		safe_query($query);	
	}	
	//safe_query("UPDATE done SET completed='N',lasttime=now() WHERE sentence_num='$source_id' AND user_id=$user_id");
	
}

#get comment
function getComment($sentence_num,$user_id) {
	$query = "SELECT comment FROM comment WHERE sentence_num=$sentence_num AND user_id=$user_id;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		return $row[0];
	}
	return "";
}

function getComments($taskid, $user_id) {
	$hash = array();
	$query = "SELECT sentence_num,id,type,comment FROM comment LEFT JOIN sentence ON sentence_num=sentence.num WHERE task_id=$taskid AND comment.user_id=$user_id";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			$hash[$row["sentence_num"]] = str_replace("&quot;","\"",$row["comment"]);
		}
	}
	return $hash;
}

function getErrorList() {
	$hash = array();
	$query = "SELECT error,confidence,selection_counter FROM errorList where selection_counter < 5 and wrongcases >= right_cases and find_in_corpus > 0  order by confidence asc limit 10;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			$hash[$row["error"]] = $row;
		}
	}
	return $hash;
}


# check if the user click for a sentence
function isDone($sentence_num,$user_id) {
	$query = "SELECT completed FROM done WHERE sentence_num=$sentence_num AND user_id=$user_id;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		if ($row[0] == "Y") {
			return 1;
		} else {
			return 0;
		}
	}
	return -1;
}

# check if the user click get next file for a sentence
function isDoneExported($sentence_num,$user_id) {
	$query = "SELECT exported FROM done WHERE sentence_num=$sentence_num AND user_id=$user_id;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		if ($row[0] == "Y") {
			return 1;
		} else {
			return 0;
		}
	}
	return -1;
}



function getConoscenza($sentence_num,$user_id) {
	$query = "SELECT conoscenza FROM annotation WHERE sentence_num=$sentence_num AND user_id=$user_id;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		if ($row[0] == "1") {
			return 1;
		} else {
			return 0;
		}
	}
	return 0;
}



# check if the user click get next file for a sentence,possibile bug, that the previously exported files were useless but her counted them in the trainset.TOBE fixed!
function getDoneExportedCountANDTRAINED() {
	$query = "SELECT count(sentence_num) FROM done WHERE exported='Y' AND trained='Y';";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		if ($row[0] >0) {
			return $row[0];
		} else {
			return 0;
		}
	}
	return -1;
}
# check if the user click get next file for a sentence
function getDoneExportedCount() {
	$query = "SELECT count(sentence_num) FROM done WHERE exported='Y' AND trained='N';";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		if ($row[0] >0) {
			return $row[0];
		} else {
			return 0;
		}
	}
	return -1;
}
function setDoneExportedSentencelistAsTrained($causal){
	$query = "UPDATE done SET trained='Y' WHERE sentence_num IN ( $causal ) AND exported='Y' ";
	 safe_query($query);
}

# get all sentenceids of a user which has exported 'y'
function getDoneExportedSentencelist($userid) {
	$query = "SELECT sentence_num FROM done where exported='Y'AND trained='N';";
	$result = safe_query($query);	
	$str="";
		if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			if(strlen($str)>0){
				$str.=", ";
			}
			$str.= $row[0];
		}
		}
	
	
	return $str;
}
#save "Y" when a user click the "DONE" button in the sentence page
function saveDone($sentence_num,$user_id,$completed) {
	$query="";
	if (isDone($sentence_num,$user_id) < 0) {
		$query = "INSERT INTO done (sentence_num,user_id,completed,lasttime) VALUES ($sentence_num,$user_id,'Y',now())";
	} else {
		$query = "UPDATE done SET completed='Y',lasttime=now() WHERE sentence_num=$sentence_num AND user_id=$user_id";
	}
	return safe_query($query);
}

#save "Y" when a user click the "export next file" button in the sentence page
function saveDoneExported($sentence_num,$user_id,$completed) {
	$query="";
	if (isDone($sentence_num,$user_id) < 0) {
		$query = "INSERT INTO done (sentence_num,user_id,completed,lasttime,exported) VALUES ($sentence_num,$user_id,'Y',now(),'Y')";
	} else {
		$query = "UPDATE done SET exported='Y',lasttime=now() WHERE sentence_num=$sentence_num AND user_id=$user_id";
	}
	return safe_query($query);
}

# get info about the target sentences
function getSystemSentences($id,$taskid) {
	$query = "SELECT num,lang,text,tokenization FROM sentence WHERE linkto='$id' AND task_id=$taskid AND type != 'reference' order by type";
	$result = safe_query($query);
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			$hash[$row["num"]] = array($row["lang"], $row["text"], $row["tokenization"]);
		}
	}
	
	$taskinfo = getTaskInfo($taskid);
	if ($taskinfo['randout'] == "Y") {
		#randomize and return
		return shuffle_assoc($hash,$id);
	}
	return $hash;

}

function getOutputSentence($sourceId, $type) {
	$query = "SELECT num from sentence where linkto='".$sourceId. "' and type='".$type ."'";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		return $row[0];
	}
	return -1;
}

# get hash with uniq info about the available sentences for a task: lang,sentence_num, text, progress counter
function getSourceSentences($taskid) {
	$query = "SELECT num,lang,text,id FROM sentence WHERE gold='N' and task_id='$taskid' AND linkto is null order by num;";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		$i=1;
		while ($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = array($row[1],$row[2],$row[3],$i);
			$i++;
        }
	}
	return $hash;
}
function getSourceSentencesGold($taskid) {
	$query = "SELECT num,lang,text,id FROM sentence WHERE gold='Y' and task_id='$taskid' AND linkto is null order by num;";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		$i=1;
		while ($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = array($row[1],$row[2],$row[3],$i);
			$i++;
        }
	}
	return $hash;
}

function getSourceSentencesNotSolved($taskid) {
	$query = "SELECT num,lang,text,id FROM sentence WHERE finished = 'C' and gold='N' and task_id='$taskid' AND linkto is null order by num;";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		$i=1;
		while ($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = array($row[1],$row[2],$row[3],$i);
			$i++;
        }
	}
	return $hash;
}
function getSourceSentences3vs1Userbased($taskid,$uid) {
	$query = "SELECT num,lang,text,id FROM sentence WHERE sentence.annotators like '%".$uid."%' and  ( LENGTH(annotators) - LENGTH(REPLACE(annotators, ',', '')) between 3 and 3 ) and finished = 'Y' and gold='N' and task_id='$taskid' AND linkto is null order by num;";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		$i=1;
		while ($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = array($row[1],$row[2],$row[3],$i);
			$i++;
        }
	}
	return $hash;
}

function getSourceSentences3vs1($taskid) {
	$query = "SELECT num,lang,text,id FROM sentence WHERE ( LENGTH(annotators) - LENGTH(REPLACE(annotators, ',', '')) between 3 and 3 ) and finished = 'Y' and gold='N' and task_id='$taskid' AND linkto is null order by num;";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		$i=1;
		while ($row = mysql_fetch_row($result)) {
			$hash[$row[0]] = array($row[1],$row[2],$row[3],$i);
			$i++;
        }
	}
	return $hash;
}
# get an array with done sentece by a user
function getDoneSentences($taskid,$userid) {
	$query = "SELECT sentence_num FROM done LEFT JOIN sentence on done.sentence_num=sentence.num WHERE done.user_id=$userid AND sentence.task_id=$taskid AND completed ='Y'";
	$result = safe_query($query);	
	$arr = array();
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_row($result)) {
			array_push($arr, $row[0]);
		}
	}
	return $arr;
}


# get an array with done sentece by a user
function getErrorSentences($taskid) {
	$query = "SELECT linkto FROM sentence WHERE text LIKE '% %' AND task_id=$taskid";
	$result = safe_query($query);	
	$arr = array();
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_row($result)) {
			array_push($arr, $row[0]);
		}
	}
	return $arr;
}

#get the counter about sentence types of a task
function countTaskSentences ($taskid) {
	$query="SELECT type,count(*) as num FROM sentence WHERE task_id=$taskid group by type order by type;";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_array($result)) {
			$hash[$row["type"]] = $row["num"];
		}
	}
	return $hash;
}

#get the counter about sentence types of a task
function countTaskSystem ($taskid) {
	if (!empty($taskid)) {
		if (getTaskType($taskid) == "docann") {
			return 1;
		} else {
			$query="SELECT distinct type FROM sentence WHERE task_id=$taskid AND type != 'source' AND type != 'reference';";
			$result = safe_query($query);	
			return mysql_num_rows($result);
		}
	}
	return 0;
}

#get the list of sentence types
function getSentenceType () {
	$query="SELECT distinct type FROM sentence ORDER BY type";
	$result = safe_query($query);	
	$arr = array();
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_row($result)) {
			array_push($arr, $row[0]);
		}
	}
	return $arr;
}

# get task ID by the task name
function getTaskID($taskname) {
	$query = "SELECT id FROM task WHERE name='$taskname';";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		return $row[0];
	}
	return 0;
}



function rangesJson2ArrayInverse($ranges) {
	$array = array();
	$dec = json_decode(stripslashes($ranges));
	for($idx=0;$idx<count($dec);$idx++){
    	$obj = (Array) $dec[$idx];
    	$array[$obj["label"]] = array($obj["val"], $obj["color"]);
    }
    return $array;
}

function rangesJson2Array($ranges) {
	$array = array();
	$dec = json_decode(stripslashes($ranges));
	for($idx=0;$idx<count($dec);$idx++){
    	$obj = (Array) $dec[$idx];
    	$array[$obj["val"]] = array($obj["label"], $obj["color"]);
    }
    return $array;
}

# get task type by a task name
function getTaskType($taskid) {
	$query = "SELECT type FROM task WHERE id=$taskid;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		return $row[0];
	}
	return "";
}

# get task name by a task id
function getTaskName($taskid) {
	$query = "SELECT name FROM task WHERE id=$taskid;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_row($result);
		return $row[0];
	}
	return "";
}

# get an array with tasks name
function getTasks($userid) {
	global $taskTypes;
	$hash = array();
	//if you are root
	if (isset($userid)) {
		$annotationCount=getAnnotationTaskStats();
		if (intval($userid) == 0) {
			$query = "SELECT id,name,type FROM task ORDER BY type, name";
			$result = safe_query($query);	
			if (mysql_num_rows($result) > 0) {
				while ($row = mysql_fetch_row($result)) {
					#print $row[0] ."= array(".$row[1].", ".$row[2].")<br>";
					$count=0;
					if (isset($annotationCount[$row[0]])) {
						$count = $annotationCount[$row[0]];	
					}
					if (isset($taskTypes[$row[2]])) {
						$hash[$row[0]] = array($row[1], $row[2],$count);
					}
				}
			}
		} else { 
			$query = "SELECT DISTINCT id,name,type FROM task LEFT JOIN usertask ON task.id=usertask.task_id WHERE user_id='$userid' OR owner='$userid' order by type, name";
			$result = safe_query($query);	
			while ($row = mysql_fetch_row($result)) {
				$count=0;
				if (isset($annotationCount[$row[0]])) {
					$count = $annotationCount[$row[0]];	
				}
				if (isset($taskTypes[$row[2]])) {
					$hash[$row[0]] = array($row[1], $row[2],$count);
				}
			}
			//add also own task
			//$query = "SELECT id,name,type FROM task WHERE owner='$userid'";
			//$result = safe_query($query);	
			//while ($row = mysql_fetch_row($result)) {
			//	$hash[$row[0]] = array($row[1], $row[2]);
			//}	
		}	
	} 
	return $hash;
}

# get an array with tasks name Document level
function getTasksDocumentLevel($userid) {
	global $taskTypes;
	$hash = array();
	//if you are root
	if (isset($userid)) {
		$annotationCount=getAnnotationTaskStats();
		if (intval($userid) == 0) {
			$query = "SELECT id,name,type FROM task where type ='quality' ORDER BY type, name";
			$result = safe_query($query);	
			if (mysql_num_rows($result) > 0) {
				while ($row = mysql_fetch_row($result)) {
					#print $row[0] ."= array(".$row[1].", ".$row[2].")<br>";
					$count=0;
					if (isset($annotationCount[$row[0]])) {
						$count = $annotationCount[$row[0]];	
					}
					if (isset($taskTypes[$row[2]])) {
						$hash[$row[0]] = array($row[1], $row[2],$count);
					}
				}
			}
		} else { 
			$query = "SELECT DISTINCT id,name,type FROM task LEFT JOIN usertask ON task.id=usertask.task_id WHERE task.type ='quality' and ( user_id='$userid' OR owner='$userid' ) order by type, name";
			$result = safe_query($query);	
			while ($row = mysql_fetch_row($result)) {
				$count=0;
				if (isset($annotationCount[$row[0]])) {
					$count = $annotationCount[$row[0]];	
				}
				if (isset($taskTypes[$row[2]])) {
					$hash[$row[0]] = array($row[1], $row[2],$count);
				}
			}
			//add also own task
			//$query = "SELECT id,name,type FROM task WHERE owner='$userid'";
			//$result = safe_query($query);	
			//while ($row = mysql_fetch_row($result)) {
			//	$hash[$row[0]] = array($row[1], $row[2]);
			//}	
		}	
	} 
	return $hash;
}
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}


function addFileDataMine ($taskid,$type,$tokenization,$filepath,$filename, $user_id="",$filecsv="",$errorFileDerived="") {


	$taskinfo = getTaskInfo($taskid);
	$errmsg="";
	$insert=0;
   	$mappingsID2NUM = getSourceSentenceIdMapping($taskid);
	// print "TASK: $taskid, TYPE: $type, mappingsID2NUM: ". join(",",$mappingsID2NUM). " ($tokenization,$filepath,$filename)<br>";
	#skip the file that start with dot
	if (preg_match("/^\..+$/",basename($filename))) {
		return $errmsg;
	}	 
	$NERFIN=false;
	if(endsWith(basename($filename),".txt")){
		$NERFIN=true;
	}
	// this array is used just when the input file contains values
	$outputID2NUM = array();
	
	$handle = fopen($filepath, "r");
	if ($handle) {
		$tasktype = getTaskType($taskid);
		$linenum = 0;
		$fileType ="csv";
		$fileName = $filename;
		$txpText="";
		$tokenstartId = -1;
		$currentPosition=0;
		$hasInputValues=0;
		while (($line = fgets($handle)) !== false) {
    		$linenum++;
 	   		if (preg_match("/^# FILE:/",$line)) {
    			$fileType="txp";
    			#$fileName=trim(preg_replace("/^# FILE:/","",$line));
    		} else if (preg_match("/^# FIELDS:/",$line)) {
    			$fields = preg_split("/\t/",preg_replace("/^# FIELDS:\s*/","", $line));
    			for ($i=0; $i< count($fields); $i++) {
    				if ($fields[$i] == "tokenstart") {
    					$tokenstartId=$i;
    				}
    			}
    		}
    		if ($tasktype == "docann") {
    			if ($fileType == "txp") {
    				if (preg_match("/^# /",$line)) {
    					continue;
    				} else {
    					if (trim($line) == "" && strlen($txpText) >0) {
    						$txpText .= "\n";
    						$currentPosition++;
    					} else {
    						$items = explode("\t", trim(htmlentities2utf8($line)));
    						if ($tokenstartId != -1) {
    							for ($i=$currentPosition;$i<intval($items[$tokenstartId]); $i++) {
    								$txpText .= " ";
    							}
    						} 
    						$txpText .= $items[0];
    						if ($tokenstartId != -1) {
    							$currentPosition = intval($items[$tokenstartId]) + strlen($items[0]);
    						} else {
    							$txpText .= " ";
    						} 	
    					}
    				}
    			} else {
    				$txpText .= htmlentities2utf8($line);
    			}
    		} 
    	}
    	fclose($handle);
		
		if (strlen(trim($txpText)) > 0) {
    		if (strlen(trim($fileName)) > 0) {
    		  if($tokenization==7){
    			if($NERFIN){
    				//run textpro 
    				
    				 $random_digit= date("Y-m-d-H-i-s");
       				 $id = "".$random_digit;
      				  $directory = ACTIVELEARN_RUN_PATH;
     				   $folder=$directory.$id."/";
     				   $try = $directory.$id."/";
     				   mkdir($folder, 0777);
     				   exec("chmod 777 $folder");
       					 $inp= $try."input";
      				  exec("cp ".$filepath." ".$inp);
      				 exec("chmod 777 $inp"); 
       				 $commandline = ACTIVELEARN_textpro_PATH." ".$inp;
       				 $output = exec($commandline.' 2> '.$try.'executeLog.txt',$output);	
    					$txpText="";
    						$handletxtpro = fopen($inp."-output", "r");
    				
						if ($handletxtpro) {
						
											
								$TOKENID=1;
                                                $ENTANNOT=array();
                                                $ENTANNOTID=array();

                                                $taskranges = rangesJson2Array($taskinfo["ranges"]);
                                                foreach ($taskranges as $tag => $value){
                                                        if ($tag != "0" && $tag != "1"){
                                                                $tags[substr($value[0],0,3)] = $tag;
                                                        }
                                                }

                                                
                                                foreach ($tags as $tag => $value){
                                                  $ENTANNOT[$tag] = "";
                                                  $ENTANNOTID[$tag] = "";
                                                }

						

					$startspan=false;
						while (($linetxt = fgets($handletxtpro)) !== false) {
							//echo $linetxt;
							if((strlen(trim($linetxt)) == 0)){
								$txpText.= "\n";
							}else
							if((endsWith($linetxt,"O")&&$startspan)){
								
							}else
							if (strlen(trim($linetxt)) > 0) {
								$items = explode("\t", trim(htmlentities2utf8($linetxt)));
    							$TOKEN=$items[0];
    							$txpText.=$TOKEN." ";
    							$TAGTXT=trim($items[1]);

							foreach ($tags as $tag => $value){
                                                          if(endsWith($TAGTXT,"B-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOT[$tag])) > 0) {
                                                              $ENTANNOT[$tag].="__BR__";
                                                              $ENTANNOTID[$tag].=",";
                                                            }
                                                            $ENTANNOT[$tag].=$TOKEN;
                                                            $ENTANNOTID[$tag].=$TOKENID;
                                                          }

                                                          else if(endsWith($TAGTXT,"I-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOT[$tag])) > 0) {
                                                              $ENTANNOT[$tag].="  ";
                                                              $ENTANNOTID[$tag].=" ";
                                                            }
                                                            $ENTANNOT[$tag].=$TOKEN;
                                                            $ENTANNOTID[$tag].=($TOKENID-1)."-".$TOKENID." ".$TOKENID;
                                                          }
                                                        }
				
									
    							$TOKENID++;
    					   	 
						  }
						}
						fclose($handletxtpro);
						$txpText=trim($txpText);
						$query = "INSERT INTO sentence (id, type, lang, task_id, text, lasttime, tokenization,errorDerived) VALUES ('$fileName','".$type."','',$taskid,'".addslashes($txpText) ."', now(),1,'".$errorFileDerived."');";
    		//print $query;
    			$insert += safe_query($query);
    			$sentID = mysql_insert_id();
					
						foreach ($tags as $tag => $idTag){
                                                  $query = "INSERT INTO annotation (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTID[$tag]}','" .addslashes($ENTANNOT[$tag]) ."',now())";
                                                  $insert += safe_query($query);

                                                  $query = "INSERT INTO annotationsave (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTID[$tag]}','" .addslashes($ENTANNOT[$tag]) ."',now())";
                                                  $insert += safe_query($query);

                                                  //$idTag++;
                                                }
				       					
					}
    			}
    		}//end tokenization = NER
    			else if($tokenization==5){
    				// NER annotated
    				$handletxtpro = fopen($filepath, "r");
				
					if ($handletxtpro) {
						
						$TOKENID=1;
						$ENTANNOT=array();
                                                $ENTANNOTID=array();
						$tags=array();


                                                $taskranges = rangesJson2Array($taskinfo["ranges"]);
                                                foreach ($taskranges as $tag => $value){
                                                        if ($tag != "0" && $tag != "1"){
                                                                $tags[substr($value[0],0,3)] = $tag;
                                                        }
                                                }

                                               
                                                foreach ($tags as $tag => $value){
                                                  $ENTANNOT[$tag] = "";
                                                  $ENTANNOTID[$tag] = "";
                                                }
		
				$startspan=false;
					$txpText="";
						while (($linetxt = fgets($handletxtpro)) !== false) {
							
							if((strlen(trim($linetxt)) == 0)
							//||(endsWith($linetxt,"O")&&$startspan)
							){
								$txpText.= "\n";
							}else
							if (strlen(trim($linetxt)) > 0) {
								$items = explode("\t", trim(htmlentities2utf8($linetxt)));
    							$TOKEN=$items[0];
    							$txpText.=$TOKEN." ";
    							$TAGTXT=trim($items[1]);

							foreach ($tags as $tag => $value){
                                                          if(endsWith($TAGTXT,"B-".$tag)){
							    $startspan=true;
                                                            if (strlen(trim($ENTANNOT[$tag])) > 0) {
                                                              $ENTANNOT[$tag].="__BR__";
                                                              $ENTANNOTID[$tag].=",";
                                                            }
                                                            $ENTANNOT[$tag].=$TOKEN;
                                                            $ENTANNOTID[$tag].=$TOKENID;
                                                          }

                                                          else if(endsWith($TAGTXT,"I-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOT[$tag])) > 0) {
                                                              $ENTANNOT[$tag].="  ";
                                                              $ENTANNOTID[$tag].=" ";
                                                            }
                                                            $ENTANNOT[$tag].=$TOKEN;
                                                            $ENTANNOTID[$tag].=($TOKENID-1)."-".$TOKENID." ".$TOKENID;
                                                          }
                                                        }

    							$TOKENID++;
    					   	 
						  }
						}
						fclose($handletxtpro);
						$txpText=trim($txpText);
						$query = "INSERT INTO sentence (id, type, lang, task_id, text, lasttime, tokenization,errorDerived) VALUES ('$fileName','".$type."','',$taskid,'".addslashes($txpText) ."', now(),1,'".$errorFileDerived."');";
    		//print $query;
    			$insert += safe_query($query);
    			$sentID = mysql_insert_id();
			
					foreach ($tags as $tag => $idTag){
                                                  $query = "INSERT INTO annotation (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTID[$tag]}','" .addslashes($ENTANNOT[$tag]) ."',now())";
						$insert += safe_query($query);

                                                  $query = "INSERT INTO annotationsave (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTID[$tag]}','" .addslashes($ENTANNOT[$tag]) ."',now())";
                                                  $insert += safe_query($query);

                                                  //$idTag++;
                                                }
				}

		//}

    				
    			
    			
    			}else if($tokenization==6){
    				//NER Gold/annotated
    				$handletxtpro = fopen($filepath, "r");
    				
					if ($handletxtpro) {
						
						$TOKENID=1;
					
						$ENTANNOT=array();
                                                $ENTANNOTID=array();
						$ENTANNOTG=array();
						$ENTANNOTIDG=array();
						$tags=array();

                                                $taskranges = rangesJson2Array($taskinfo["ranges"]);
						foreach ($taskranges as $tag => $value){
                                                        if ($tag != "0" && $tag != "1"){
                                                                $tags[substr($value[0],0,3)] = $tag;
                                                        }
                                                }

                                                //if ($ENTTAGS != ""){
                                                //  $tags = str_split(',',$ENTTAGS);
                                                //}

                                                foreach ($tags as $tag => $value){
                                                  $ENTANNOT[$tag] = "";
                                                  $ENTANNOTID[$tag] = "";
					 	  $ENTANNOTG[$tag] = "";
						  $ENTANNOTIDG[$tag] = "";
                                                }

					$startspan=false;
					$startspanG=false;
					$txpText="";
						while (($linetxt = fgets($handletxtpro)) !== false) {
							//echo $linetxt;
							if((strlen(trim($linetxt)) == 0)
							//||(endsWith($linetxt,"O")&&$startspan)
							){
							//	$TOKENID++;
								$txpText.= "\n";
							}else
							if (strlen(trim($linetxt)) > 0) {
								$items = explode("\t", trim(htmlentities2utf8($linetxt)));
    							$TOKEN=$items[0];
    							$TAGTXTG=$items[1];
    							$txpText.=$TOKEN." ";
    							$TAGTXT=trim($items[2]);
							
							foreach ($tags as $tag => $value){
                                                          if(endsWith($TAGTXT,"B-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOT[$tag])) > 0) {
                                                              $ENTANNOT[$tag].="__BR__";
                                                              $ENTANNOTID[$tag].=",";
                                                            }
                                                            $ENTANNOT[$tag].=$TOKEN;
                                                            $ENTANNOTID[$tag].=$TOKENID;
                                                          }

                                                          else if(endsWith($TAGTXT,"I-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOT[$tag])) > 0) {
                                                              $ENTANNOT[$tag].="  ";
                                                              $ENTANNOTID[$tag].=" ";
                                                            }
                                                            $ENTANNOT[$tag].=$TOKEN;
                                                            $ENTANNOTID[$tag].=($TOKENID-1)."-".$TOKENID." ".$TOKENID;
                                                          }
                                                        }

							foreach ($tags as $tag => $value){
                                                          if(endsWith($TAGTXTG,"B-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOTG[$tag])) > 0) {
                                                              $ENTANNOTG[$tag].="__BR__";
                                                              $ENTANNOTIDG[$tag].=",";
                                                            }
                                                            $ENTANNOTG[$tag].=$TOKEN;
                                                            $ENTANNOTIDG[$tag].=$TOKENID;
                                                          }

                                                          else if(endsWith($TAGTXTG,"I-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOTG[$tag])) > 0) {
                                                              $ENTANNOTG[$tag].="  ";
                                                              $ENTANNOTIDG[$tag].=" ";
                                                            }
                                                            $ENTANNOTG[$tag].=$TOKEN;
                                                            $ENTANNOTIDG[$tag].=($TOKENID-1)."-".$TOKENID." ".$TOKENID;
                                                          }
                                                        }

					

    							$TOKENID++;
    					   	 
						  }
						}
						fclose($handletxtpro);
						$txpText=trim($txpText);
						$query = "INSERT INTO sentence (id, type, lang, task_id, text, lasttime, tokenization,errorDerived) VALUES ('$fileName','".$type."','',$taskid,'".addslashes($txpText) ."', now(),1,'".$errorFileDerived."');";
    		//print $query;
    			$insert += safe_query($query);
    			$sentID = mysql_insert_id();
						

					foreach ($tags as $tag => $idTag){
                                                  $query = "INSERT INTO annotation (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTID[$tag]}','" .addslashes($ENTANNOT[$tag]) ."',now())";
                                                  $insert += safe_query($query);

                                                  $query = "INSERT INTO annotationsave (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTID[$tag]}','" .addslashes($ENTANNOT[$tag]) ."',now())";
                                                  $insert += safe_query($query);

                                                  //$idTag++;
                                                }

					foreach ($tags as $tag => $idTag){
                                                  $query = "INSERT INTO annotation (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTIDG[$tag]}','" .addslashes($ENTANNOTG[$tag]) ."',now())";
                                                  $insert += safe_query($query);

                                                  $query = "INSERT INTO annotationsave (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTIDG[$tag]}','" .addslashes($ENTANNOTG[$tag]) ."',now())";
                                                  $insert += safe_query($query);

                                                  //$idTag++;
                                                }


				}
    			
    			
    			}
    			
    			
    			
    		} else {
    			$errmsg = "WARNING! The filename is missing.";
    		}
    	}
    		
    	
    	//add info about used file
    	/*$query = "DELETE FROM file WHERE type='$type' AND task_id=$taskid";
    	safe_query($query);
		$query = "INSERT INTO file VALUES ('$filename','$type',$taskid)";
    	safe_query($query);
		*/
	} else {
    	// error opening the file.
    	$errmsg = "ERROR! Some problems occured opening the file $filename.";
	}
	#if ($insert == count($mappingsID2NUM)) {
    #	$errmsg = "DONE! $insert resources have been inserted";
    #}
    return $sentID;
}
# add data
function addFileData ($taskid,$type,$tokenization,$filepath,$filename, $user_id="") {

	/*
	<option value='3'>token-level token
	<option value='4'>token-level token/gold
	<option value='5'>doc level ID/text
	<option value='6'>doc level ID/text/gold	
	*/

	$errmsg="";
	$insert=0;
   	$mappingsID2NUM = getSourceSentenceIdMapping($taskid);
   	$taskinfo = getTaskInfo($taskid);
   	$tasktype = getTaskType($taskid);
   	$taskrangesR = rangesJson2ArrayInverse($taskinfo["ranges"]);

	// print "TASK: $taskid, TYPE: $type, mappingsID2NUM: ". join(",",$mappingsID2NUM). " ($tokenization,$filepath,$filename)<br>";
	#skip the file that start with dot
	if (preg_match("/^\..+$/",basename($filename))) {
		return $errmsg;
	}	 
	
	if ($tasktype == "docann" && !endsWith(basename($filename),".iob2")) {
		return "Error: token level files should has '.iob2' extension. ";
	}
	if ($tasktype == "quality" && !endsWith(basename($filename),".tsv")) {
		return "Error: document level files should has '.tsv' extension. ";
	}
	if($tokenization<3 || $tokenization > 6){
		return "Error: Select a file type. ";
	}
	
	// this array is used just when the input file contains values
	$outputID2NUM = array();
	$TOKENID=1;

						$ENTANNOT=array();
                        $ENTANNOTID=array();
						$tags=array();
	
                                                $taskranges = rangesJson2Array($taskinfo["ranges"]);
                                                foreach ($taskranges as $tag => $value){
                                                        if ($tag != "0" && $tag != "1"){
                                                                $tags[substr($value[0],0,3)] = $tag;
                                                        }
                                                }

                                                foreach ($tags as $tag => $value){
                                                  $ENTANNOT[$tag] = "";
                                                  $ENTANNOTID[$tag] = "";
                                                }
					$startspan=false;
					$txpText="";
	$handle = fopen($filepath, "r");
	if ($handle) {
		
		$linenum = 0;
		$fileType ="csv";
		$fileName = $filename;
		$txpText="";
		$tokenstartId = -1;
		$currentPosition=0;
		$hasInputValues=0;
		$tttext="";
		while (($line = fgets($handle)) !== false) {
    		$linenum++;
 	   		$line = trim($line);
    		if ($tasktype == "docann") {
    		   if($tokenization==3){
    		   		$txpText .= " ".$line;
    		   		$txpText = trim($txpText);
    		   }
    			
    			 if($tokenization==4){
							if((strlen(trim($line)) == 0)
							){
								$txpText.= "\n";
							}else
							if (strlen(trim($line)) > 0) {
								$items = explode("\t", trim(htmlentities2utf8($line)));
    							$TOKEN=$items[0];
    							$txpText.=$TOKEN." ";
    							$TAGTXT=trim($items[1]);

							foreach ($tags as $tag => $value){
                                                          if(endsWith($TAGTXT,"B-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOT[$tag])) > 0) {
                                                              $ENTANNOT[$tag].="__BR__";
                                                              $ENTANNOTID[$tag].=",";
                                                            }
                                                            $ENTANNOT[$tag].=$TOKEN;
                                                            $ENTANNOTID[$tag].=$TOKENID;
                                                          }

                                                          else if(endsWith($TAGTXT,"I-".$tag)){
                                                            $startspan=true;
                                                            if (strlen(trim($ENTANNOT[$tag])) > 0) {
                                                              $ENTANNOT[$tag].="  ";
                                                              $ENTANNOTID[$tag].=" ";
                                                            }
                                                            $ENTANNOT[$tag].=$TOKEN;
                                                            $ENTANNOTID[$tag].=($TOKENID-1)."-".$TOKENID." ".$TOKENID;
                                                          }
                                                        }

							
								
    							$TOKENID++;
    					   	 
						  }
						
    			 }
    			
    			
    			
    			
    			
    		}
    		if ($tasktype !== "docann") {
    			//$tttext =htmlentities2utf8($line)."\n";
    			$items = explode("\t", trim(htmlentities2utf8($line)));
    			$filename = $items[0];
    			$tttext = $items[1];
    			if($tokenization==5){
						$query = "INSERT INTO sentence (id, type, lang, task_id, text, lasttime, tokenization,gold) VALUES ('".$filename ."','source','it',$taskid,'". $filename."', now(),0,'N');"; 
						safe_query($query);
      				 	$sId = mysql_insert_id();
     				  	$query = "INSERT INTO sentence (id, type, lang, task_id, linkto, text, lasttime, tokenization,gold) VALUES ('".$filename ."','sys1','it',$taskid,'". $sId ."','".addslashes($tttext)."', now(), 0,'N');";
     				  	safe_query($query);
     				   $sentID = mysql_insert_id();
       			}
       			
       			if($tokenization==6){
       				
       				$label = $items[2];
       				$query = "INSERT INTO sentence (id, type, lang, task_id, text, lasttime, tokenization,gold) VALUES ('".$filename ."','source','it',$taskid,'". $filename."', now(),0,'Y');"; 
						safe_query($query);
      				 	$sId = mysql_insert_id();
     				  	$query = "INSERT INTO sentence (id, type, lang, task_id, linkto, text, lasttime, tokenization,gold) VALUES ('".$filename ."','sys1','it',$taskid,'". $sId ."','".addslashes($tttext)."', now(), 0,'Y');";
     				  	safe_query($query);
     				   $sentID = mysql_insert_id();
					$query = "INSERT INTO annotation (`sentence_num`, `output_id`, `user_id`, `eval`, `lasttime`) VALUES ('". $sId ."', '". $sentID ."', '". $userid ."', '". $taskrangesR[$label][0]."', now());";
       				safe_query($query);
       	
       				$query = "INSERT INTO `done` (`sentence_num`, `user_id`, `completed`, `lasttime`) VALUES ('". $sId ."', '". $userid ."', 'Y', 'now()');";
       				safe_query($query);
       					
       					
       			}
			}
    	}
    	fclose($handle);
		
		
	
	
		if ($tasktype == "docann") {
    		   if($tokenization==3){
    		   		$txpText=trim($txpText);
						$query = "INSERT INTO sentence (id, type, lang, task_id, text, lasttime, tokenization) VALUES ('$fileName','".$type."','',$taskid,'".addslashes($txpText) ."', now(),1);";
    			$insert += safe_query($query);
    			$sentID = mysql_insert_id();
    		   }
    		   
    		   if($tokenization==4){
    		  	 	$txpText=trim($txpText);
						$query = "INSERT INTO sentence (id, type, lang, task_id, text, lasttime, tokenization) VALUES ('$fileName','".$type."','',$taskid,'".addslashes($txpText) ."', now(),1);";
    			$insert += safe_query($query);
    			$sentID = mysql_insert_id();
						foreach ($tags as $tag => $idTag){
                                                  $query = "INSERT INTO annotation (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTID[$tag]}','" .addslashes($ENTANNOT[$tag]) ."',now())";
                                                  $insert += safe_query($query);
                                                  $query = "INSERT INTO annotationsave (sentence_num,output_id,user_id,eval,evalids,evaltext,lasttime) VALUES ('$sentID','$sentID',$user_id,$idTag,'{$ENTANNOTID[$tag]}','" .addslashes($ENTANNOT[$tag]) ."',now())";
                                                  $insert += safe_query($query);
                                                }
    		   }
    		   			$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$sentID."', '".$user_id."','Y');";
			  			safe_query($query);
    	}
	
    			
	} else {
    	// error opening the file.
    	$errmsg = "ERROR! Some problems occured opening the file $filename.";
	}
	
    return $errmsg;
}

function showSentenceOrig ($idsentence){
	$idsentence=str_replace("-output","",$idsentence);
	$f=fopen(PATH_USED_DOC.$idsentence,"r");
	$sent=fgets($f);
	fclose($f);
	return $sent;	
}
# get system ids and labels
function getSystems() {
	$query = "SELECT DISTINCT type FROM sentence";
	$result = safe_query($query);	
	$hash = array();
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_row($result)) {
			array_push($hash, $row[0]);
		}
	}
	return $hash;
}

#delete the annotations from a Sentence and user
function deleteAnnotationsOfSentence ($senticeid,$userid) {	
	$query="DELETE from annotation where user_id=$userid and sentence_num=$senticeid";
	mysql_query($query) or print ("ERROR! Annotations deleting failed. (" . mysql_error() .")");	
	$deleted = mysql_affected_rows();
	
	$query="DELETE from annotationsave where user_id=$userid and sentence_num=$senticeid";
	mysql_query($query) or print ("ERROR! Annotations deleting failed. (" . mysql_error() .")");	
	$deleted = mysql_affected_rows();
	
	#delete done records
	$query="DELETE from done where user_id=$userid and sentence_num=$senticeid";
	safe_query($query);
	return $deleted;

}

#delete the annotations from a task and user
function deleteAnnotations ($taskid,$userid) {	
	$query="DELETE annotation from annotation LEFT JOIN sentence on annotation.sentence_num=sentence.num where user_id=$userid and task_id=$taskid";
	mysql_query($query) or print ("ERROR! Annotations deleting failed. (" . mysql_error() .")");	
	$deleted = mysql_affected_rows();
	
	$query="DELETE annotationsave from annotationsave LEFT JOIN sentence on annotationsave.sentence_num=sentence.num where user_id=$userid and task_id=$taskid";
	mysql_query($query) or print ("ERROR! Annotations deleting failed. (" . mysql_error() .")");	
	$deleted = mysql_affected_rows();
	
	#delete done records
	$query="DELETE done from done LEFT JOIN sentence on done.sentence_num=sentence.num where user_id=$userid and task_id=$taskid";
	safe_query($query);
	return $deleted;
}

function deleteSentences($taskid,$type) {	
	#delete all annotations
    $query = "DELETE FROM annotation where output_id IN (
    SELECT num FROM sentence WHERE sentence.task_id=$taskid AND type='$type');";
	#delete all sentences
	$query = "DELETE FROM sentence WHERE task_id=$taskid AND type='$type';";
	#$result = safe_query($query);
	return safe_query($query);
}

		
#controllo che i done abbiamo tutti lo stesso numero di output controllati: se ce ne sono in numero diverso probabilmente c'e` stato qualche erore dell'interfaccia: -1 non è stato trovato nessun done, 1 tutto OK!, 0 c'è qualche errore
#select annotation.sentence_num, count(*) as count from annotation LEFT JOIN done ON annotation.sentence_num=done.sentence_num WHERE completed="Y" AND done.user_id=1 group by annotation.sentence_num;
function getCheckAndDone($userid) {	
	$query = "select distinct count(*) as count from annotation LEFT JOIN done ON annotation.sentence_num=done.sentence_num AND completed='Y' AND annotation.user_id=done.user_id WHERE done.user_id=$userid group by annotation.sentence_num;";
	$result = safe_query($query);	
	if (mysql_num_rows($result) > 1) {
		return 0;
	} else {
		if (mysql_num_rows($result) == 0) {
			return -1;
		}
	}
	
	return 1;
}

function getMessage() {
	$query = "SELECT text FROM message ORDER BY id DESC LIMIT 1;";
	$result = safe_query($query);
	while ($row = mysql_fetch_row($result)) {
		return $row[0];
	}
}
function getDBInconsistency($userid, $tasks) {
	$hash_error= array();
	$array_sentencenum = array();
	//get annotation about removed sentences (TODO REMOVE THIS PATCH ASAP!!) 
	$query = "select output_id from annotation left join sentence on annotation.output_id=sentence.num where num is null AND user_id=$userid;";
	$result = safe_query($query);
	while ($row = mysql_fetch_row($result)) {
		array_push($array_sentencenum, $row[0]);
	}
	$query = "select output_id from annotation left join sentence on annotation.sentence_num=sentence.num where num is null AND user_id=$userid;";
	$result = safe_query($query);
	while ($row = mysql_fetch_row($result)) {
		array_push($array_sentencenum, $row[0]);
	}		
	//end get
	
	
	//check if all evaluated sentence has full annotated output
	foreach ($tasks as $taskid) {
		$tasksyscount=countTaskSystem($taskid);
			
		#if ($tasksyscount > 0) {
			$query="SELECT count(*) FROM done LEFT JOIN sentence ON done.sentence_num=sentence.num WHERE user_id=$userid AND completed='Y' AND task_id=$taskid;";
			$result = safe_query($query);
			$row = mysql_fetch_row($result);
			$num_done = $row[0];
				
			$query = "SELECT sentence_num,output_id,count(*) AS count FROM annotation LEFT JOIN sentence ON sentence.num=annotation.sentence_num where task_id=$taskid AND user_id=$userid group by sentence_num,output_id order by sentence_num;";
			$result = safe_query($query);
			$hash = array();
			if (mysql_num_rows($result) > 0) {
				while ($row = mysql_fetch_row($result)) {
					if (!in_array($row[1], $array_sentencenum)) {
						if (isset($hash[$row[0]])) {
							$hash[$row[0]] += 1;
						} else {
							$hash[$row[0]] = 1;
						}
					}	
				}
				while (list ($sentence_num, $countann) = each($hash)) {
					if ($countann < $tasksyscount) {
						$hash_error[$sentence_num] = array($taskid,"");
					}
				}
					
				if ($num_done != count($hash)) {
					$msg = "";
					if ($num_done == 1) {
						$msg = "there is ".$num_done." confirmed annotation on ";
					} else {
						$msg = "there are ".$num_done." confirmed annotations on ";
					}
					$msg .= count($hash) ."!";
					$hash_error["DONE!".$taskid] = array($taskid, $msg);
				}
			}
		#}
	}
	
	$query = "select distinct sentence_num FROM annotation where user_id=$userid";
	$result_annotation = safe_query($query);
	$hash = array();
	while ($row = mysql_fetch_row($result_annotation)) {
		$hash[$row[0]] = 1;
	}
	$query = "select distinct sentence_num,task_id,done.lasttime FROM done LEFT JOIN sentence ON done.sentence_num=sentence.num WHERE user_id=$userid AND completed='Y'";
	$result_done = safe_query($query);	
	while ($row = mysql_fetch_row($result_done)) {
		if (!isset($hash[$row[0]])) {
			$hash_error[$row[0]] = array($row[1],$row[2]);
		} 
	}
	
	return 	$hash_error;
}
function getDBChart(){
	$query = "SELECT id,score,docs FROM f1measures order by id ASC;";
	$my_result_annotation = safe_query($query);
	$arr=array();
	$arr['labels']="";
	$arr['vals']="";
	$i=0;
	while ($my_row_annotation = mysql_fetch_row($my_result_annotation)) {
		$arr['vals'] .=  $my_row_annotation[1].",";
		//$arr['labels'] .=  "\"Training ".$i."\",";
		$arr['labels'] .= "\"". $my_row_annotation[2]." docs\",";

		$i++;
	}
	$arr['files']="0";
		$query = "select user_id,count(*) from done WHERE exported='Y';";
		$result = safe_query($query);
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_row($result)) {
				$arr['files'] = $row[1];
			}
		}
		
	return $arr;
}

function getDBChartAW(){
	$query = "SELECT id,score FROM f1measures order by id ASC;";
	$my_result_annotation = safe_query($query);
	$arr=array();
	$arr['labels']="";
	$arr['vals']="";
	$i=1;
	while ($my_row_annotation = mysql_fetch_row($my_result_annotation)) {
		$arr['vals'] .=  $my_row_annotation[1].",";
		$arr['labels'] .=  "\"Training ".$i."\",";
		//$arr['labels'] .= "\"". $my_row_annotation[2]." docs\",";

		$i++;
	}

		
	return $arr;
}
function getAnnotationReport ($taskid) {
	$hash_report = array();
	$query = "select eval,type,user_id,count(*) from annotation left join sentence on annotation.output_id=sentence.num where sentence.gold = 'N' and task_id=$taskid group by eval,type,user_id order by eval,type,user_id";
	$result_done = safe_query($query);	
	while ($row = mysql_fetch_row($result_done)) {
		if (isset($hash_report[$row[0]])) {
			$hash_report[$row[0]] .= ",".$row[1]." ".$row[2]." ".$row[3];
		} else {
			$hash_report[$row[0]] = $row[1]." ".$row[2]." ".$row[3];
		} 
	}
	return $hash_report;
}

function getAnnotationReportGold ($taskid) {
	$hash_report = array();
	$query = "select eval,type,user_id,count(*) from annotation left join sentence on annotation.output_id=sentence.num where sentence.gold = 'Y' and task_id=$taskid and user_id != 0 group by eval,type,user_id order by eval,type,user_id";
	$result_done = safe_query($query);	
	while ($row = mysql_fetch_row($result_done)) {
		if (isset($hash_report[$row[0]])) {
			$hash_report[$row[0]] .= ",".$row[1]." ".$row[2]." ".$row[3];
		} else {
			$hash_report[$row[0]] = $row[1]." ".$row[2]." ".$row[3];
		} 
	}
	return $hash_report;
}
function getAnnotationReportNotSolved ($taskid) {
	$hash_report = array();
	$query = "select eval,type,user_id,count(*) from annotation left join sentence on annotation.sentence_num=sentence.num where sentence.finished = 'C' and sentence.gold = 'N' and task_id=$taskid group by eval,type,user_id order by eval,type,user_id";
	$result_done = safe_query($query);	
	while ($row = mysql_fetch_row($result_done)) {
		if (isset($hash_report[$row[0]])) {
			$hash_report[$row[0]] .= ",".$row[1]." ".$row[2]." ".$row[3];
		} else {
			$hash_report[$row[0]] = $row[1]." ".$row[2]." ".$row[3];
		} 
	}
	return $hash_report;
}
function getAnnotationReport3vs1Userbased ($taskid,$uid) {
	$hash_report = array();
	$query = "select eval,type,user_id,count(*) from annotation left join sentence on annotation.sentence_num=sentence.num where sentence.annotators like '%".$uid."%' and ( LENGTH(sentence.annotators) - LENGTH(REPLACE(sentence.annotators, ',', '')) between 3 and 3 ) and sentence.finished = 'Y' and sentence.gold = 'N' and task_id=$taskid group by eval,type,user_id order by eval,type,user_id";
	$result_done = safe_query($query);	
	while ($row = mysql_fetch_row($result_done)) {
		if (isset($hash_report[$row[0]])) {
			$hash_report[$row[0]] .= ",".$row[1]." ".$row[2]." ".$row[3];
		} else {
			$hash_report[$row[0]] = $row[1]." ".$row[2]." ".$row[3];
		} 
	}
	return $hash_report;
}
function getAnnotationReport3vs1 ($taskid) {
	$hash_report = array();
	$query = "select eval,type,user_id,count(*) from annotation left join sentence on annotation.sentence_num=sentence.num where ( LENGTH(sentence.annotators) - LENGTH(REPLACE(sentence.annotators, ',', '')) between 3 and 3 ) and sentence.finished = 'Y' and sentence.gold = 'N' and task_id=$taskid group by eval,type,user_id order by eval,type,user_id";
	$result_done = safe_query($query);	
	while ($row = mysql_fetch_row($result_done)) {
		if (isset($hash_report[$row[0]])) {
			$hash_report[$row[0]] .= ",".$row[1]." ".$row[2]." ".$row[3];
		} else {
			$hash_report[$row[0]] = $row[1]." ".$row[2]." ".$row[3];
		} 
	}
	return $hash_report;
}
function getAgreementSentences ($taskid) {
	$query = "select id,lang,linkto,output_id,annotation.user_id as user_id,eval,evalids,text,tokenization,sentence_num from annotation left join sentence on annotation.output_id=sentence.num where sentence.gold = 'N' and task_id=$taskid AND type != 'source' AND type != 'reference' order by num,user_id"; //order by linkto,output_id,user_id,eval
	#print $query;
	return safe_query($query);	
}

function getAgreementSentencesGold ($taskid) {
	$query = "select id,lang,linkto,output_id,annotation.user_id as user_id,eval,evalids,text,tokenization,sentence_num from annotation left join sentence on annotation.output_id=sentence.num where sentence.gold = 'Y' and task_id=$taskid AND type != 'source' AND type != 'reference' order by num,user_id"; //order by linkto,output_id,user_id,eval
	#print $query;
	return safe_query($query);	
}

function getAgreementSentencesNotSolved ($taskid) {
	$query = "select id,lang,linkto,output_id,annotation.user_id as user_id,eval,evalids,text,tokenization,sentence_num from annotation left join sentence on annotation.sentence_num=sentence.num where finished = 'C' and sentence.gold = 'N' and task_id=$taskid AND type = 'source' AND type != 'reference' order by num,user_id"; //order by linkto,output_id,user_id,eval
	#print $query;
	return safe_query($query);	
}
function getAgreementSentences3vs1Userbased ($taskid,$uid) {
	$query = "select id,lang,linkto,output_id,annotation.user_id as user_id,eval,evalids,text,tokenization,sentence_num from annotation left join sentence on annotation.sentence_num=sentence.num where sentence.annotators like '%".$uid."%' and LENGTH(annotators) - LENGTH(REPLACE(annotators, ',', '')) between 3 and 3 and finished = 'Y' and sentence.gold = 'N' and task_id=$taskid AND type = 'source' AND type != 'reference' order by num,user_id"; //order by linkto,output_id,user_id,eval
	#print $query;
	return safe_query($query);	
}
function getAgreementSentences3vs1 ($taskid) {
	$query = "select id,lang,linkto,output_id,annotation.user_id as user_id,eval,evalids,text,tokenization,sentence_num from annotation left join sentence on annotation.sentence_num=sentence.num where LENGTH(annotators) - LENGTH(REPLACE(annotators, ',', '')) between 3 and 3 and finished = 'Y' and sentence.gold = 'N' and task_id=$taskid AND type = 'source' AND type != 'reference' order by num,user_id"; //order by linkto,output_id,user_id,eval
	#print $query;
	return safe_query($query);	
}
#this function return the previous and next ids and the counter of a sentence
function getPrevNext ($taskid, $id) {
	$prevnext = array("","");
	$source_sentences = getSourceSentences($taskid);
	if (count($source_sentences) > 0) {
		$prev="";
		$i = 1;
		while (list($k,$arr) = each($source_sentences)) {
			if ($k == $id) {
				if (list($next,$arr1) = each($source_sentences)) {
					return array($prev, $next, $i);
				}
			}
			$prev=$k;
			$i++;
		}
		return array($prev,"", $i);
	}
	return array("","", 0);
}

function getNextFBKJunior_AL ($taskid, $uid) {
 // if a file is assigned but not done, keep insisting on him
	$query = "select sentence_num as cnum from done where done.completed = 'N' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$snum=$row[0];
			return $snum;
		}
	}
	//Give gold
	 // check if it is the point to give gold for that user. if annotation div % 5 =0 then give the next of the gold.
	$query = "select count(sentence_num) as cnum from done where done.completed = 'Y' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$cnum=$row[0];
			if(($cnum % 5) ==0 ){
				$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where sentence.gold = 'Y' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
				$result_annotators = safe_query($query);
				$annotators_count = mysql_num_rows($result_annotators);		
				if ($annotators_count > 0) {					
					while ($row = mysql_fetch_row($result_annotators)) {
						$snum=$row[0];
						$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			  			safe_query($query);
			  			return $snum;
					}
				}
				
			}
			
		}
	}
	
	//check if a retraining point
	$query = "SELECT name,cycle,maxlimit,counter FROM clusters where counter %120 = 119  ;"; 
	
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$name=$row[0];
			$cycle=$row[1];
			$maxlimit=$row[2];
			$counter=$row[3];
			$query = "update clusters set cycle='".($cycle+1)."', counter='".($counter+1)."' where name = '".$name."';";
			safe_query($query);
			// if there is result, do retraining on 50 files, increase the cycle number
	        retrainAW();
	       /* $causal=" and category='POSITIVO' order by confidence asc ";
				$rre= getNextFileFromClusterAW($name,$uid,$causal);
				if($rre!=-1){
					return $rre;
				}*/
				$causal=" order by confidence asc ";
				$rre= getNextFileFromClusterAW($name,$uid,$causal);
				if($rre!=-1){
					//$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				   // safe_query($query);
					return $rre;
				}
			
		}
	}
	// if there is no result, 
	
	
  // $query = " SELECT name,counter FROM clusters where maxlimit >= counter and (select count(num) from sentence where cluster=clusters.name and sentence.gold = 'N' and sentence.finished = 'N' and sentence.type= 'source' ) > 0 order by cycle,name asc; ";
	$query=" SELECT name,counter,confidence,num FROM clusters,sentence where maxlimit >= counter and cluster=clusters.name and sentence.gold = 'N' and sentence.finished = 'N' and sentence.type= 'source'  order by cycle,name asc,confidence asc; ";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$name=$row[0];
			$counter=$row[1];
			/*$causal=" and category='POSITIVO' order by confidence asc ";
			$rre= getNextFileFromClusterAW($name,$uid,$causal);
				if($rre!=-1){
				$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				safe_query($query);
					return $rre;
				}*/
			$causal=" order by confidence asc ";
			$rre= getNextFileFromClusterAW($name,$uid,$causal);
			if($rre!=-1){
				$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				safe_query($query);
				return $rre;
			}
		}
	}
	
	return -1;
}

function getNextAW_AL ($taskid, $uid) {
 // if a file is assigned but not done, keep insisting on him
	$query = "select sentence_num as cnum from done where done.completed = 'N' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$snum=$row[0];
			return $snum;
		}
	}
	//check if a retraining point
	$query = "SELECT name,cycle,maxlimit,counter FROM clusters where counter %51 =50 and maxlimit > counter ;"; 
	
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$name=$row[0];
			$cycle=$row[1];
			$maxlimit=$row[2];
			$counter=$row[3];
			$query = "update clusters set cycle='".($cycle+1)."' where name = '".$name."';";
			safe_query($query);
			// if there is result, do retraining on 50 files, increase the cycle number
	        retrainAW();
			//IMP give the file number 51 
			if($maxlimit>=$counter){
				$causal=" and category='POSITIVO' order by confidence desc ";
				$rre= getNextFileFromClusterAW($name,$uid,$causal);
				//increase its counter any way, even if there is not next file, so it won't retrain in case if stopped at mod 51 = 50
				$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				safe_query($query);
				if($rre!=-1){
					return $rre;
				}
				
				$causal=" and category='NEUTRO' order by confidence desc ";
				$rre= getNextFileFromClusterAW($name,$uid,$causal);
				if($rre!=-1){
					return $rre;
				}
				$causal=" and category='NEGATIVO' order by confidence desc ";
				$rre= getNextFileFromClusterAW($name,$uid,$causal);
				if($rre!=-1){
					return $rre;
				}
			
				$causal=" and category!= 'N/A' order by confidence asc ";
				$rre= getNextFileFromClusterAW($name,$uid,$causal);
				if($rre!=-1){
					return $rre;
				}
				
				$causal=" order by confidence asc ";
				$rre= getNextFileFromClusterAW($name,$uid,$causal);
				if($rre!=-1){
					return $rre;
				}
			}
		}
	}
	// if there is no result, 
	
	
  // $query = " SELECT name,counter FROM clusters where maxlimit >= counter and (select count(num) from sentence where cluster=clusters.name and sentence.gold = 'N' and sentence.finished = 'N' and sentence.type= 'source' ) > 0 order by cycle,name asc; ";
	$query=" SELECT name,counter,confidence,num FROM clusters,sentence where maxlimit >= counter and cluster=clusters.name and sentence.gold = 'N' and sentence.finished = 'N' and sentence.type= 'source'  order by cycle,name asc,confidence asc; ";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$name=$row[0];
			$counter=$row[1];
			
			$causal=" and category='POSITIVO' order by confidence desc ";
			$rre= getNextFileFromClusterAW($name,$uid,$causal);
			if($rre!=-1){
				$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				safe_query($query);
				return $rre;
			}
			$causal=" and category='NEUTRO' order by confidence desc ";
			$rre= getNextFileFromClusterAW($name,$uid,$causal);
			if($rre!=-1){
				$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				safe_query($query);
				return $rre;
			}
			$causal=" and category='NEGATIVO' order by confidence desc ";
			$rre= getNextFileFromClusterAW($name,$uid,$causal);
			if($rre!=-1){
				$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				safe_query($query);
				return $rre;
			}
			
			$causal=" and category!= 'N/A' order by confidence asc ";
			$rre= getNextFileFromClusterAW($name,$uid,$causal);
			if($rre!=-1){
				$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				safe_query($query);
				return $rre;
			}
			
			$causal=" order by confidence asc ";
			$rre= getNextFileFromClusterAW($name,$uid,$causal);
			if($rre!=-1){
				$query = "update clusters set counter='".($counter+1)."' where name = '".$name."';";
				safe_query($query);
				return $rre;
			}
		}
	}
	
	return -1;
}

function exportAgreedAWFinal_Formate2($filecsv){
	
    $taskid=158;
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);			
			$fh=fopen($filecsv,"w"); 
			$query = "SELECT s2.lang,s2.text,s2.tokenization,s2.num,s1.id FROM sentence s1,sentence s2,done WHERE done.sentence_num=s1.num and done.completed='Y' and s1.finished ='Y' and  s1.gold='N' and s1.type='source' and s1.num=s2.linkto and s2.type='sys1'  ;";
			$my_result_annotation= safe_query($query);
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_annotatedTokens = array();
			fwrite($fh,"\"adjudicated_category\",\"text\"\n");
			
			while ($row_source = mysql_fetch_row($my_result_annotation)) {	
						//document level task
						$taga=getTagDocumentLevelAW($row_source[3],$taskranges);
						if($taga=="NEGATIVO"||$taga=="POSITIVO"||$taga=="NEUTRO"){
						if($taga=="NEGATIVO"){
							$taga="NEG";
						}
						if($taga=="POSITIVO"){
							$taga="POS";
						}
						if($taga=="NEUTRO"){
							$taga="NEU";
						}
						$tre=str_replace('"','\"',$row_source[1]);
						//$tre=str_replace(array("\r\n", "\n", "\r"), '\\n', $tre);
						$tre=str_replace(array("\r\n", "\n", "\r"), ' ', $tre);
						fwrite($fh,"\"".$taga."\",\"".$tre."\"\n");
						}
						$taga="";
				}
		//echo $my_tot_row;
		fflush($fh);
		fclose($fh);
}
function exportAgreedAW($filecsv,$causal){
	
    $taskid=158;
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);			
			$fh=fopen($filecsv,"w"); 
			$query = "SELECT s2.lang,s2.text,s2.tokenization,s2.num,s1.id FROM sentence s1,sentence s2,done WHERE done.sentence_num=s1.num and done.completed='Y' and s1.finished ='Y' and  s1.gold='N' and s1.type='source' and s1.num=s2.linkto and s2.type='sys1' ".$causal." ;";
			$my_result_annotation= safe_query($query);
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_annotatedTokens = array();
			fwrite($fh,"\"id\",\"twitter_id\",\"text\",\"tag\"\n");
			
			while ($row_source = mysql_fetch_row($my_result_annotation)) {	
						//document level task
						$taga=getTagDocumentLevelAW($row_source[3],$taskranges);
						$tre=str_replace('"','\"',$row_source[1]);
						//$tre=str_replace(array("\r\n", "\n", "\r"), '\\n', $tre);
						$tre=str_replace(array("\r\n", "\n", "\r"), ' ', $tre);
						fwrite($fh,"\"".$row_source[3]."\",\"".$row_source[4]."\",\"".$tre."\",\"".$taga."\"\n");

						$taga="";
				}
		//echo $my_tot_row;
		fflush($fh);
		fclose($fh);
}
function exportWaitAnnotator($filecsv,$causal){
	
    $taskid=158;
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);			
			$fh=fopen($filecsv,"w"); 
			$query = "SELECT s2.lang,s2.text,s2.tokenization,s2.num,s1.id FROM sentence s1,sentence s2,done WHERE done.sentence_num=s1.num and done.completed='Y' and s1.finished ='W' and  s1.gold='N' and s1.type='source' and s1.num=s2.linkto and s2.type='sys1' ".$causal." ;";
			$my_result_annotation= safe_query($query);
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_annotatedTokens = array();
			fwrite($fh,"\"id\",\"twitter_id\",\"text\",\"tag\"\n");
			
			while ($row_source = mysql_fetch_row($my_result_annotation)) {	
						//document level task
						$taga=getTagDocumentLevelAW($row_source[3],$taskranges);
						$tre=str_replace('"','\"',$row_source[1]);
						//$tre=str_replace(array("\r\n", "\n", "\r"), '\\n', $tre);
						$tre=str_replace(array("\r\n", "\n", "\r"), ' ', $tre);
						fwrite($fh,"\"".$row_source[3]."\",\"".$row_source[4]."\",\"".$tre."\",\"".$taga."\"\n");

						$taga="";
				}
		//echo $my_tot_row;
		fflush($fh);
		fclose($fh);
}
function exportAgreedAWTestsetAWFinal_format2($filecsv,$causal){
	
    $taskid=158;
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);			
			$fh=fopen($filecsv,"w"); 
			$query = "SELECT s2.lang,s2.text,s2.tokenization,s2.num,s1.id FROM sentence s1,sentence s2 WHERE s1.finished ='Y' and  s1.gold='N' and s1.type='source' and s1.num=s2.linkto and s2.type='sys1' ".$causal." ;";
			$my_result_annotation= safe_query($query);
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_annotatedTokens = array();
			fwrite($fh,"\"adjudicated_category\",\"text\"\n");
			
			while ($row_source = mysql_fetch_row($my_result_annotation)) {	
						//document level task
						$taga=getTagDocumentLevelAW($row_source[3],$taskranges);
						if($taga=="NEGATIVO"||$taga=="POSITIVO"||$taga=="NEUTRO"){
						if($taga=="NEGATIVO"){
							$taga="NEG";
						}
						if($taga=="POSITIVO"){
							$taga="POS";
						}
						if($taga=="NEUTRO"){
							$taga="NEU";
						}
						$tre=str_replace('"','\"',$row_source[1]);
						//$tre=str_replace(array("\r\n", "\n", "\r"), '\\n', $tre);
						$tre=str_replace(array("\r\n", "\n", "\r"), ' ', $tre);
						fwrite($fh,"\"".$taga."\",\"".$tre."\"\n");
						}
						$taga="";
				}
		//echo $my_tot_row;
		fflush($fh);
		fclose($fh);
}
function exportAgreedAWTestsetAWFinal_format1($filecsv){
	
    $taskid=158;
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);			
			$fh=fopen($filecsv,"w"); 
			$query = "SELECT s2.lang,s2.text,s2.tokenization,s2.num,s1.id FROM sentence s1,sentence s2 WHERE s1.finished ='Y' and  s1.gold='N' and s1.type='source' and s1.num=s2.linkto and s2.type='sys1'  ;";
			$my_result_annotation= safe_query($query);
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_annotatedTokens = array();
			fwrite($fh,"\"fbk_id\",\"twitter_id\",\"text\",\"adjudicated_category\",\"list_of_categories\",\"external_knowledge\"\n");
			
			while ($row_source = mysql_fetch_row($my_result_annotation)) {	
						//document level task
						$taga=getTagDocumentLevelAWTestsetAWFinal_format1($row_source[3],$taskranges);
						$tre=str_replace('"','\"',$row_source[1]);
						//$tre=str_replace(array("\r\n", "\n", "\r"), '\\n', $tre);
						$tre=str_replace(array("\r\n", "\n", "\r"), ' ', $tre);
						fwrite($fh,"\"".$row_source[3]."\",\"".$row_source[4]."\",\"".$tre."\",\"".$taga[0]."\",\"".$taga[1]."\",\"0\"\n");

						$taga="";
				}
		//echo $my_tot_row;
		fflush($fh);
		fclose($fh);
}

function getTagDocumentLevelAW($output_id,$taskranges){
	$query = "SELECT output_id,eval,sentence_num FROM annotation where output_id=".$output_id." ;";
			$my_result_annotation = safe_query($query);
			//$my_tot_row = mysql_num_rows($my_result_annotation);
			$res=array();
			while ($my_row_annotation = mysql_fetch_row($my_result_annotation)) {	
					$my_label = $taskranges[$my_row_annotation[1]][0];
					if(!isset($res[$my_label])){
					  $res[$my_label]=0;
					}
					$res[$my_label]=$res[$my_label]+1;
			}
			
			$ann="O";
			$max=-1;
			foreach($res as $k=>$v){
				if($v>$max){
					$max=$v;
					$ann=$k;
					
				}
			}
			return $ann;	
}
function getTagDocumentLevelAWTestsetAWFinal_format1($output_id,$taskranges){
	$query = "SELECT output_id,eval,sentence_num FROM annotation where output_id=".$output_id." ;";
			$my_result_annotation = safe_query($query);
			//$my_tot_row = mysql_num_rows($my_result_annotation);
			$res=array();
			$anno="";
			while ($my_row_annotation = mysql_fetch_row($my_result_annotation)) {	
					$my_label = $taskranges[$my_row_annotation[1]][0];
					if(!isset($res[$my_label])){
					  $res[$my_label]=0;
					}
					$res[$my_label]=$res[$my_label]+1;
					if(strlen($anno)>0){
					$anno.=",";
				}
				$anno.=$my_label;
			}
			
			$ann="O";
			$max=-1;
			
			foreach($res as $k=>$v){
				if($v>$max){
					$max=$v;
					$ann=$k;
				}
				
			}
			return array($ann,$anno);	
}
function getNextFileFromClusterAW($clust,$uid,$causal){
/*
increase counter, assign it to the user by added it to done.
check if counter reach maxlimit or not, 
if there is no files in a cluster more to the next
*/
//give me all the sentences which i didn't annotate
	$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where cluster='".$clust."' and sentence.gold = 'N' and sentence.finished = 'N' and sentence.type= 'source' ".$causal." limit 4; ";
	
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
		    //make it finished = 'W'  wait if it is assigned to an annotator
			$snum=$row[0];
			
			$anns=trim($row[1]);
			$annr="";
			$annsize=1;
			if(strlen($anns)>0){
				$an = array_unique(explode(',', $anns));	
				array_push($an,$uid);
				$an = array_unique($an);
				$annsize=sizeof($an);
				foreach($an as $k=>$v){
				 if(strlen($annr)>0){
				 	$annr.= ",";
				 }
				 $annr.=$v;
				}
			}else{
				$annr=$uid;
			}
			
			//Y instead of W to mark it as done.
			  $query = "UPDATE sentence SET finished ='Y', annotators ='".$annr."' WHERE num ='".$snum."';";
			   safe_query($query);
			
			$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			safe_query($query);
			return $snum;
		}
	}
	
	return -1;
}

function retrainAW(){
			
 			$intDir=ACTIVELEARN_retrain_PATH;
			if (!is_dir($intDir)) {
				mkdir($intDir, 0777);
			}
			$date = date('Ymd_his', time());
			
			$intDir .= "/retrain_".$date."/";
			if (!is_dir($intDir)) {
				mkdir($intDir, 0777);
			}
			// export the train set in the folder of run
			
			$causal=getDoneExportedSentencelistAW($userid);
			//sign the senterences to be retrained as retrained, and when checking the number of exported should be without those
			setDoneExportedSentencelistAsTrainedAW($causal);
			if(strlen($causal)>0){
			$causal= " AND s1.num IN ( ".$causal." ) ";
			 }
			 $filecsv = $intDir."dump";
			
			//inside save we takecare about the level of annotation Document or token.
			//saveIOB2FileActiveLearningAll($intDir, $taskid,$sentenceid, $userid,$causal,$filecsv);

			exportTrainAW($filecsv,$causal);
			//saveIOB2FileActiveLearningAll ($taskid, $userid, $causal,$filecsv,false);
		
			//copy the dump to the GOLD, and asign them as trained, then don't export trained.
			$fhfff=fopen($intDir."dump-files","w"); 
			fwrite($fhfff,$causal."\n");
						// get the name of the next two clusters to update their confidence
			$commandline = ACTIVELEARN_retrain_script_PATH." ".$intDir;
			$commandline='sh -c \' '.$commandline.' > '.$intDir.'executeLog-retrain-1.txt 2> '.$intDir.'executeLog-retrain.txt & \' ';
       		fwrite($fhfff,$commandline."\n");
       		$output = exec($commandline);	
       		
       		fflush($fhfff);
		fclose($fhfff);

}

function exportTrainAW($filecsv,$causal){
	
    $taskid=158;
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);			
			$fh=fopen($filecsv,"w"); 
			$query = "SELECT s2.lang,s2.text,s2.tokenization,s2.num,s1.id,s1.features FROM sentence s1,sentence s2,done WHERE done.sentence_num=s1.num and done.completed='Y' and s1.finished ='Y' and  s1.gold='N' and s1.type='source' and s1.num=s2.linkto and s2.type='sys1' ".$causal." ;";
			$my_result_annotation= safe_query($query);
			$my_tot_row = mysql_num_rows($my_result_annotation);
			//fwrite($fh,"\"id\",\"twitter_id\",\"text\",\"tag\"\n");
			
			while ($row_source = mysql_fetch_row($my_result_annotation)) {	
						//document level task
						$taga=getTagDocumentLevelAW($row_source[3],$taskranges);
						fwrite($fh,trim($row_source[4])."\t".trim($row_source[5])."\t".trim($taga)."\n");
						$taga="";
				}
		//echo $my_tot_row;
		fflush($fh);
		fclose($fh);
}
function getNextClusterAW(){
 $query = " SELECT name FROM clusters where maxlimit >= counter order by cycle,name asc limit 4; ";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	$ret="";
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
		if(strlen($ret)>0){
			$ret.=",";
		}
			$ret.=$row[0];
		}
	}
	return $ret;
}
function setDoneExportedSentencelistAsTrainedAW($causal){
	$query = "UPDATE done SET trained='Y' WHERE sentence_num IN ( $causal )  ";
	 safe_query($query);
}

# get all sentenceids of a user which has exported 'y'
function getDoneExportedSentencelistAW($userid) {
	$query = "SELECT sentence_num FROM done where completed='Y' and trained='N';";
	$result = safe_query($query);	
	$str="";
		if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			if(strlen($str)>0){
				$str.=", ";
			}
			$str.= $row[0];
		}
		}
	
	
	return $str;
}

function getALLDoneExportedSentencelistAW($userid) {
	$query = "SELECT sentence_num FROM done where completed='Y';";
	$result = safe_query($query);	
	$str="";
		if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result)) {
			if(strlen($str)>0){
				$str.=", ";
			}
			$str.= $row[0];
		}
		}
	
	
	return $str;
}



function getNextAW_EN_FR ($taskid, $uid) {
   // if a file is assigned but not done, keep insisting on him
   $query = "select sentence_num as cnum from done where done.completed = 'N' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$snum=$row[0];
			return $snum;
		}
	}
    // check if it is the point to give gold for that user. if annotation div % 5 =0 then give the next of the gold.
	$query = "select count(sentence_num) as cnum from done where done.completed = 'Y' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$cnum=$row[0];
			if(($cnum % 10) ==0 ){
				$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where sentence.gold = 'Y' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
				$result_annotators = safe_query($query);
				$annotators_count = mysql_num_rows($result_annotators);		
				if ($annotators_count > 0) {					
					while ($row = mysql_fetch_row($result_annotators)) {
						$snum=$row[0];
						$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			  			safe_query($query);
			  			return $snum;
					}
				}
				
			}
			
		}
	}
    //give me all the sentences which i didn't annotate
	$query = "SELECT sentence.num as snum, annotators as anns, sentence.finished FROM sentence where sentence.gold = 'N' and sentence.finished != 'Y' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
		    //make it finished = 'W'  wait if there are two annotated it, and this is the third.
			$snum=$row[0];
			$finsh=$row[2];
			$anns=trim($row[1]);
			$annr="";
			$annsize=1;
			if(strlen($anns)>0){
				$an = array_unique(explode(',', $anns));	
				array_push($an,$uid);
				$an = array_unique($an);
				$annsize=sizeof($an);
				foreach($an as $k=>$v){
				 if(strlen($annr)>0){
				 	$annr.= ",";
				 }
				 $annr.=$v;
				}
			}else{
				$annr=$uid;
			}
			
			if($finsh =='C'){
		    	$query = "UPDATE sentence SET finished ='Y', annotators ='".$annr."' WHERE num ='".$snum."';";
		    	safe_query($query);
			}else {
		    	$query = "UPDATE sentence SET finished ='C', annotators ='".$annr."' WHERE num ='".$snum."';";
		    	safe_query($query);
			}
			
			$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			safe_query($query);
			return $snum;
		}
	}
	
	return -1;
}
function getSYNCTokenLevel ($taskid) {
	$userCount=countAnnotatorsOfATask($taskid);
    //give me all the sentences which i didn't annotate
	//$query = "SELECT sentence.num as snum, annotation.user_id as uid FROM sentence where sentence.task_id = '".$taskid."' and sentence.gold = 'N' and ( sentence.finished = 'W' or sentence.finished = 'N') and sentence.type= 'source'  group by annotation.user_id, sentence.num;";
	$query = "SELECT sentence.num as snum, annotation.user_id as uid FROM sentence, done where sentence.task_id = '".$taskid."' and sentence.gold = 'N' and done.completed = 'Y' and done.sentence_num = sentence.num and ( sentence.finished = 'W' or sentence.finished = 'N') and sentence.type= 'source'  group by annotation.user_id, sentence.num;";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);	
	$sentUsr=array();
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			if(!isset($sentUsr[$row[0]])){
				$sentUsr[$row[0]]=array();
			}
			array_push($sentUsr[$row[0]],$row[1]);
		}
	}
	
	foreach( $sentUsr as $sent=>$evls){
		
		   $agree = false;
				if( (isset($mysession["modalityperdoc"])&&$mysession["modalityperdoc"]=="oneperdoc"&&sizeof($evls)>=1)     
					||
					(isset($mysession["modalityperdoc"])&&$mysession["modalityperdoc"]=="multiperdoc"&&
					  isset($mysession["agreementmodality"])&&$mysession["agreementmodality"]=="nannotatorsagreed"&&
					  isset($mysession["annagreed"])&&$mysession["annagreed"]>=sizeof($evls))
				){
					//do finished
					$query = "UPDATE sentence SET finished ='Y' WHERE num ='".$sent."';";
					safe_query($query);
					$agree=true;
				}
		
			if(sizeof($sentUsr[$sent])>=$userCount&&!$agree){
				$query = "UPDATE sentence SET finished ='C' WHERE num ='".$sent."';";
				safe_query($query);
			}
			
			if(!$agree){
				$query = "UPDATE sentence SET finished ='N' WHERE num ='".$sent."';";
				safe_query($query);
			}
	}
	return $sents;
}

function getNextTokenLevel ($taskid, $uid) {
   getSYNCTokenLevel ($taskid);
   // if a file is assigned but not done, keep insisting on him
   $query = "select sentence_num as cnum from done where done.completed = 'N' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$snum=$row[0];
			return $snum;
		}
	}
    // check if it is the point to give gold for that user. if annotation div % 5 =0 then give the next of the gold.
	$query = "select count(sentence_num) as cnum from done where done.completed = 'Y' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$cnum=$row[0];
			if(isset($mysession["goldtestEveryAnno"])&&($cnum % $mysession["goldtestEveryAnno"]) ==0 ){
				$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where sentence.task_id = '".$taskid."' and sentence.gold = 'Y' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
				$result_annotators = safe_query($query);
				$annotators_count = mysql_num_rows($result_annotators);		
				if ($annotators_count > 0) {					
					while ($row = mysql_fetch_row($result_annotators)) {
						$snum=$row[0];
						$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			  			safe_query($query);
			  			return $snum;
					}
				}
				
			}
			
		}
	}
    //give me all the sentences which i didn't annotate
	$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where sentence.task_id = '".$taskid."' and sentence.gold = 'N' and sentence.finished = 'N' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
		    //make it finished = 'W'  wait if there are two annotated it, and this is the third.
			$snum=$row[0];
			
			$anns=trim($row[1]);
			$annr="";
			$annsize=1;
			if(strlen($anns)>0){
				$an = array_unique(explode(',', $anns));	
				array_push($an,$uid);
				$an = array_unique($an);
				$annsize=sizeof($an);
				foreach($an as $k=>$v){
				 if(strlen($annr)>0){
				 	$annr.= ",";
				 }
				 $annr.=$v;
				}
			}else{
				$annr=$uid;
			}
			
			if( (isset($mysession["modalityperdoc"])&&$mysession["modalityperdoc"]=="multiperdoc"&&
					  isset($mysession["agreementmodality"])&&$mysession["agreementmodality"]=="nannotatorsagreed"&&
					  isset($mysession["annagreed"])&&$mysession["annagreed"]>=$annsize)
				)
			{
			  $query = "UPDATE sentence SET finished ='W', annotators ='".$annr."' WHERE num ='".$snum."';";
			   safe_query($query);
			}else{
				$query = "UPDATE sentence SET annotators ='".$annr."' WHERE num ='".$snum."';";
			   safe_query($query);
			}
			$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			safe_query($query);
			return $snum;
		}
	}
	
	return -1;
}



function getNextAW ($taskid, $uid) {
   getSYNCAW ($taskid);
   // if a file is assigned but not done, keep insisting on him
   $query = "select sentence_num as cnum from done where done.completed = 'N' and done.user_id = '".$uid."' ; ";
   
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$snum=$row[0];
			return $snum;
		}
	}

	$queryTask = "SELECT modalityperdoc, agreementmodality, annagreed, goldtestEveryAnno, id from task where id = ".$taskid.";";
        $result_task = safe_query($queryTask);
        $taskInfo = mysql_fetch_row($result_task);
        $modalityperdoc = $taskInfo[0];
        $agreementmodality = $taskInfo[1];
        $annagreed = $taskInfo[2];
	$goldtestEveryAnno = $taskInfo[3];

    // check if it is the point to give gold for that user. if annotation div % 5 =0 then give the next of the gold.
	$query = "select count(sentence_num) as cnum from done where done.completed = 'Y' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$cnum=$row[0];
			
			if($goldtestEveryAnno != 0 && (($cnum % $goldtestEveryAnno) == 0)){
			
			#if(isset($mysession["goldtestEveryAnno"])&&($cnum % $mysession["goldtestEveryAnno"]) ==0 ){
				$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where sentence.task_id = '".$taskid."' and sentence.gold = 'Y' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
					
			$result_annotators = safe_query($query);
				$annotators_count = mysql_num_rows($result_annotators);		
				
				if ($annotators_count > 0) {					
					while ($row = mysql_fetch_row($result_annotators)) {
						$snum=$row[0];
						$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			  			safe_query($query);
			  			return $snum;
					}
				}
				
			}
			
		}
	}

    //give me all the sentences which i didn't annotate
	$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where sentence.task_id = '".$taskid."' and sentence.gold = 'N' and sentence.finished = 'N' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
		    //make it finished = 'W'  wait if there are two annotated it, and this is the third.
			$snum=$row[0];
			
			$anns=trim($row[1]);
			$annr="";
			$annsize=1;
			if(strlen($anns)>0){
				$an = array_unique(explode(',', $anns));	
				array_push($an,$uid);
				$an = array_unique($an);
				$annsize=sizeof($an);
				foreach($an as $k=>$v){
				 if(strlen($annr)>0){
				 	$annr.= ",";
				 }
				 $annr.=$v;
				}
			}else{
				$annr=$uid;
			}
			
			//if( (isset($mysession["modalityperdoc"])&&$mysession["modalityperdoc"]=="multiperdoc"&&
			//		  isset($mysession["agreementmodality"])&&$mysession["agreementmodality"]=="nannotatorsagreed"&&
			//		  isset($mysession["annagreed"])&&$mysession["annagreed"]>=$annsize)
			//	)
			//{
	
			if( ($modalityperdoc=="multiperdoc"&&
                                          $agreementmodality=="nannotatorsagreed"&&
                                          $annagreed>=$annsize)
                                )
                        {


			  $query = "UPDATE sentence SET finished ='W', annotators ='".$annr."' WHERE num ='".$snum."';";
			   safe_query($query);
			}else{
				$query = "UPDATE sentence SET annotators ='".$annr."' WHERE num ='".$snum."';";
			   safe_query($query);
			}
			$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			safe_query($query);
			return $snum;
		}
	}
	
	return -1;
}

function getNextFBKJUNIOR ($taskid, $uid) {
   getSYNCAW_FBKJUNIOR ($taskid);
   // if a file is assigned but not done, keep insisting on him
   $query = "select sentence_num as cnum from done where done.completed = 'N' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$snum=$row[0];
			return $snum;
		}
	}
    // check if it is the point to give gold for that user. if annotation div % 5 =0 then give the next of the gold.
	$query = "select count(sentence_num) as cnum from done where done.completed = 'Y' and done.user_id = '".$uid."' ; ";
   $result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			$cnum=$row[0];
			if(($cnum % 10) ==0 ){
				$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where sentence.gold = 'Y' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
				$result_annotators = safe_query($query);
				$annotators_count = mysql_num_rows($result_annotators);		
				if ($annotators_count > 0) {					
					while ($row = mysql_fetch_row($result_annotators)) {
						$snum=$row[0];
						$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			  			safe_query($query);
			  			return $snum;
					}
				}
				
			}
			
		}
	}
    //give me all the sentences which i didn't annotate
	$query = "SELECT sentence.num as snum, annotators as anns FROM sentence where sentence.gold = 'N' and sentence.finished = 'N' and sentence.type= 'source' and num not in (select annotation.sentence_num from annotation where annotation.user_id ='".$uid."' )  order by num limit 4; ";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);		
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
		    //make it finished = 'W'  wait if there are two annotated it, and this is the third.
			$snum=$row[0];
			
			$anns=trim($row[1]);
			$annr="";
			$annsize=1;
			if(strlen($anns)>0){
				$an = array_unique(explode(',', $anns));	
				array_push($an,$uid);
				$an = array_unique($an);
				$annsize=sizeof($an);
				foreach($an as $k=>$v){
				 if(strlen($annr)>0){
				 	$annr.= ",";
				 }
				 $annr.=$v;
				}
			}else{
				$annr=$uid;
			}
			
			
			if($annsize>=2){
			  $query = "UPDATE sentence SET finished ='W', annotators ='".$annr."' WHERE num ='".$snum."';";
			   safe_query($query);
			}else{
				$query = "UPDATE sentence SET annotators ='".$annr."' WHERE num ='".$snum."';";
			   safe_query($query);
			}
			$query = "INSERT INTO done (sentence_num, user_id, completed) VALUES ('".$snum."', '".$uid."','N');";
			safe_query($query);
			return $snum;
		}
	}
	
	return -1;
}
function getSYNCAW_FBKJUNIOR ($taskid) {
    //give me all the sentences which i didn't annotate
	$query = "SELECT sentence.num as snum, annotation.user_id as uid, annotation.eval as ueval FROM annotation, sentence, done where sentence.gold = 'N' and done.completed = 'Y' and done.sentence_num = annotation.sentence_num and ( sentence.finished = 'W' or sentence.finished = 'N') and sentence.type= 'source' and annotation.sentence_num = sentence.num group by annotation.user_id, sentence.num, annotation.eval;";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);	
	$sents=array();	
	$sentUsr=array();
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			if(!isset($sents[$row[0]][$row[2]])){
				$sents[$row[0]][$row[2]]=array();
			}
			array_push($sents[$row[0]][$row[2]],$row[1]); 
			if(!isset($sentUsr[$row[0]])){
				$sentUsr[$row[0]]=array();
			}
			array_push($sentUsr[$row[0]],$row[1]);
		}
	}
	
	foreach( $sents as $sent=>$evls){
		
		if(sizeof($sentUsr[$sent])>=2){
		   $agree = false;
			foreach( $evls as $k=>$v){
				if(sizeof($v)>=2){
					//do finished
					$query = "UPDATE sentence SET finished ='Y' WHERE num ='".$sent."';";
					safe_query($query);
					$agree=true;
				}
			}
			if(sizeof($sentUsr[$sent])>=3&&!$agree){
				$query = "UPDATE sentence SET finished ='C' WHERE num ='".$sent."';";
				safe_query($query);
			}
			
			if((sizeof($sentUsr[$sent])==2)&&!$agree){
				$query = "UPDATE sentence SET finished ='N' WHERE num ='".$sent."';";
				safe_query($query);
			}
		}
	}
	return $sents;
}
function getSYNCAW_FBKJUNIOR_twoAnnotators ($taskid) {
    //give me all the sentences which i didn't annotate
	$query = "SELECT sentence.num as snum, annotation.user_id as uid, annotation.eval as ueval FROM annotation, sentence, done where sentence.gold = 'N' and done.completed = 'Y' and done.sentence_num = annotation.sentence_num and ( sentence.finished = 'W' or sentence.finished = 'N') and sentence.type= 'source' and annotation.sentence_num = sentence.num group by annotation.user_id, sentence.num, annotation.eval;";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);	
	$sents=array();	
	$sentUsr=array();
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			if(!isset($sents[$row[0]][$row[2]])){
				$sents[$row[0]][$row[2]]=array();
			}
			array_push($sents[$row[0]][$row[2]],$row[1]); 
			if(!isset($sentUsr[$row[0]])){
				$sentUsr[$row[0]]=array();
			}
			array_push($sentUsr[$row[0]],$row[1]);
		}
	}
	
	foreach( $sents as $sent=>$evls){
		
		if(sizeof($sentUsr[$sent])>=2){
		   $agree = false;
			foreach( $evls as $k=>$v){
				if(sizeof($v)==2){
					//do finished
					$query = "UPDATE sentence SET finished ='Y' WHERE num ='".$sent."';";
					safe_query($query);
					$agree=true;
				}
			}
			if(sizeof($sentUsr[$sent])==2&&!$agree){
				$query = "UPDATE sentence SET finished ='C' WHERE num ='".$sent."';";
				safe_query($query);
			}
			
			if((sizeof($sentUsr[$sent])==1)&&!$agree){
				$query = "UPDATE sentence SET finished ='N' WHERE num ='".$sent."';";
				safe_query($query);
			}
		}
	}
	return $sents;
}

function countAnnotatorsOfATask($taskid) {
	$query = "SELECT distinct user.id as uid FROM user, usertask WHERE user.id= usertask.user_id and user.status = 'annotator' and usertask.task_id = '".$taskid."' ";
	return mysql_num_rows(safe_query($query));
}


function getSYNCAW ($taskid) {

	$userCount=countAnnotatorsOfATask($taskid);

	$queryTask = "SELECT modalityperdoc, agreementmodality, annagreed from task where id = '".$taskid."';";
	$result_task = safe_query($queryTask); 
	$taskInfo = mysql_fetch_row($result_task);
	$modalityperdoc = $taskInfo[0];
	$agreementmodality = $taskInfo[1];
	$annagreed = $taskInfo[2];

    //give me all the sentences which i didn't annotate
 	//$query = "SELECT sentence.num as snum, annotation.user_id as uid, annotation.eval as ueval FROM annotation, sentence where sentence.task_id = '".$taskid."' and sentence.gold = 'N' and ( sentence.finished = 'W' or sentence.finished = 'N') and sentence.type= 'source' and annotation.sentence_num = sentence.num group by annotation.user_id, sentence.num, annotation.eval;";
	$query = "SELECT sentence.num as snum, annotation.user_id as uid, annotation.eval as ueval FROM annotation, sentence, done where sentence.task_id = '".$taskid."' and sentence.gold = 'N' and done.completed = 'Y' and done.sentence_num = annotation.sentence_num and ( sentence.finished = 'W' or sentence.finished = 'N') and sentence.type= 'source' and annotation.sentence_num = sentence.num group by annotation.user_id, sentence.num, annotation.eval;";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);	
	$sents=array();	
	$sentUsr=array();
	
	if ($annotators_count > 0) {					
		while ($row = mysql_fetch_row($result_annotators)) {
			if(!isset($sents[$row[0]][$row[2]])){
				$sents[$row[0]][$row[2]]=array();
			}
			
			array_push($sents[$row[0]][$row[2]],$row[1]); 
			if(!isset($sentUsr[$row[0]])){
				$sentUsr[$row[0]]=array();
			}
			array_push($sentUsr[$row[0]],$row[1]);
		}
	}
	
	foreach( $sents as $sent=>$evls){
			
		//if(sizeof($sentUsr[$sent])>=3){
		   $agree = false;
			foreach( $evls as $k=>$v){
	
		
//		if( (isset($mysession["modalityperdoc"])&&$mysession["modalityperdoc"]=="oneperdoc"&&sizeof($v)>=1)     
//					||
//					(isset($mysession["modalityperdoc"])&&$mysession["modalityperdoc"]=="multiperdoc"&&
//					  isset($mysession["agreementmodality"])&&$mysession["agreementmodality"]=="nannotatorsagreed"&&
//					  isset($mysession["annagreed"])&&$mysession["annagreed"]>=sizeof($v))				
//){

			if( ($modalityperdoc=="oneperdoc"&&sizeof($v)>=1)     
                                        ||
                                        ($modalityperdoc=="multiperdoc"&&
                                          $agreementmodality=="nannotatorsagreed"&&
                                          $annagreed<=sizeof($v))                          
){

					//do finished
					$query = "UPDATE sentence SET finished ='Y' WHERE num ='".$sent."';";
					safe_query($query);
					$agree=true;
				}
			}
			if(sizeof($sentUsr[$sent])>=$userCount&&!$agree){
				$query = "UPDATE sentence SET finished ='C' WHERE num ='".$sent."';";
				safe_query($query);
			}
			
			if(!$agree){
				$query = "UPDATE sentence SET finished ='N' WHERE num ='".$sent."';";
				safe_query($query);
			}
		//}
	}

	return $sents;
}
	
### EXPORT FUNCTIONS ###

function saveCSVFile ($intDir, $taskid, $userid="") {
	$taskinfo = getTaskInfo($taskid);	
	$taskname=$taskinfo["name"];
	$tasktype=$taskinfo["type"];
	$taskranges = rangesJson2Array($taskinfo["ranges"]);
	
	$tasksyscount=countTaskSystem ($taskid);
	$query_clause = "";
	if (isset($userid) && $userid != "") {
		$query_clause = "AND user_id=$userid";
	}
	
	//count the number of annotators for the current task
	$query = "select distinct user_id from annotation LEFT JOIN sentence ON annotation.output_id=sentence.num WHERE task_id=$taskid $query_clause";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);
	#print "TASK: ". $taskid . " ($query) annotations:".$annotators_count."<br>";
		
	if ($annotators_count > 0) {					
		//loop on users
		while ($row = mysql_fetch_row($result_annotators)) {
			$userid=$row[0];
			$sentence_done = getDoneSentences($taskid,$userid);
			$filecsv = $intDir.$taskname."_ann".$userid.".csv";
			$fh=fopen($filecsv,"w"); 
			
			//save comments
			$comments = getComments($taskid,$userid);	
			/*
			if (count($comments) > 0) {
				$filecsv_comment = $intDir.$taskname."_comment".$userid.".csv";
				$fh_comment=fopen($filecsv_comment,"w"); 
				fwrite($fh_comment,"ID\ttype\tcomment\n");
				while (list ($sentnum,$comment) = each($comments)) {
					//if (in_array($sentnum,$sentence_done)) {
						fwrite($fh_comment,	str_replace("&quot;","\"",$comment)."\n");
					//}
				}
				fclose($fh_comment); 				
			}
			*/
				
			
			#print "FILE : $fh $filecsv <br>";
			$count_ann=0;
			#print "<h3>USER: ".$row[0]."</h3> $filecsv [$tasktype]";
				
			if (preg_match("/quality/i", $tasktype)) {
				fwrite($fh,"ID\tlanguage_pair\tsystem\tscore\ttarget_tok_num\ttarget_text\tsource_tok_num\tsource_text\n");
			} else if (preg_match("/errors/i", $tasktype)) {
			fwrite($fh,"ID\tlanguage_pair\tsystem\tannotation_typeid\tannotation_label\ttokenIDs\ttarget_tok_num\ttokenized_target_text\tsource_tok_num\tsource_text\tcomment\n");
			} else if (preg_match("/wordaligner/i", $tasktype)) {
				fwrite($fh,"ID\tlanguage_pair\tsystem\tannotation_typeid\tannotation_label\ttokenIDs\ttarget_tok_num\ttokenized_target_text\tsource_tok_num\ttokenized_source_text\tcomment\n");
			} else {
				fwrite($fh,"ID\tlanguage_pair\tsystem\tannotation_typeid\tannotation_label\ttokenIDs\ttarget_tok_num\ttarget_text\tsource_tok_num\tsource_text\n");
			}	
			
			$query = "SELECT output_id,id,type,eval,evalids,sentence_num,lang,text,tokenization FROM annotation LEFT JOIN sentence ON annotation.output_id=sentence.num WHERE user_id=".$userid." AND task_id=".$taskid." order by id;";
				
			$result_annotation = safe_query($query);
			saveLog($taskid . " " . $taskname . " " . mysql_num_rows($result_annotation) . " " . $query);
			$last_id = "";
			$src_text="";
			while ($row_annotation = mysql_fetch_row($result_annotation)) {
				if (in_array($row_annotation[5],$sentence_done)) {
					if ($last_id != $row_annotation[1]) {
						$last_id = $row_annotation[1];
							
						#get source data
						$query = "SELECT lang,text,tokenization FROM sentence WHERE task_id=$taskid AND id='".$row_annotation[1]."' AND type='source'";
						$result_source = safe_query($query);
						$row_source = mysql_fetch_row($result_source);
						
						$src_text = preg_replace("/[\n|\r]/","",preg_replace("/\t+/"," ",$row_source[1]));
					}
					$label = $taskranges[$row_annotation[3]][0];
								
					$text = preg_replace("/[\n|\r]/","",preg_replace("/\t+/"," ",$row_annotation[7]));
					$trg_tokens = getTokens(preg_replace("/.*_/","", $row_annotation[6]), $text,$row_annotation[8]);
					#if (isset($hash_common_taskanns[$row_annotation[5]])) { 
					$src_tokens = getTokens($row_source[0],$src_text,$row_source[2]);
							
					if (preg_match("/quality/i", $tasktype)) {
						fwrite($fh,$row_annotation[1] ."\t". $row_source[0]."_".$row_annotation[6] ."\t" . $row_annotation[2] ."\t". $row_annotation[3] ."\t".count($trg_tokens)."\t$text\t".count($src_tokens)."\t".$src_text."\n");
					} else {
					  if (preg_match("/errors/i", $tasktype)) {
						$splitted_ids = explode(",",preg_replace("/^,/","",$row_annotation[4]));
						$cleaned_ids = array();
						#$savelog=0;
						#if ($row_annotation[1] == "MI009_GSPPA-I_13_REP_en-11" && $taskid=4 && $row_annotation[2] == sys1 && $label == "Lexicon") { 
						#	$savelog =1;
						#}
						foreach ($splitted_ids as $item_ids) {
							$ids = explode(" ", trim($item_ids));
							#if ($savelog == 1) {
							#	saveLog("=>>>>> "  . trim($item_ids));
							#}
							$spaceIDs = array();
							$tokenIDs = array();
							foreach ($ids as $id) {
								if ($id != "") {
									if (preg_match('/-/',$id)) { 
										if (!in_array($id, $spaceIDs)) {
											array_push($spaceIDs, $id);
										}
									} else {
										if (!in_array($id, $tokenIDs)) {
											array_push($tokenIDs, $id);
										}
									}
								}
							}
							
							if (count($tokenIDs) > 0) {
								array_push($cleaned_ids, join(" ",$tokenIDs));
							} else if (count($spaceIDs) == 1) {
								array_push($cleaned_ids, join(" ",$spaceIDs));
							}
						}
						$strids = trim(join(",", $cleaned_ids));
						#if ($savelog == 1) {
						#	saveLog($row_annotation[4] . ">>>> " . $strids);
						#}
						if ($row_annotation[3] > 1 && $strids == "") {
							continue;
						}
													
						fwrite($fh,$row_annotation[1] ."\t". $row_source[0]."_".$row_annotation[6] ."\t". $row_annotation[2] ."\t".$row_annotation[3]."\t".$label ."\t". $strids ."\t". count($trg_tokens)."\t".join(" ", $trg_tokens)."\t".count($src_tokens)."\t".$src_text."\t"); 								
					  } else if (preg_match("/wordaligner/i", $tasktype)) {
						$list_ids=preg_split("/[ |,]+/", $row_annotation[4]);
						sort($list_ids);
                                        	fwrite($fh,$row_annotation[1] ."\t". $row_source[0]."_".$row_annotation[6] ."\t". $row_annotation[2] ."\t".$row_annotation[3] ."\t".$label."\t". join(" ",$list_ids) ."\t". count($trg_tokens)."\t".join(" ", $trg_tokens)."\t".count($src_tokens)."\t".join(" ", $src_tokens)."\t");
					  } else {
						fwrite($fh,$row_annotation[1] ."\t". $row_source[0]."_".$row_annotation[6] ."\t". $row_annotation[2] ."\t". $row_annotation[3] ."\t".$label."\t". $row_annotation[4] ."\t". count($trg_tokens)."\t$text\t".count($src_tokens)."\t".$src_text."\t"); 	
					  }
					  //write comments
					  if (isset($comments[$row_annotation[0]])) {
						fwrite($fh,$comments[$row_annotation[0]]);
					  }
					  fwrite($fh,"\n");
					}			
					
					
					$count_ann++;
					#print $row_annotation[0] ."\t". $row_annotation[1] ."\t". $row_annotation[2] ."\t". $row_annotation[3] ."\n";
				}
			}
			fclose($fh); 
			saveLog("SAVED FILE $filecsv: $count_ann $annotators_count $taskname " . mysql_num_rows($result_annotation));
			if ($count_ann == 0) {
				unlink($filecsv);
			}
			#print "Saved $count_ann annotations."; 
		}
	}	
}

function saveXMLFile ($intDir, $taskid, $userid="") {
	$taskinfo = getTaskInfo($taskid);	
	$taskname = $taskinfo["name"];
	$tasktype = $taskinfo["type"];
	$taskranges = rangesJson2Array($taskinfo["ranges"]);
	$tasksyscount=countTaskSystem ($taskid);
	
	$query_clause = "";
	if (isset($userid) && $userid != "") {
		$query_clause = "AND user_id=$userid";
	}
	
	//count the number of annotators for the current task
	$query = "select distinct user_id from annotation LEFT JOIN sentence ON annotation.output_id=sentence.num WHERE task_id=$taskid $query_clause";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);
	#print "TASK: ". $taskid . " ($query) annotations:".$annotators_count."<br>";
		
	if ($annotators_count > 0) {
		//loop on users
		while ($row = mysql_fetch_row($result_annotators)) {
			$userid=$row[0];
			$sentence_done = getDoneSentences($taskid,$userid);
			
			$filecsv = $intDir.$taskname."_ann".$userid.".xml";
			$fh=fopen($filecsv,"w"); 
			fwrite($fh,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<".$tasktype."_task>\n");
			#print "FILE : $fh $filecsv <br>";
			$count_ann=0;
			#print "<h3>USER: ".$row[0]."</h3> $filecsv";
			
			$query = "SELECT output_id,id,type,eval,evalids,sentence_num,lang,text,tokenization FROM annotation LEFT JOIN sentence ON annotation.output_id=sentence.num WHERE user_id=".$userid." AND task_id=".$taskid." order by id,type,eval;";
			$result_annotation = safe_query($query);
			saveLog($taskid . " " . $taskname . " " . mysql_num_rows($result_annotation) . " " . $query);
			$last_id = "";
			$system_id = "";
			$tokens=array();
			$sourcetokens=array();
				
			//get comments
			$comments = getComments($taskid,$userid);	
						
			while ($row_annotation = mysql_fetch_row($result_annotation)) {				
				if (in_array($row_annotation[5],$sentence_done)) {
					$label = $taskranges[$row_annotation[3]][0];
			
					if ($last_id != $row_annotation[1]) {
						if ($last_id != "") {
							fwrite($fh,"    </target>\n");	
							fwrite($fh,"  </system>\n </eval_item>\n");
						}
						$last_id = $row_annotation[1];
						$system_id = "";
						
						#get source data
						$query = "SELECT lang,text,tokenization FROM sentence WHERE task_id=$taskid AND id='".$row_annotation[1]."' AND type='source'";
						$result_source = safe_query($query);
						$row_source = mysql_fetch_row($result_source);
						fwrite($fh," <eval_item ID='".$last_id."' language_pair='".$row_source[0]."_".$row_annotation[6]."'>\n");
				
						$src_text = preg_replace("/[\n|\r]/","",preg_replace("/\t+/"," ",$row_source[1]));
						if ($src_text != "") {
							$sourcetokens = getTokens($row_source[0], $src_text, $row_source[2]);
							fwrite($fh,"  <source tok_num='".count($sourcetokens)."'>\n    <text>".xml_escape($src_text)."</text>\n");
								
							#add tokens
							if (preg_match("/wordaligner/i", $tasktype)) {
								$i=1;
								fwrite($fh,"    <tokens>\n");
								foreach ($sourcetokens as $token) {
									fwrite($fh,"      <token id='$i'>".xml_escape($token)."</token>\n");
									$i++;
								}
								fwrite($fh,"    </tokens>\n");								
							}
							fwrite($fh,"  </source>\n");
						}	
					}	
					if ($system_id != $row_annotation[2]) {
					
						if ($system_id != "") {
							fwrite($fh,"    </target>\n");	
							fwrite($fh,"  </system>\n");
						}
						$system_id = $row_annotation[2];
				
						$text = preg_replace("/[\n|\r]/","",preg_replace("/\t+/"," ",$row_annotation[7]));
						fwrite($fh,"  <system name='".$system_id."'");
						if (preg_match("/quality/i", $tasktype)) {
							fwrite($fh," score='".$row_annotation[3]."'");
						}
						fwrite($fh,">\n");
						if (count($comments) > 0) {
							if (array_key_exists($row_annotation[0], $comments)) {
								fwrite($fh,"    <comment>".preg_replace("/.+\t/","",$comments{$row_annotation[0]}) ."</comment>\n");
							}									
						}
						$tokens = getTokens(preg_replace("/.*_/","",$row_annotation[6]), $text, $row_annotation[8]);
						fwrite($fh,"    <target tok_num='".count($tokens)."'>\n      <text>".xml_escape($text)."</text>\n");	
						
						#add tokens
						$i=1;
						fwrite($fh,"      <tokens>\n");
						foreach ($tokens as $token) {
							fwrite($fh,"        <token id='$i'>".xml_escape($token)."</token>\n");
							$i++;
						}
						fwrite($fh,"      </tokens>\n");								
					}
					
					if (preg_match("/errors/i", $tasktype)) {
						$splitted_ids = explode(",",preg_replace("/^,/","",$row_annotation[4]));
						$cleaned_ids = array();
							
						foreach ($splitted_ids as $item_ids) {
							$ids = explode(" ", trim($item_ids));
							#if ($savelog == 1) {
							#	saveLog("=>>>>> "  . trim($item_ids));
							#}
							$spaceIDs = array();
							$tokenIDs = array();
							foreach ($ids as $id) {
								if ($id != "") {
									if (preg_match('/-/',$id)) { 
										if (!in_array($id, $spaceIDs)) {
											array_push($spaceIDs, $id);																						
										}
									} else {
										if (!in_array($id, $tokenIDs)) {
											array_push($tokenIDs, $id);
										}
									}
								}
							}
							
							if (count($tokenIDs) > 0) {
								array_push($cleaned_ids, join(" ",$tokenIDs));
							} else if (count($spaceIDs) == 1) {
								array_push($cleaned_ids, join(" ",$spaceIDs));
							}
						}
						$strids = trim(join(",", $cleaned_ids));
						
						#if ($savelog == 1) {
						#	saveLog($row_annotation[4] . ">>>> " . $strids);
						#}
						if ($row_annotation[3] > 1 && $strids == "") {
							continue;
						}
						fwrite($fh,"      <annotation type='error' typeid='".$row_annotation[3]."' label='".$label."'>\n");
						foreach ($cleaned_ids as $ids) {
							fwrite($fh,"        <span>\n");
							foreach (explode(" ", $ids) as $id) {
								fwrite($fh,"          <token id='$id'");
								if (preg_match('/-/',$id)) {
									fwrite($fh,"/>\n");
								} else {
									fwrite($fh,">".xml_escape($tokens[($id-1)])."</token>\n");
								} 	
							}
							fwrite($fh,"        </span>\n");								
						}							
						fwrite($fh,"      </annotation>\n");
					
					} else if (preg_match("/wordaligner/i", $tasktype)) {
						fwrite($fh,"      <annotation type='wordalign' typeid='".$row_annotation[3]."' label='$label'>\n");
						$splitted_ids = explode(" ",$row_annotation[4]);
							
						foreach ($splitted_ids as $id) {
							$xy = explode("-", $id);
							if (isset($sourcetokens[$xy[0]-1]) && isset($tokens[$xy[1]-1])) {
								fwrite($fh,"        <span>\n");
								fwrite($fh,"          <token from='source' id='".$xy[0]."'>".xml_escape($sourcetokens[$xy[0]-1])."</token>\n"); 	
								fwrite($fh,"          <token from='target' id='".$xy[1]."'>".xml_escape($tokens[$xy[1]-1])."</token>\n"); 	
								fwrite($fh,"        </span>\n");
							}
						}
						fwrite($fh,"      </annotation>\n");										
					}
				}
				$count_ann++;					  	
				#print $row_annotation[0] ."\t". $row_annotation[1] ."\t". $row_annotation[2] ."\t". $row_annotation[3] ."\n";	
			}
						
			if (mysql_num_rows($result_annotation) > 0 && count($sentence_done) > 0) {
				fwrite($fh,"    </target>\n");	
				fwrite($fh,"  </system>\n</eval_item>\n");
			}
			fwrite($fh,"</".$tasktype."_task>\n");				
			fclose($fh); 
				
			saveLog("SAVED FILE $filecsv: $count_ann $annotators_count $taskname " . mysql_num_rows($result_annotation));
			if ($count_ann == 0) {
				unlink($filecsv);
			}
			#print "Saved $count_ann annotations."; 
		}
	}
}
function activelearning($taskid,$userid,$sentenceid){
	//print($intDir+", "+$taskid+", "+$sentenceid+", "+ $userid);
	 //saveIOB2FileActiveLearning ($intDir, $taskid,$sentenceid, $userid);
	$GOLDRUN=false;
	 //here sign this sentence as ready to be added to the re-train
	 saveDoneExported($sentenceid,$userid,'Y');
	 //get if the sentence which have saveDoneExported greater than threshold then do retraining and trained='N' .
	// if(getDoneExportedCount()>ACTIVELEARN_retrain_threshold){
	if(getDoneExportedCount()== ACTIVELEARN_train_nbfiles || (getDoneExportedCount()>((getDoneExportedCountANDTRAINED()+ACTIVELEARN_train_nbfiles)*ACTIVELEARN_retrain_threshold))||(ACTIVELEARN_train_afterfirstfile&&getDoneExportedCountANDTRAINED()==0&&getDoneExportedCount()==1)){
	//if(ACTIVELEARN_retrain_active&&(((getDoneExportedCount()>=ACTIVELEARN_train_afterfirstfile_NUM)&&ACTIVELEARN_train_afterXfiles)||(getDoneExportedCount()>((getDoneExportedCountANDTRAINED()+ACTIVELEARN_train_nbfiles)*ACTIVELEARN_retrain_threshold))||(ACTIVELEARN_train_afterfirstfile&&getDoneExportedCountANDTRAINED()==0&&getDoneExportedCount()==1))){	
	 		 $intDir=ACTIVELEARN_retrain_PATH;
			if (!is_dir($intDir)) {
				mkdir($intDir, 0777);
			}
			$date = date('Ymd_his', time());
			
			$intDir .= "/retrain_".$date."/";
			if (!is_dir($intDir)) {
				mkdir($intDir, 0777);
			}
			// export the train set in the folder of run
			
			$causal=getDoneExportedSentencelist($userid);
			//sign the senterences to be retrained as retrained, and when checking the number of exported should be without those
			setDoneExportedSentencelistAsTrained($causal);
			if(strlen($causal)>0){
			$causal= " AND num IN ( ".$causal." ) ";
			 }
			 $filecsv = $intDir."dump";
			
			//inside save we takecare about the level of annotation Document or token.
			//saveIOB2FileActiveLearningAll($intDir, $taskid,$sentenceid, $userid,$causal,$filecsv);
			saveIOB2FileActiveLearningAll ($taskid, $userid, $causal,$filecsv,false);
		
			//copy the dump to the GOLD, and asign them as trained, then don't export trained.
			$fhfff=fopen($intDir."dump-files","w"); 
			fwrite($fhfff,$causal."\n");
			
			$commandline = ACTIVELEARN_retrain_script_PATH." ".$intDir;
       		$output = exec('sh -c \' '.$commandline.' > '.$intDir.'executeLog-retrain-1.txt 2> '.$intDir.'executeLog-retrain.txt & \' ');	
       		
       		// select form the GOLD corpus
       		if(ACTIVELEARN_train_GOLD_active){
       			$GOLDRUN=true;
       		}
	 }
	  $intDir=ACTIVELEARN_RUN_FOLDER;
		if (!is_dir($intDir)) {
			mkdir($intDir, 0777);
		}
		$date = date('Ymd_his', time());
	
		$intDir .= "/export_".$date.($GOLDRUN?"_GOLD":"")."/";
		if (!is_dir($intDir)) {
			mkdir($intDir, 0777);
			exec("chmod -R 777 ".$intDir);
		}
		$filecsv = "";
	$new_sentenceid ="";
	 if(ACTIVELEARN_RandomSearch_Active ==false){
		$filecsv = $intDir."export".$userid."-".$sentenceid.".iob2";

		if(ACTIVELEARN_annotation_problem == "tokenlevel"){ //token level task
			saveIOB2FileActiveLearning ($intDir, $taskid,$sentenceid, $userid);
		}else{
			saveIOB2FileActiveLearningDocumentLevel($intDir, $taskid,$sentenceid, $userid);
		}
		 //call to extract the correction list	 
        $commandline = ACTIVELEARN_pipline_CMD.$filecsv.($GOLDRUN?" -GOLD ":"");
        $output = exec($commandline.' 2> '.$intDir.'executeLog.txt',$output);	
        
	 // select a file from corpus folder
	 
	 //move it to this folder and then process it with the pipeline then insert it to the system.
	// $filepath=$filecsv;
	$dataLE= explode(';', $output); // link;errorFromWhereisSelected
	
     $filepath=$dataLE[0];
	 $filename=basename($filepath);
	 $type="source";
	 $tokenization=5;
	$new_sentenceid =  addFileDataMine ($taskid,$type,$tokenization,$filepath,$filename, $userid,$filecsv,$dataLE[1]);
	}else{
		$commandline = ACTIVELEARN_RandomSearch_PATH.$intDir;
        $output = exec($commandline.' 2> '.$intDir.'executeLog.txt',$output);	        
    	$filepath=$output;
	 	$filename=basename($filepath);
	 	$type="source";
	 	$tokenization=5;
		$new_sentenceid = addFileData ($taskid,$type,$tokenization,$filepath,$filename, $userid);
		//echo $filepath."==".$filename."==".$new_sentenceid;
	}
	 
	 
	return $new_sentenceid;
}
function saveIOB2FileActiveLearningAll ($taskid, $userid="", $causal="",$filecsv,$header=false) {
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);	
	#print "TASK: ". $taskid . " ($query) annotations:".$annotators_count."<br>";
		
			$fh=fopen($filecsv,"w"); 
							
						#get source data
			$query = "SELECT lang,text,tokenization,num,id FROM sentence WHERE task_id=$taskid ".$causal." ;";


						$my_result_annotation= safe_query($query);
				
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_annotatedTokens = array();
			if(ACTIVELEARN_annotation_problem == "documentlevel"){ 
				fwrite($fh,"\"id\",\"twitter_id\",\"text\",\"tag\"\n");
			}
			while ($row_source = mysql_fetch_row($my_result_annotation)) {	
			   if(ACTIVELEARN_annotation_problem == "tokenlevel"){ //token level task
						if($header){
							fwrite($fh,"# FILEID: ".xml_escape($row_source[3])."\n");
						}
						echo $row_source[3]."</br>";
						#mantengo gli a capo
						$tokens = getTokens($row_source[0], preg_replace("/\n/"," __BR__ ", $row_source[1]), $row_source[2]);	
						
						$annotatedTokens=getSysAnnotation($taskid,$row_source[3],$userid,$taskranges);
						
						$i=1;
						foreach ($tokens as $token) {
							if ($token == "__BR__") {
								fwrite($fh,"\n");
							} else {
								fwrite($fh,xml_escape($token)."\t");
								
								if (isset($annotatedTokens{$i})) {
									fwrite($fh,trim($annotatedTokens{$i}));
								} else {
									fwrite($fh,"O");
								}
								
								
								fwrite($fh,"\n");
								
								$i++;
							}
						}
						fwrite($fh,"\n\n");
						$annotatedTokens = array();
					}else{
						//document level task
						$taga=getTagDocumentLevel($row_source[3],$taskranges);
						$tre=str_replace('"','\"',$row_source[1]);
						$tre=str_replace(array("\r\n", "\n", "\r"), '\\n', $tre);
						fwrite($fh,"\"".$row_source[4]."\",\"".$row_source[3]."\",\"".$tre."\",\"".$taga."\"\n");
						$taga="";
					}
				}
	fflush($fh);
	fclose($fh);
}

function saveIOB2FileActiveLearningDocumentLevel ($intDir, $taskid,$sentenceid, $userid="") {
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);	
	#print "TASK: ". $taskid . " ($query) annotations:".$annotators_count."<br>";
		
			$filecsv = $intDir."export".$userid."-".$sentenceid.".iob2";
			$fh=fopen($filecsv,"w"); 
			fwrite($fh,"\"id\",\"twitter_id\",\"text\",\"tag\"\n");
			$taga=getTagDocumentLevel($row_source[3],$taskranges);
			$has=getSentence($num, $taskid);
			fwrite($fh,"\"".$sentenceid."\",\"".$has["sys1"]["id"]."\",\"".str_replace('"','\"',$has["sys1"]["text"])."\",\"".$taga."\"\n");
}


function getTagDocumentLevel($output_id,$taskranges){
	$query = "SELECT output_id,eval,sentence_num FROM annotation where output_id=".$output_id." ;";
			$my_result_annotation = safe_query($query);
			//$my_tot_row = mysql_num_rows($my_result_annotation);
			while ($my_row_annotation = mysql_fetch_row($my_result_annotation)) {	
					$my_label = $taskranges[$my_row_annotation[1]][0];
					return $my_label;				
			}
			return "O";
}
function saveIOB2FileActiveLearningAllGold ( $taskid, $userid="",$causal="", $filecsv,$header=false) {
	$taskinfo = getTaskInfo($taskid);
	$taskranges = rangesJson2Array($taskinfo["ranges"]);
	#print "TASK: ". $taskid . " ($query) annotations:".$annotators_count."<br>";

	$fh=fopen($filecsv,"w");
	
		
		#get source data
		$query = "SELECT lang,text,tokenization,num  FROM sentence WHERE task_id=$taskid  ".$causal." ;";
		
		$my_result_annotation= safe_query($query);
		$my_tot_row = mysql_num_rows($my_result_annotation);
		$my_annotatedTokens = array();
		while ($row_source = mysql_fetch_row($my_result_annotation)) {
			if($header){
				fwrite($fh,"# FILEID: ".xml_escape($row_source[3])."\n");
			}
			echo $row_source[3]."</br>";
		
			$annotatedTokens=getSysAnnotation($taskid,$row_source[3],$userid,$taskranges);
			$my_annotatedTokens= getGoldAnnotationSave($taskid,$row_source[3],$userid,$taskranges);
				
		
		#mantengo gli a capo
		$tokens = getTokens($row_source[0], preg_replace("/\n/"," __BR__ ", $row_source[1]), $row_source[2]);
		//	fwrite($fh, "# FILE: ".$last_id."\n");

		$i=1;
		foreach ($tokens as $token) {
			if ($token == "__BR__") {
				fwrite($fh,"\n");
			} else {
				fwrite($fh,xml_escape($token)."\t");
				if (isset($my_annotatedTokens{$i})) {
					fwrite($fh,trim($my_annotatedTokens{$i})."\t");
				} else {
					fwrite($fh,"O"."\t");
				}
				if (isset($annotatedTokens{$i})) {
					fwrite($fh,trim($annotatedTokens{$i}));
				} else {
					fwrite($fh,"O");
				}


				fwrite($fh,"\n");

				$i++;
			}
		}
		fwrite($fh,"\n\n");
		$annotatedTokens = array();
	
		}

}



function saveIOB2FileActiveLearning ($intDir, $taskid,$sentenceid, $userid="") {
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);	
	#print "TASK: ". $taskid . " ($query) annotations:".$annotators_count."<br>";
		
			$filecsv = $intDir."export".$userid."-".$sentenceid.".iob2";
			$fh=fopen($filecsv,"w"); 
				$annotatedTokens=getSysAnnotation($taskid,$sentenceid,$userid,$taskranges);
				$my_annotatedTokens= getGoldAnnotationSave($taskid,$sentenceid,$userid,$taskranges);
			
			if (count($annotatedTokens) > 0) {
						#get source data
						$query = "SELECT lang,text,tokenization FROM sentence WHERE task_id=$taskid AND num='".$sentenceid."' AND type='source'";
						$result_source = safe_query($query);
						$row_source = mysql_fetch_row($result_source);
				
						#mantengo gli a capo
						$tokens = getTokens($row_source[0], preg_replace("/\n/"," __BR__ ", $row_source[1]), $row_source[2]);	
					//	fwrite($fh, "# FILE: ".$last_id."\n");
						
						$i=1;
						foreach ($tokens as $token) {
							if ($token == "__BR__") {
								fwrite($fh,"\n");
							} else {
								fwrite($fh,xml_escape($token)."\t");
								if (isset($my_annotatedTokens{$i})) {
									fwrite($fh,trim($my_annotatedTokens{$i})."\t");
								} else {
									fwrite($fh,"O"."\t");
								}
								if (isset($annotatedTokens{$i})) {
									fwrite($fh,trim($annotatedTokens{$i}));
								} else {
									fwrite($fh,"O");
								}
								
								
								fwrite($fh,"\n");
								
								$i++;
							}
						}
						fwrite($fh,"\n\n");
						$annotatedTokens = array();
					}
			
	
}


function getSysAnnotation($taskid,$sentenceid,$userid,$taskranges){
	#$query = "SELECT output_id,id,type,eval,evalids,sentence_num FROM annotation LEFT JOIN sentence ON output_id=num WHERE user_id=".$userid." AND task_id=".$taskid." AND sentence_num=".$sentenceid." AND eval IN (4, 2, 3) order by output_id,type,eval;";
	$query = "SELECT output_id,id,type,eval,evalids,sentence_num FROM annotation LEFT JOIN sentence ON output_id=num WHERE user_id=".$userid." AND task_id=".$taskid." AND sentence_num=".$sentenceid." order by output_id,type,eval;";
			$my_result_annotation = safe_query($query);
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_num_row = 1;
			$my_annotatedTokens = array();
			while ($my_row_annotation = mysql_fetch_row($my_result_annotation)) {	
					$my_label = $taskranges[$my_row_annotation[3]][0];
					$my_evalitems = preg_split("/,/", $my_row_annotation[4]);
					foreach($my_evalitems as $my_item) {
						$my_tokenitems = preg_split("/ /", $my_item);
						for ($l = 0; $l < count($my_tokenitems); $l++) {
							if ($l==0) {
								if (isset($my_annotatedTokens{$my_tokenitems[$l]})) {
									$my_annotatedTokens{$my_tokenitems[$l]} .= " B-".strtoupper(substr($my_label,0,3));
								} else {
									$my_annotatedTokens{$my_tokenitems[$l]} = " B-".strtoupper(substr($my_label,0,3));
								}
							} else {
								if (isset($my_annotatedTokens{$my_tokenitems[$l]})) {
									$my_annotatedTokens{$my_tokenitems[$l]} .= " I-".strtoupper(substr($my_label,0,3));
								} else {
									$my_annotatedTokens{$my_tokenitems[$l]} = "I-".strtoupper(substr($my_label,0,3));
								}
							}
						} 
					} 
				
				
				
			}
			return $my_annotatedTokens;
	
}
function get($table){
	$query = "SELECT id,overall,PER,ORG,LOC,PRO,THI,CHA,EVE FROM ".$table." order by id;";
	
	$my_result_annotation = safe_query($query);
	$my_tot_row = mysql_num_rows($my_result_annotation);
	$arr=array();
	while ($my_row_annotation = mysql_fetch_row($my_result_annotation)) {
		$arr['labels'] .=  "\"Train n".$my_row_annotation[0]."\",";
		$arr['vals'] .=  $my_row_annotation[1].",";
		$arr['valsP'] .=  $my_row_annotation[2].",";
		$arr['valsO'] .=  $my_row_annotation[3].",";
		$arr['valsL'] .=  $my_row_annotation[4].",";
	}
	return $arr;	
}
function getGoldAnnotationSave($taskid,$sentenceid,$userid,$taskranges){
	#$query = "SELECT output_id,id,type,eval,evalids,sentence_num FROM annotationsave LEFT JOIN sentence ON output_id=num WHERE user_id=".$userid." AND task_id=".$taskid." AND sentence_num=".$sentenceid." AND eval IN (4, 2, 3) order by output_id,type,eval;";
	$query = "SELECT output_id,id,type,eval,evalids,sentence_num FROM annotationsave LEFT JOIN sentence ON output_id=num WHERE user_id=".$userid." AND task_id=".$taskid." AND sentence_num=".$sentenceid." order by output_id,type,eval;";
			$my_result_annotation = safe_query($query);
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_num_row = 1;
			$my_annotatedTokens = array();
			while ($my_row_annotation = mysql_fetch_row($my_result_annotation)) {	
					$my_label = $taskranges[$my_row_annotation[3]][0];
					$my_evalitems = preg_split("/,/", $my_row_annotation[4]);
					foreach($my_evalitems as $my_item) {
						$my_tokenitems = preg_split("/ /", $my_item);
						for ($l = 0; $l < count($my_tokenitems); $l++) {
							if ($l==0) {
								if (isset($my_annotatedTokens{$my_tokenitems[$l]})) {
									$my_annotatedTokens{$my_tokenitems[$l]} .= " B-".strtoupper(substr($my_label,0,3));
								} else {
									$my_annotatedTokens{$my_tokenitems[$l]} = " B-".strtoupper(substr($my_label,0,3));
								}
							} else {
								if (isset($my_annotatedTokens{$my_tokenitems[$l]})) {
									$my_annotatedTokens{$my_tokenitems[$l]} .= " I-".strtoupper(substr($my_label,0,3));
								} else {
									$my_annotatedTokens{$my_tokenitems[$l]} = "I-".strtoupper(substr($my_label,0,3));
								}
							}
						} 
					} 
				
				
				
			}
			return $my_annotatedTokens;
	
}

function saveIOB2File ($intDir, $taskid, $userid="") {
	$taskinfo = getTaskInfo($taskid);	
	$taskname=$taskinfo["name"];
	$tasktype=$taskinfo["type"];
	$taskranges = rangesJson2Array($taskinfo["ranges"]);
	
	$tasksyscount=countTaskSystem ($taskid);
	$query_clause = "";
	if (isset($userid) && $userid != "") {
		$query_clause = "AND user_id=$userid";
	}
	
	//count the number of annotators for the current task
	$query = "select distinct user_id from annotation LEFT JOIN sentence ON annotation.output_id=sentence.num WHERE task_id=$taskid $query_clause";
	$result_annotators = safe_query($query);
	$annotators_count = mysql_num_rows($result_annotators);
	#print "TASK: ". $taskid . " ($query) annotations:".$annotators_count."<br>";
		
	if ($annotators_count > 0) {	
		//loop on users
		while ($row = mysql_fetch_row($result_annotators)) {
			$userid=$row[0];
			$sentence_done = getDoneSentences($taskid,$userid);
			$filecsv = $intDir.$taskname."_ann".$userid.".iob2";
			$fh=fopen($filecsv,"w"); 
			
			//save comments
			/*
			$comments = getComments($taskid,$userid);	
			if (count($comments) > 0) {
				$filecsv_comment = $intDir.$taskname."_comment".$userid.".csv";
				$fh_comment=fopen($filecsv_comment,"w"); 
				fwrite($fh_comment,"ID\ttype\tcomment\n");
				while (list ($sentnum,$comment) = each($comments)) {
					//if (in_array($sentnum,$sentence_done)) {
						fwrite($fh_comment,	str_replace("&quot;","\"",$comment)."\n");
					//}
				}
				fclose($fh_comment); 				
			}
			*/
			
			$query = "SELECT output_id,id,type,eval,evalids,sentence_num FROM annotation LEFT JOIN sentence ON annotation.output_id=sentence.num WHERE user_id=".$userid." AND task_id=".$taskid." order by output_id,type,eval;";
			$result_annotation = safe_query($query);
			$last_id="";		
			$tot_row = mysql_num_rows($result_annotation);
			$num_row = 1;
			$annotatedTokens = array();
			while ($row_annotation = mysql_fetch_row($result_annotation)) {	
				if ($num_row == $tot_row) {
					$label = $taskranges[$row_annotation[3]][0];
					$evalitems = preg_split("/,/", $row_annotation[4]);
					foreach($evalitems as $item) {
						$tokenitems = preg_split("/ /", $item);
						for ($l = 0; $l < count($tokenitems); $l++) {
							if ($l==0) {
								if (isset($annotatedTokens{$tokenitems[$l]})) {
									$annotatedTokens{$tokenitems[$l]} .= " B-".strtoupper(substr($label,0,3));
								} else {
									$annotatedTokens{$tokenitems[$l]} = " B-".strtoupper(substr($label,0,3));
								}
							} else {
								if (isset($annotatedTokens{$tokenitems[$l]})) {
									$annotatedTokens{$tokenitems[$l]} .= " I-".strtoupper(substr($label,0,3));
								} else {
									$annotatedTokens{$tokenitems[$l]} = "I-".strtoupper(substr($label,0,3));
								}
							}
						} 
					} 
				}
					
				if ($last_id != $row_annotation[1] || $num_row == $tot_row) {
					if (count($annotatedTokens) > 0) {
						#get source data
						$query = "SELECT lang,text,tokenization FROM sentence WHERE task_id=$taskid AND id='".$last_id."' AND type='source'";
						$result_source = safe_query($query);
						$row_source = mysql_fetch_row($result_source);
				
						#mantengo gli a capo
						$tokens = getTokens($row_source[0], preg_replace("/\n/"," __BR__ ", $row_source[1]), $row_source[2]);	
						fwrite($fh, "# FILE: ".$last_id."\n");
						
						$i=1;
						foreach ($tokens as $token) {
							if ($token == "__BR__") {
								fwrite($fh,"\n");
							} else {
								fwrite($fh,xml_escape($token)."\t");
								if (isset($annotatedTokens{$i})) {
									fwrite($fh,trim($annotatedTokens{$i})."\n");
								} else {
									fwrite($fh,"O\n");
								}
								$i++;
							}
						}
						fwrite($fh,"\n\n");
						$annotatedTokens = array();
					}
					$last_id = $row_annotation[1];
				}
				
				if (!in_array($row_annotation[5],$sentence_done)) {
					$annotatedTokens = array();
					continue;
				}
			
				$label = $taskranges[$row_annotation[3]][0];
				$evalitems = preg_split("/,/", $row_annotation[4]);
				foreach($evalitems as $item) {
					$tokenitems = preg_split("/ /", $item);
					for ($l = 0; $l < count($tokenitems); $l++) {
						if ($l==0) {
							if (isset($annotatedTokens{$tokenitems[$l]})) {
								$annotatedTokens{$tokenitems[$l]} .= " B-".strtoupper(substr($label,0,3));
							} else {
								$annotatedTokens{$tokenitems[$l]} = " B-".strtoupper(substr($label,0,3));
							}
						} else {
							if (isset($annotatedTokens{$tokenitems[$l]})) {
								$annotatedTokens{$tokenitems[$l]} .= " I-".strtoupper(substr($label,0,3));
							} else {
								$annotatedTokens{$tokenitems[$l]} = "I-".strtoupper(substr($label,0,3));
							}
						}
					} 
				} 
					
				$num_row++;	
			}
		}	
	}
}


function exportTaskCSV ($taskid) {
	$intDir="/tmp";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	$date = date('Ymd_his', time());
	
	$intDir .= "/mtequal_".$taskid."_".$date."/";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}

	saveCSVFile($intDir, $taskid);
	
	$filezip = "/tmp/mtequal-CSV_$date.zip";
	
	$zip = new ZipArchive();
	if($zip->open($filezip, ZIPARCHIVE::CREATE)!==TRUE){
		print "ERROR! Sorry ZIP creation failed.";
	}
	$files= scandir($intDir);
	//var_dump($files);
	//unset($files[0],$files[1]);
	foreach ($files as $file) {
		#print "ADD to zip: $file<br>";
		if ($file != "." && $file != "..") { 
			if (isset($userid) && $userid != null) {
				$zip->addFile($intDir.$file,"mtequal_CSV_".$userid."-".$date."/".$file);
			} else {
  				$zip->addFile($intDir.$file,"mtequal_CSV-".$date."/".$file);
  			}
  		}    
	}
	$zip->close();

	if (file_exists($filezip)) {
		#print $filezip . " (" . file_exists($filezip) .")";
		readfile($filezip);
		unlink($filezip);
	}
	deleteDirectory($intDir);
	exit(0);
}

function exportTaskXML ($taskid) {
	$intDir="/tmp";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	$date = date('Ymd_his', time());
	#$intDir =$_SERVER['DOCUMENT_ROOT'] ."/mtequal_".$date."/";
	$intDir .= "/mtequal";
	
	$query_clause = " status='annotator'";
	if (isset($userid) && $userid != null) {
		$query_clause = " user.id='".$userid."'";
		$intDir .= "_".$userid;	
	}
	$intDir .= "_".$date."/";	
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	
	saveXMLFile($intDir, $taskid);
	
	$filezip = "/tmp/mtequal-XML_$date.zip";
	
	$zip = new ZipArchive();
	if($zip->open($filezip, ZIPARCHIVE::CREATE)!==TRUE){
		print "ERROR! Sorry ZIP creation failed.";
	}
	$files= scandir($intDir);
	//var_dump($files);
	//unset($files[0],$files[1]);
	foreach ($files as $file) {
		#print "ADD to zip: $file<br>";
		if ($file != "." && $file != "..") { 
			if (isset($userid) && $userid != null) {
				$zip->addFile($intDir.$file,"mtequal_XML_".$userid."-".$date."/".$file);
			} else {
  				$zip->addFile($intDir.$file,"mtequal_XML-".$date."/".$file);
  			}
  		}    
	}
	$zip->close();

	if (file_exists($filezip)) {
		#print $filezip . " (" . file_exists($filezip) .")";
		readfile($filezip);
		
		unlink($filezip);
	}
	deleteDirectory($intDir);
	exit(0);	
}

function exportTaskIOB2 ($taskid) {
	$intDir="/tmp";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	$date = date('Ymd_his', time());
	#$intDir =$_SERVER['DOCUMENT_ROOT'] ."/mtequal_".$date."/";
	$intDir .= "/mtequal";
	
	#$query_clause = " status='annotator'";
	if (isset($taskid) && $taskid != "") {
		#$query_clause = " user.id='".$userid."'";
		$intDir .= "_".$taskid;	
	}
	$intDir .= "_".$date."/";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}

	saveIOB2File($intDir, $taskid);
	$filezip = "/tmp/mtequal-IOB2_$date.zip";
	
	$zip = new ZipArchive();
	if($zip->open($filezip, ZIPARCHIVE::CREATE)!==TRUE){
		print "ERROR! Sorry ZIP creation failed.";
	}
	$files= scandir($intDir);
	//var_dump($files);
	//unset($files[0],$files[1]);
	foreach ($files as $file) {
		#print "ADD to zip: $file<br>";
		if ($file != "." && $file != "..") { 
			if (isset($userid) && $userid != null) {
				$zip->addFile($intDir.$file,"mtequal_IOB2_".$userid."-".$date."/".$file);
			} else {
  				$zip->addFile($intDir.$file,"mtequal_IOB2-".$date."/".$file);
  			}
  		}    
	}
	$zip->close();

	if (file_exists($filezip)) {
		#print $filezip . " (" . file_exists($filezip) .")";
		readfile($filezip);
		unlink($filezip);
	}
	deleteDirectory($intDir);
	exit(0);	
}

function exportCSV ($userid) {
	$intDir="/tmp";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	$date = date('Ymd_his', time());
	#$intDir =$_SERVER['DOCUMENT_ROOT'] ."/mtequal_".$date."/";
	$intDir .= "/mtequal_".$userid."_".$date."/";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	
	$tasks = getTasks($userid);
	while (list ($taskid,$arrinfo) = each($tasks)) {
		saveCSVFile($intDir, $taskid, $userid);
	} 
				
	$filezip = "/tmp/mtequal-CSV_$date.zip";
	
	$zip = new ZipArchive();
	if($zip->open($filezip, ZIPARCHIVE::CREATE)!==TRUE){
		print "ERROR! Sorry ZIP creation failed.";
	}
	$files= scandir($intDir);
	//var_dump($files);
	//unset($files[0],$files[1]);
	foreach ($files as $file) {
		#print "ADD to zip: $file<br>";
		if ($file != "." && $file != "..") { 
			if (isset($userid) && $userid != null) {
				$zip->addFile($intDir.$file,"mtequal_CSV_".$userid."-".$date."/".$file);
			} else {
  				$zip->addFile($intDir.$file,"mtequal_CSV-".$date."/".$file);
  			}
  		}    
	}
	$zip->close();

	if (file_exists($filezip)) {
		#print $filezip . " (" . file_exists($filezip) .")";
		readfile($filezip);
		unlink($filezip);
	}
	deleteDirectory($intDir);
	exit(0);
	
}

#save XML files
function exportXML ($userid) {
	$intDir="/tmp";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	$date = date('Ymd_his', time());
	#$intDir =$_SERVER['DOCUMENT_ROOT'] ."/mtequal_".$date."/";
	$intDir .= "/mtequal";
	
	$query_clause = " status='annotator'";
	if (isset($userid) && $userid != null) {
		$query_clause = " user.id='".$userid."'";
		$intDir .= "_".$userid;	
	}
	$intDir .= "_".$date."/";	
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	
	$tasks = getTasks($userid);
	while (list ($taskid,$arrinfo) = each($tasks)) {
			saveXMLFile($intDir, $taskid, $userid);
	} 	
			
	$filezip = "/tmp/mtequal-XML_$date.zip";
	
	$zip = new ZipArchive();
	if($zip->open($filezip, ZIPARCHIVE::CREATE)!==TRUE){
		print "ERROR! Sorry ZIP creation failed.";
	}
	$files= scandir($intDir);
	//var_dump($files);
	//unset($files[0],$files[1]);
	foreach ($files as $file) {
		#print "ADD to zip: $file<br>";
		if ($file != "." && $file != "..") { 
			if (isset($userid) && $userid != null) {
				$zip->addFile($intDir.$file,"mtequal_XML_".$userid."-".$date."/".$file);
			} else {
  				$zip->addFile($intDir.$file,"mtequal_XML-".$date."/".$file);
  			}
  		}    
	}
	$zip->close();

	if (file_exists($filezip)) {
		#print $filezip . " (" . file_exists($filezip) .")";
		readfile($filezip);
		
		unlink($filezip);
	}
	deleteDirectory($intDir);
	exit(0);	
}

### PRESENTATION FUNCTION ###
function showAlignedSentence ($task, $lang, $text, $type = "", $tokenize = 0, $idx = "", $hashErrors = array(),$colorRange = array()) {
	global $languages;
	$tokens = getTokens($lang, $text, $tokenize);
	$hashTokenidErrortype = array();
	if (count($tokens) > 0) {
		while (list ($errID, $errARRAY) = each($hashErrors)) {
			if (!empty($errARRAY[0])) {
			  	$tokids = preg_split("/,/", $errARRAY[0]);
			  	#print "<br><li>$errID TOKIDS: ". $errARRAY[0]."<br>";
				
			  	//loop on each segment
				foreach ($tokids as $tids) {
					if (trim($tids) == "") {
						continue;
					}
					#print "<li>$errID TIDS: ". $tids."<br>";
				
					$span = preg_split("/ /", $tids);
					# loop on each token and spaces to check the max already set layer
					foreach ($span as $tid) {
						if (!isset($hashTokenidErrortype{$tid})) {
							$hashTokenidErrortype{$tid} = array();
						} 						
						$hashTokenidErrortype{$tid}{$errID} = $colorRange{$errID}[1];
					}
					
				} 
			}	
		}
	}
		
	$mouseaction = " onmouseover=\"this.className='blackborderb'\" onmouseout=\"this.className='whiteborderb'\"";
	$text = "";
	$id=1;
	foreach ($tokens as $token) {
		if ($token == "__BR__") {
			$text .= "<br>";
		} else {
			$text .= "<div id='$idx.$id' class='token'><nobr $mouseaction>$token</nobr>";
			$borderTopStyle = "";
			$borderBottomStyle = "";		
			if (isset($hashTokenidErrortype{$id})) {
				while (list ($errID, $color) = each($hashTokenidErrortype{$id})) {
					if ($errID == 2) {
						$borderBottomStyle = "border-bottom: 4px solid #$color";
					} else if ($errID == 1) {
						//$backgroundStyle = "heigth: 4px; background: #$color;";
						$borderTopStyle = "border-top: 4px solid #$color; ";
					}
				}
			}
			$text .= "<div id='b$idx.$id' style='$borderTopStyle $borderBottomStyle'></div></div>";
			
			if ($tokenize != 3) {
				$text .= "<div class='token'>&nbsp;</div>";
			}
		}
		$id++;
	}
		
	$html="<div class='cell $type";
	if (isset($languages[$lang][2]) && $languages[$lang][2] == "rtl") {
		$html.=" rtl";
	}
	$html.="'>$text</div>";
	return $html;
}
 
function showSentence ($task, $lang, $text, $type = "", $tokenize = 0, $idx = "", $hashErrors = array(), $colorRange = array()) {
	global $languages;
	$mouseaction = " onmouseover=\"this.className='blackborderb'\" onmouseout=\"this.className='whiteborderb'\"";
	if ($type == "output") {
		$tokens = getTokens($lang, $text, $tokenize);
		$hashTokenidErrortype = array();
		$max_level=0;
		if (count($tokens) > 0) {
			while (list ($errID, $errARRAY) = each($hashErrors)) {
			  if (!empty($errARRAY[0])) {
			  	$tokids = preg_split("/,/", $errARRAY[0]);
			  	#print "<br><li>$errID TOKIDS: ". $errARRAY[0]."<br>";
				
			  	//loop on each segment
				foreach ($tokids as $tids) {
					if (trim($tids) == "") {
						continue;
					}
					#print "<li>$errID TIDS: ". $tids."<br>";
				
					$span = preg_split("/ /", $tids);
					# loop on each token and spaces to check the max already set layer
					$level=0;
					foreach ($span as $tid) {
						if (!isset($hashTokenidErrortype{$tid})) {
							$hashTokenidErrortype{$tid} = array();
						} else if (count($hashTokenidErrortype{$tid}) > $level) {
							$level = count($hashTokenidErrortype{$tid});
						}
					}
					if ($level > $max_level) {
						$max_level = $level+1;
					}	
					reset($span);	
					foreach ($span as $tid) {
						for ($l=count($hashTokenidErrortype{$tid}); $l < $level; $l++) {
							array_push($hashTokenidErrortype{$tid}, "FFF");
						}
						$hashTokenidErrortype{$tid}[$level] = $colorRange{$errID}[1];						
					}
				}
			  } 
			}	
		}
	
		$text = "";
		$id=1;
		foreach ($tokens as $token) {
			if ($token == "__BR__") {
				$text .= "<br>";
			} else {
				#$text .= "<div id='$idx.$id' $tokenbg><nobr>$token";
				$text .= "<div id='$idx.$id' class='token'><nobr $mouseaction>$token</nobr>";
				if (isset($hashTokenidErrortype{$id})) {
					foreach ($hashTokenidErrortype{$id} as $col) {
						$text .= "<div id='b$idx.$id' style='background: #". $col ."; border-bottom: 1px solid ";
						if ($col != "FFF") {
							$text .= "#888";
						} else {
							$text .= "#FFF";
						}
						$text .= "; height: 4px'></div>";
					}
					if (count($hashTokenidErrortype{$id})<$max_level) {
						$text .= "<div style='background: #FFF; height: ". (($max_level-count($hashTokenidErrortype{$id}))*5) ."px'></div>";
  					}
  					unset($hashTokenidErrortype{$id});
					
				} else {
					$text .= "<div id='b$idx.$id' style='height: ". ($max_level*5) ."px; background: #FFF;'></div>";
				}
				$text .= "</div>";
				
				if ($tokenize != 3) {
					$spaceId=$id."-".($id+1);
					$text .= "<div id='$idx.".$spaceId."' class='token'><nobr ";
					if ($task != "wordaligner") {
						$text .= $mouseaction;
					}
					$text .= ">&nbsp;</nobr>";
					if (isset($hashTokenidErrortype{$spaceId})) {
						foreach ($hashTokenidErrortype{$spaceId} as $col) {
							$text .= "<div style='background: #". $col ."; font-size: 1px; border-bottom: 1px solid ";
							if ($col != "FFF") {
								$text .= "#888";
							} else {
								$text .= "#FFF";
							}
							$text .= "; height: 4px'></div>";
						}
						if (count($hashTokenidErrortype{$spaceId})<$max_level) {
							$text .= "<div style='background: #FFF; height: ". (($max_level-count($hashTokenidErrortype{$spaceId}))*5) ."px'></div>";
  						}
						unset($hashTokenidErrortype{$spaceId});
					} else {
						$text .= "<div style='background: #FFF; height: ". ($max_level*5) ."px'></div>";
					}
					$text .= "</div>";
				}
				$id++;
			}
		}
	}
	
	$html="<div class='cell $type";
	if (isset($languages[$lang][2]) && $languages[$lang][2] == "rtl") {
		$html.=" rtl";
	}
	$html.="'>$text</div>";
	return $html;
}

function strToHex($string){
    $obj = array_map(function($x){return "&#". strval(ord($x)) . ";";},str_split($string));
	return implode($obj);
}

/*
#other special character
<char id="32" hexcode="0020" desc="SPACE"/>
<char id="8202" hexcode="200a" desc="HAIR SPACE"/>
<char id="160" hexcode="00A0" desc="NO-BREAK SPACE"/>
<char id="9" hexcode="0009" desc="TABULAR SPACE"/>
<char id="10" hexcode="000A" desc="LINE FEED"/>
<char id="13" hexcode="0x0d" desc="CARRIAGE RETURN"/>
*/
function isSpace ($ch) {
	if ($ch == " " || strToHex($ch) == "&#32;") {
		return true;
	}
	return false;
}
  
function isPunctuation ($ch) {
 	if (preg_match("/[\!|\?|\"|\'|\-|\/|\$|,|:|;|\.|\(|\)|\[|\]|\{|\}]/",$ch)) {
 		return true;
 	}
 	return false;
}


# tokenization values:
# 0: NO
# 1: YES, using spaces only
# 2: YES, using spaces and punctuations
# 3: YES, character by character	
function getTokens  ($lang, $text, $tokenization = 2) {
	$text = preg_replace("/<\/b>/","<#b>", $text);
    $text = preg_replace("/<\/i>/","<#i>", $text);
    $tokens=array();
	if ($tokenization == 0) {
		array_push($tokens, trim($text));
	} else {
		preg_match_all('/./u', trim($text), $tokenized_text);
		if ($tokenization == 3) {
			foreach ($tokenized_text[0] as $ch) {
				if (trim($ch) != "") {
					array_push($tokens, $ch);
				}
			}
		} else {
			$token="";
			foreach ($tokenized_text[0] as $ch) {            
				if (($tokenization==1 && isSpace($ch)) || 
					($tokenization==2 && (isSpace($ch) || isPunctuation($ch)))) {
					if (strlen($token) > 0) {
						array_push($tokens, preg_replace("/<#b>/","</b>",preg_replace("/<#i>/","</i>",$token)));
						$token="";
					}
					if (trim($ch) != "") {
						array_push($tokens, $ch);
					}
				} else {
					$token .="$ch";
				}	
			}
			if (strlen($token) > 0) {
				array_push($tokens, preg_replace("/<#b>/","</b>",preg_replace("/<#i>/","</i>",$token)));					
			}
		}
	}
	return $tokens;
}	
	

#$date = "2009-03-04 17:45";
#$result = nicetime($date); // 2 days ago
function niceTime($date) {
	if(empty($date)) {
        return "No date provided";
    }
    
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60","60","24","7","4.35","12","10");
    
    $now = time();
    $unix_date = strtotime($date);
    
       // check validity of date
    if(empty($unix_date)) {    
        return "Bad date";
    }

    // is it future date or past date
    if($now > $unix_date) {    
        $difference = $now - $unix_date;
        $tense = "ago";
        
    } else {
        $difference = $unix_date - $now;
        $tense = "from now";
    }
    
    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
        $difference /= $lengths[$j];
    }
    
    $difference = round($difference);
    
    if($difference != 1) {
        $periods[$j].= "s";
    }
    
    return "$difference $periods[$j] {$tense}";
}

/*
 * return an array whose elements are shuffled in random order.
 */
function shuffle_assoc($list,$id) { 
  if (!is_array($list)) return $list; 

  $keys = array_keys($list); 
  //shuffle($keys); 
  //each user will be a difference random output ordered
  seoShuffle($keys,$id);
  $random = array(); 
  foreach ($keys as $key) 
    $random[$key] = $list[$key]; 

  return $random; 
} 

#the same randomized result each time that list is generated.
function seoShuffle(&$items,$seed) { 
    mt_srand($seed); 
    for ($i = count($items) - 1; $i > 0; $i--){ 
        $j = @mt_rand(0, $i); 
        $tmp = $items[$i]; 
        $items[$i] = $items[$j]; 
        $items[$j] = $tmp; 
    } 
} 

/*
 * Logging
 */
#saveLog("PASSO! $eval $source_id,$target_id,$user_id,$action\n");
function saveLog($line) {
	if (SAVELOG == 1) {
		$time = date( "d/m/Y H:m:s", time() );

		$myFile = "/tmp/mtequal.log";
		$fh = fopen($myFile, 'a') or die("can't open file");
		fwrite($fh, "$time\t$line\n");
		fclose($fh);
	}
}

#generic function for Mysql query
function safe_query ($query = "") {
    global $mysession;
    if (empty($query)) {
		return FALSE;
    }
	$errorno=0;
    if (QUERY_LOG == "yes" && substr(strtolower(trim($query)),0, 6) != "select") {
		$querylog = addslashes($query);
    	$querylog = "INSERT INTO log (user_id, query, error, lasttime) VALUES (".$mysession['userid'].",\"$querylog\",\"$errorno\",now());";
		mysql_query($querylog) or die ("Error! " . mysql_error());
    }    
    //print 	$query."<br>\n";
    $result = mysql_query($query) or $errorno= mysql_errno();
    if ($errorno != 0) {
		if (QUERY_DEBUG == "no") {
		    print ("<BR>Query failed: please contact the webmaster " . SYSADMIN . ".");
		} else {
			$error = mysql_error();	
	    	print ("</td></tr></table></td></tr></table><BR>Query failed:" 
					      . "<li> errorno=" . $errorno
					      . "<li> error=" . $error
					      . "<b><li> query=" . $query
					      . "<p><a href=\"javascript:history.go(-1)\"> Back</a>");
			return 0;
		}	
    } 
    return $result;
}

//Delete folder function 
function deleteDirectory($dir) { 
    if (!file_exists($dir)) return true; 
    if (!is_dir($dir) || is_link($dir)) return unlink($dir); 
    foreach (scandir($dir) as $item) { 
    if ($item == '.' || $item == '..') continue; 
		if (!deleteDirectory($dir . "/" . $item)) { 
        	chmod($dir . "/" . $item, 0777); 
	  		if (!deleteDirectory($dir . "/" . $item)) return false; 
    	}; 
    }
    return rmdir($dir); 
} 
    
function xml_escape($s) {
	$s = str_replace("&quot;","\"",$s);
	$s = htmlspecialchars($s, ENT_QUOTES, 'UTF-8', false);
    $s = html_entity_decode($s, ENT_QUOTES, 'UTF-8');
    return trim($s);
}

function chr_utf8($code) { 
        if ($code < 0) return false; 
        elseif ($code < 128) return chr($code); 
        elseif ($code < 160) // Remove Windows Illegals Cars 
        { 
            if ($code==128) $code=8364; 
            elseif ($code==129) $code=160; // not affected 
            elseif ($code==130) $code=8218; 
            elseif ($code==131) $code=402; 
            elseif ($code==132) $code=8222; 
            elseif ($code==133) $code=8230; 
            elseif ($code==134) $code=8224; 
            elseif ($code==135) $code=8225; 
            elseif ($code==136) $code=710; 
            elseif ($code==137) $code=8240; 
            elseif ($code==138) $code=352; 
            elseif ($code==139) $code=8249; 
            elseif ($code==140) $code=338; 
            elseif ($code==141) $code=160; // not affected 
            elseif ($code==142) $code=381; 
            elseif ($code==143) $code=160; // not affected 
            elseif ($code==144) $code=160; // not affected 
            elseif ($code==145) $code=8216; 
            elseif ($code==146) $code=8217; 
            elseif ($code==147) $code=8220; 
            elseif ($code==148) $code=8221; 
            elseif ($code==149) $code=8226; 
            elseif ($code==150) $code=8211; 
            elseif ($code==151) $code=8212; 
            elseif ($code==152) $code=732; 
            elseif ($code==153) $code=8482; 
            elseif ($code==154) $code=353; 
            elseif ($code==155) $code=8250; 
            elseif ($code==156) $code=339; 
            elseif ($code==157) $code=160; // not affected 
            elseif ($code==158) $code=382; 
            elseif ($code==159) $code=376; 
        } 
        if ($code < 2048) return chr(192 | ($code >> 6)) . chr(128 | ($code & 63)); 
        elseif ($code < 65536) return chr(224 | ($code >> 12)) . chr(128 | (($code >> 6) & 63)) . chr(128 | ($code & 63)); 
        else return chr(240 | ($code >> 18)) . chr(128 | (($code >> 12) & 63)) . chr(128 | (($code >> 6) & 63)) . chr(128 | ($code & 63)); 
    } 

// Callback for preg_replace_callback('~&(#(x?))?([^;]+);~', 'html_entity_replace', $str); 
function html_entity_replace($matches) { 
	if ($matches[2]) { 
        return chr_utf8(hexdec($matches[3])); 
    } elseif ($matches[1]) { 
        return chr_utf8($matches[3]); 
    } 
    switch ($matches[3]) { 
        case "nbsp": return chr_utf8(160); 
    	case "iexcl": return chr_utf8(161); 
        case "cent": return chr_utf8(162); 
        case "pound": return chr_utf8(163); 
        case "curren": return chr_utf8(164); 
        case "yen": return chr_utf8(165); 
        //... etc with all named HTML entities 
    } 
    return false; 
} 
    
function htmlentities2utf8_old ($string) { // because of the html_entity_decode() bug with UTF-8 
    $string = preg_replace_callback('~&(#(x?))?([^;]+);~', 'html_entity_replace', $string); 
	return $string; 
} 

function htmlentities2utf8 ($string) {
	return str_replace("\xc2\xa0",' ',$string);
	#return utf8_decode(mb_convert_encoding(str_replace("\xc2\xa0",' ',$string), 'UTF-8', 'HTML-ENTITIES'));
}

?>
