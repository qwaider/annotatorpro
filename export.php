<?php
ini_set('max_execution_time', 1000);

include("config.php");
include("functions.php");
if ($mysession["status"] == "root" || $mysession["status"] == "manager" || $mysession["status"] == "annotator") { 
	$namefile = "annotatorpro_export_";
	if (isset($userid)) {
		$namefile .= "_".$userid;
	} else {
		$userid = null;
	}
	$namefile .= "-".date("Ymd_His");
	
	
	header("Pragma: public");
    header("Expires: -1");
    header("Cache-Control: public, must-revalidate, post-check=0, pre-check=0");
    header("Content-type: application/zip");
	header("Content-Disposition: attachment; filename=\"".$namefile.".zip\"");
	header("Content-Transfer-Encoding: binary");
	
	if (isset($userid)) {
		//export CVD format
		if ($format == "csv") {
			exportCSV($userid);
		} else if ($format == "xml") {
			exportXML($userid);
		} 
	} else if (isset($taskid) && !empty($taskid)) {
		if ($format == "csv") {
			exportTaskCSV($taskid);
		} else if ($format == "xml") {
			exportTaskXML($taskid);
		} else if ($format == "iob2") {
			exportTaskIOB2($taskid);
		} 	else if ($format == "ex") {
			$taskID=$_GET['taskid'];
			$user=$_GET['userid'];
			exportEx($taskid,$user);
		} 	
		
		}
}

function exportEx ($taskid, $userid) {
	$taskinfo = getTaskInfo($taskid);	
	$taskranges = rangesJson2Array($taskinfo["ranges"]);	
	#print "TASK: ". $taskid . " ($query) annotations:".$annotators_count."<br>";
	
	
	$intDir="/tmp";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}
	$date = date('Ymd_his', time());
	#$intDir =$_SERVER['DOCUMENT_ROOT'] ."/mtequal_".$date."/";
	$intDir .= "/annotatorpro";
	
	#$query_clause = " status='annotator'";
	if (isset($taskid) && $taskid != "") {
		#$query_clause = " user.id='".$userid."'";
		$intDir .= "_".$taskid;	
	}
	$intDir .= "_".$date."/";
	if (!is_dir($intDir)) {
		mkdir($intDir, 0777);
	}

	saveIT($intDir, $taskid, $userid,$taskinfo);
	$filezip = "/tmp/annotatorpro_$date.zip";
	
	$zip = new ZipArchive();
	if($zip->open($filezip, ZIPARCHIVE::CREATE)!==TRUE){
		print "ERROR! Sorry ZIP creation failed.";
	}
	$files= scandir($intDir);
	//var_dump($files);
	//unset($files[0],$files[1]);
	foreach ($files as $file) {
		#print "ADD to zip: $file<br>";
		if ($file != "." && $file != "..") { 
			if (isset($userid) && $userid != null) {
				$zip->addFile($intDir.$file,"annotatorpro_".$userid."_".$date."/".$file);
			} else {
  				$zip->addFile($intDir.$file,"annotatorpro_".$date."/".$file);
  			}
  		}    
	}
	$zip->close();

	if (file_exists($filezip)) {
		#print $filezip . " (" . file_exists($filezip) .")";
		readfile($filezip);
		unlink($filezip);
	}
	deleteDirectory($intDir);
	exit(0);	

	
}	
	
	
function saveIT ($filecsv, $taskid, $userid,$taskinfo) {
	
	
		
			$fh=fopen($filecsv,"w"); 
							
						#get source data
			$query = "SELECT lang,text,tokenization,num,id FROM sentence WHERE task_id=$taskid ;";


						$my_result_annotation= safe_query($query);
				
			$my_tot_row = mysql_num_rows($my_result_annotation);
			$my_annotatedTokens = array();
			if($taskinfo['type'] == "quality"){ 
				fwrite($fh,"\"id\",\"file_id\",\"text\",\"tag\"\n");
			}
			while ($row_source = mysql_fetch_row($my_result_annotation)) {	
			   if($taskinfo['type'] == "docann"){ //token level task
						if($header){
							fwrite($fh,"# FILEID: ".xml_escape($row_source[3])."\n");
						}
						echo $row_source[3]."</br>";
						#mantengo gli a capo
						$tokens = getTokens($row_source[0], preg_replace("/\n/"," __BR__ ", $row_source[1]), $row_source[2]);	
						
						$annotatedTokens=getSysAnnotation($taskid,$row_source[3],$userid,$taskranges);
						
						$i=1;
						foreach ($tokens as $token) {
							if ($token == "__BR__") {
								fwrite($fh,"\n");
							} else {
								fwrite($fh,xml_escape($token)."\t");
								
								if (isset($annotatedTokens{$i})) {
									fwrite($fh,trim($annotatedTokens{$i}));
								} else {
									fwrite($fh,"O");
								}
								
								
								fwrite($fh,"\n");
								
								$i++;
							}
						}
						fwrite($fh,"\n\n");
						$annotatedTokens = array();
					}else{
						//document level task
						$taga=getTagDocumentLevel($row_source[3],$taskranges);
						$tre=str_replace('"','\"',$row_source[1]);
						$tre=str_replace(array("\r\n", "\n", "\r"), '\\n', $tre);
						fwrite($fh,"\"".$row_source[4]."\",\"".$row_source[3]."\",\"".$tre."\",\"".$taga."\"\n");
						$taga="";
					}
				}
	fflush($fh);
	fclose($fh);
}

?>
