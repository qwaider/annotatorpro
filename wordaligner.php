<?php
header("Content-type: text/html; charset=utf-8");
?>

<html>
<head>
<link href="css/mtequal.css" rel="styleSheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/mtequal.js"></script>
	
<?php
include("config.php");
include("functions.php");

if (isset($viewer)) {
	$mysession["wordalign_viewer"] = $viewer;
	$_SESSION["mysession"] = $mysession; 	
} else {
	if (isset($mysession) && !isset($mysession["wordalign_viewer"])) {
		$mysession["wordalign_viewer"] = 2;
		$_SESSION["mysession"] = $mysession;
	}
	$viewer = $mysession["wordalign_viewer"];
	
}
if (!isset($sentidx)) {
	$sentidx = -1;
}
	
if (isset($taskid) && isset($mysession) && $mysession["taskid"] != $taskid) {
	$taskinfo = getTaskInfo($taskid);
	$mysession["taskid"] = $taskid;
	$mysession["tasknow"] = $taskinfo["name"];
	$mysession["tasksysnum"] = countTaskSystem($taskid);
	$mysession["tasktype"] = $taskinfo["type"];
	$mysession["taskistr"] = $taskinfo["instructions"];
	$mysession["taskranges"] = rangesJson2Array($taskinfo["ranges"]);
	$_SESSION["mysession"] = $mysession;
	
}

if (!isset($mysession) || $mysession["taskid"]==0 || empty($mysession["status"])) {
	header("Location: index.php");
	#print "<script>window.open('index.php','_self');</script>";
}

$sentence_hash = getSentence($id, $taskid);
if (!isset($sentence_hash["source"])) {
	header("Location: index.php#".($id-1)); 
	exit;
}
?>

<style>
html{height:100%}body{height:100%;min-width:980px;overflow:hidden;font-family:verdana,arial,helvetica;font-size:12px;margin:0;padding:0;}
</style>
</head>

<body>

<div style="background-color: #FFFFFF; z-index:9999; position: absolute; width: 100%; height: 100%; border-right: 1px solid #222; border-left: 1px solid #222">
<table cellpadding="0" cellspacing="0" height="100%" width="100%">
<tr height="1%">
<td style="top:0; width:100%">

<?php
include("menu_sentence.php");
print "<div style='position: absolute; top: 2px; left: 15px'>";
if ($viewer == 1) {
	print "<a href='wordaligner.php?viewer=2&id=$id&sentidx=$sentidx&taskid=$taskid'><img src='img/list.png' width=34px title='list mode' alt='Display list'></a>";
} else {
	print "<a href='wordaligner.php?viewer=1&id=$id&sentidx=$sentidx&taskid=$taskid'><img src='img/grid.png' width=35px title='matrix mode' alt='Display matrix'></a>";
}
print "</div>";

$monitoring=0;
if (isset($userid) && $userid != $mysession['userid'] && ($mysession["status"] == "root" || $mysession["status"] == "manager" || $mysession["status"] == "annotator")) {
	$time = date( "d/m/Y H:m:s", time() );
	print "<div style='display: inline-block; background: yellow; border: dashed #777 1px; border-radius: 0px 0px 15px 15px;  padding: 9px; font-size:12px; position:absolute; top: 0px; margin-left: 320px; z-index:1000'>Monitoring... sentence <b>$id</b>, user: <b>$userid</b> ($time)<br><a href='admin.php?section=annotation#user$userid' style='float:right'>« Back to Admin</a></div><br>";
	$monitoring=1;
	$sentidx=-1;
} else {
	if (isset($mysession['userid'])) {
		$userid = $mysession['userid'];
		if (!isset($taskid)) {
 			$taskid = $mysession["taskid"];
 		}
		
	} else {
		print "<br><font color=red>Access denied!</font> You are an unregistered user or your session has expired. Please <a href='index.php' target='_top'>login</a> again!";
		return;
	} 
}


if ($mysession["taskistr"] != "") {
?>

<span style="float: right; padding-right: 20px; padding-top: 9px; width:20%;">
<div style='float: right; right: 0px; top:0px; display: inline-block; position: fixed;text-align: left; background: #eee; font-size: 12px; padding-top: 10px; padding-left: 10px; padding-right: 10px; padding-bottom: 10px; border: solid #999 1px; border-radius: 0px 0px 0px 15px; z-index: 1000'>
		<button href="#collapse1" class="nav-toggle" style='float: right; margin-top: -4px;'>read more</button><div style="float: right; margin-right: 20px">Task instructions<br></div>
		<div id="collapse1" style="display:none; font-size: 14px;">
		<br><br>
		<?php print $mysession["taskistr"]; ?>
		</div>	
</div>
</span>

<?php
}

    if ($viewer == 1) {
    	print "<div style='display: block; width: 100%; float: left; left: 0px; margin-top: 5px'><div class=label>SOURCE: </div>" . showSentence("wordaligner", $sentence_hash["source"][0], $sentence_hash["source"][1], "source")."<div>";
    }
	if (isset($sentence_hash["reference"])) {
		print "<div class=labelref>REFERENCE: </div>" . showSentence ("wordaligner", $sentence_hash["reference"][0], $sentence_hash["reference"][1], "reference")."<div>";;
	}
?>
 
  </div>
								</div>
							</td>
						</tr>
						<tr>
							<td valign=top>
							<div style='display: inline-block; box-shadow: 3px -5px 5px #888; position: relative;  margin-bottom: 5px; width: 100%; height: 6px; '>
							</div>
							<iframe src="wordalign_output<?php echo $viewer; ?>.php?id=<?php echo $id; ?>&taskid=<?php echo $taskid; ?>&userid=<?php echo $userid; ?>&sentidx=<?php echo $sentidx; ?>&monitoring=<?php echo $monitoring; ?>" style="border: 0px; padding-left: 0px; margin-top: -10px; width:100%; height:100%"></iframe> 							</td>
						</tr>
					</table>
                    
				</div>

<?php
if (isset($userid) && $userid != $mysession['userid'] && ($mysession["status"] == "root" || $mysession["status"] == "manager" || $mysession["status"] == "annotator")) {
	print "<script>\n  setTimeout(\"window.open('wordaligner.php?id=$id&userid=$userid&taskid=$taskid','_self')\", 5000);\n</script>\n";
}
?>			
</body>
</html>
