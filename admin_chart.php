<?php 
$arr= getDBChartAW();
?>
<!doctype html>
<html>
	<head>
		<title>Line Chart</title>
		<script src="js/Chart.min.js"></script>
	</head>
	<body>
		
				<canvas id="canvas" width="600" height="180"></canvas>
				<!--<b align="center">F1 chart</b>-->
		


	<script>
		var lineChartData = {
				
			labels : [<?php echo $arr['labels']; ?>],
			datasets : [
				{
					label: "Overall",
					fillColor : "rgba(191, 28, 196,0.2)",
					strokeColor : "rgba(191, 28, 196,1)",
					pointColor : "rgba(191, 28, 196,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [<?php echo $arr['vals']; ?>]
				}
				
			],
			
			

		};


			
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		/*window.myLine = new Chart(ctx).Line(lineChartData, {
			responsive: true
    		});*/
		
		
		window.myLine = new Chart(ctx, {
    type: 'line',
    data: lineChartData,
    options: {
        responsive: true,
        scales: {
            xAxes: [{
               scaleLabel: {
        		display: true,
        		labelString: '# Documents'
            }}],
            
            yAxes: [{
      scaleLabel: {
        display: true,
        labelString: 'F1 (%)'
      },
      ticks: {
                        min: 0,
                        beginAtZero: true
                    }
    }]
        }
    }
});

	}


	</script>
	</body>
</html>
