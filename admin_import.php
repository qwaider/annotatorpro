

<?php
//add annotations
$errmsg="";
if (!empty($mysession["status"]) && $mysession["status"] == "admin") {
$log = "";	
if (isset($taskid) && !empty($taskid) && isset($userid) && !empty($userid)) {
	//get task configuration
	$taskinfo = getTaskInfo($taskid);
	$ranges = rangesJson2Array($taskinfo["ranges"]);

  	$insert=0;
  	$ftmp = $_FILES['filecsv']['tmp_name'];
	if (!empty($ftmp)) {
		$oname = basename($_FILES['filecsv']['name']);
		if (file_exists($ftmp)) {
		  $mappingsID2NUM = getSourceSentenceIdMapping($taskid);
		  //print "TASK: $taskid, TYPE: $type, mappingsID2NUM: ". join(",",$mappingsID2NUM). "<br>";

		  //TODO controllare che non ci siano già delle annotationi
		  $annotatedTask = getAnnotatedTasks ($userid);
		  if (!isset($annotatedTask[$taskid]) || $annotatedTask[$taskid]==0) {
			# metto a 0 le frasi senza annotatione
			$query ="SELECT linkto, num FROM sentence LEFT JOIN annotation ON sentence.num=annotation.output_id WHERE task_id=$taskid AND (user_id is null OR user_id!=$userid) AND type != 'source' AND type != 'reference'";
			$result = safe_query($query);
			while($row = mysql_fetch_row($result)) {
				print "Set default ".$row[0].",".$row[1]."<br>";
				saveQuality($row[0],$row[1],$userid,"0");
			}

			$handle = fopen($ftmp, "r");
			if ($handle) {
				$linenum = 0;
				while (($line = fgets($handle)) !== false) {
    				$linenum++;
    				$query = "";
        			#print $line ."<br><pre>".htmlentities2utf8($line)."</pre><br>----<br>";
    				$items = explode("\t", htmlentities2utf8($line));
    				if (count($items) < 4 || empty($items[1])) {
    					$errmsg = "WARNING! Parse error on file $oname: the language is missing.<br>\n[line: $linenum] $line\n";
    					break;
    				}
    				$sourceId=$mappingsID2NUM{$items[0]};
    				$outputId = getOutputSentence($sourceId,$items[1]);
    				$eval = "-1";
    				reset($ranges);
    				$subErrorType = strtolower(substr($items[2], 0,5));
    				while (list ($ev,$evarr) = each($ranges)) {
    					#print $evarr[0].",".$items[2] ." == " .strpos(strtolower($evarr[0]),$subErrorType)  ."<br>";
						if (strpos(strtolower($evarr[0]),$subErrorType) !== FALSE) {
							$eval = $ev;
							break;
						}
					}
					$trg_sentence = getSentence($outputId , $taskid);
					$tokens = getTokens ($trg_sentence[$items[1]][0], $trg_sentence[$items[1]][1], 1);
					if (count($tokens) >= intval($items[3]) && $eval != -1) {
						#print "#".$items[0]. " " .$items[2]. ": ".$outputId." ".$trg_sentence[$items[1]][1]. " -- ".(intval($items[3])-1)."/" .count($tokens)."<br>";
						#print "-- ".$tokens[intval($items[3])-1]  . "<br>";
						saveErrors($sourceId,$outputId,$userid,$eval,trim($items[3]),$tokens[intval($items[3])-1]);
					} else {
						print "ERROR! The info at line $linenum are not valid ($line)<br>";
						print "#".$items[0]. " " .$items[2]. ": ".$outputId." ".$trg_sentence[$items[1]][1]. " -- ".(intval($items[3])-1)."/" .count($tokens)." -- ".$tokens[intval($items[3])-1]  . "<br>";
					}
				}							
    		  }
    		  fclose($handle);
    		} else {
    			print "<script>alert('WARNING! The user has already annotated this task! This action can corrupt his annotation.');</script>";
    		}
		} else {
    		// error opening the file.
    		$errmsg = "ERROR! Some problems occured opening the file $oname.";
		}
		if ($errmsg == "" || $insert == count($mappingsID2NUM)) {
    		$errmsg = "DONE! $insert sentences have been inserted";
    	}
	} else {
		$errmsg = "ERROR! Uploaded file hasn't been parsed correctly.";
	} 
	#print "Uploading... taskid: $taskid, type: $type<br>\n"; #.$_FILES['filecsv']['tmp_name']. 
}


	print "<form action=\"admin.php?section=import\" method=\"post\" id=uploadform enctype=\"multipart/form-data\">";
	print "Choose a user: <select name='userid'><option value=''>\n";
	$userlist = getUserStats($mysession["userid"],$mysession["status"]);
	while (list ($uid,$uarr) = each($userlist)) {
		print "<option value='$uid'";
		if (isset($id) && $id == $uid) {
			print " selected";
		} 
		print "> &nbsp;".$uarr[1]." - \n";
	}	
	print "</select>";


	print "<br>Choose a task: <select name='taskid'><option value=''>";
	$tasks = getTasks(null);
	$ttype = "";
	while (list ($tid,$tarr) = each($tasks)) {
		if ($tarr[1] != $ttype) {
			$ttype = $tarr[1];
			print "<option value='' disabled='disabled'>--- ".ucfirst($ttype) ." tasks --- \n";
		}
		print "<option value='$tid'";
		if (isset($id) && $id == $tid) {
			print " selected";
		} 
		print "> &nbsp;".$tarr[0]."\n";
	}	
print "</select>";

 ?>

 <br><br>
 Import annotation from CSV file <font size=-1><i>(size max. <?php echo (int)(ini_get('upload_max_filesize')); ?>Mb)</i></font>:<br>
 <input type="file" id="filecsv" name="filecsv"> </br><br>
 
  <input type="submit" name="Upload" value="Upload">	
  <input type="button" onclick="javascript:hideUpload();" value="Cancel">
</form>

<?php 
} else {
	print "Warning! You don't have enought privileges.";
}
?>
