
<?php
set_time_limit(0);
include("config.php");
include("functions.php");

$agreementReport="";
$annotators=array();
$hash_report = getAnnotationReport3vs1($id,$mysession["userid"]);
while (list ($eval,$counters) = each($hash_report)) {
	$values = explode(",", $counters);
	foreach ($values as $val) {
		$items = explode(" ", $val);
		if (!in_array($items[1],$annotators)) {
			array_push($annotators, $items[1]);
		}
	}
}		
$hash_sure=array();
$hash_possible=array();

foreach ($annotators as $annotator) {
    $userinfo = getUserInfo($annotator);
    //if (preg_match("/gold/i",$userinfo["name"])) {       
        foreach ($annotators as $annotator2) {
        if ($annotator != $annotator2) {
            $hash_sure[$annotator] = array();
            $hash_sure[$annotator2] = array();


            //get sure alignments
            $query = "SELECT output_id FROM annotation LEFT JOIN sentence ON sentence.num=annotation.sentence_num WHERE sentence.annotators like '%".$uid."%' and ( LENGTH(sentence.annotators) - LENGTH(REPLACE(sentence.annotators, ',', '')) between 3 and 3 ) and sentence.gold = 'N' and finished = 'Y' and task_id=$id AND (user_id=$annotator OR user_id=$annotator2) AND eval=1 GROUP BY output_id HAVING COUNT(*) = 2";
            $result = safe_query($query);
            if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_row($result)) {
                    $query = "SELECT user_id,evalids FROM annotation WHERE output_id=".$row[0]." AND eval=1 AND (user_id=$annotator OR user_id=$annotator2)";
                    $result2 = safe_query($query);
                    while($row2 = mysql_fetch_row($result2)) {
                        foreach (preg_split("/[,| ]/",$row2[1]) as $tid) {
                            array_push($hash_sure[$row2[0]], $row[0]."-".$tid);
                        }
                    }
                }
            }

            $hash_possible[$annotator] = $hash_sure[$annotator];
            $hash_possible[$annotator2] = $hash_sure[$annotator2];
            //get possible alignments
            $query = "SELECT output_id FROM annotation LEFT JOIN sentence ON sentence.num=annotation.sentence_num WHERE sentence.annotators like '%".$uid."%' and ( LENGTH(sentence.annotators) - LENGTH(REPLACE(sentence.annotators, ',', '')) between 3 and 3 ) and sentence.gold = 'N' and finished = 'Y' and task_id=$id AND (user_id=$annotator OR user_id=$annotator2) AND eval=2 GROUP BY output_id HAVING COUNT(*) = 2";
            $result = safe_query($query);
            if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_row($result)) {
                    $query = "SELECT user_id,evalids FROM annotation WHERE output_id=".$row[0]." AND eval=2 AND (user_id=$annotator OR user_id=$annotator2)";
                    $result2 = safe_query($query);
                    while($row2 = mysql_fetch_row($result2)) {
                        foreach (preg_split("/[,| ]/",$row2[1]) as $tid) {
                            array_push($hash_possible[$row2[0]], $row[0]."-".$tid);
                        }
                    }
                }
            }

            //count the error rate
            /*print "<br>S_$annotator:" .count($hash_sure[$annotator]) .", [".join(" ",$hash_sure[$annotator]) ."]";
            print ", S_$annotator2:" .count($hash_sure[$annotator2]).", [".join(" ",$hash_sure[$annotator2]) ."]";
            print "<br>P_$annotator:" .count($hash_possible[$annotator]).", [".join(" ",$hash_possible[$annotator]) ."]";
            print "P_$annotator2:" .count($hash_possible[$annotator2]).", [".join(" ",$hash_possible[$annotator2]) ."]";*/


            $sure_intersect = array_intersect($hash_sure[$annotator],$hash_sure[$annotator2]);
            $possible_intersect = array_intersect($hash_possible[$annotator],$hash_possible[$annotator2]);


            $err_rate = (((count($sure_intersect) + count($possible_intersect)) / (count($hash_sure[$annotator]) + count($hash_possible[$annotator2])))) * 100;
            $userinfo = getUserInfo($annotator);
            $userinfo2 = getUserInfo($annotator2);
            
            $agreementReport .= "<li>User <b>{$userinfo['username']}</b> <u>vs.</u> <b>{$userinfo2['username']}</b>: <b><font color=red>". number_format($err_rate, 2, ',', ' ') . "</font></b>";
            $agreementReport .= "<a title='(S1∩S2 + P1∩P2)/(S1+P2) = (".count($sure_intersect)." + ".count($possible_intersect).") / (".count($hash_sure[$annotator])." + ".count($hash_possible[$annotator2]).")'>%</a>";

	break;
        }
     }
   // }
    break;
}
if ($agreementReport != "") {
	print '<table style="border: 1px solid #000; background: #fff"><tr bgcolor=#ccc><td align=center>Agreement:</td></tr><tr><td>';
	print $agreementReport;
	print '</td></tr></table>';
} else {
	print "No agreement!";
}
?>
