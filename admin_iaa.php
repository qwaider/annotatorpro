<style>
.tbl {height: 7px; 
	border-top: 1px solid #000; 
	width: 100%}
.tokend {display: inline-block; 
		font-size: 16px; 
		border-top: 2px solid #999; 
		border-bottom: 1px solid #000; 
		border-left: 1px solid #000; 
		border-right: 1px solid #000; 
		margin-bottom: 1px}
		
.tok {display: inline-block; 
		font-size: 16px; 
		border-top: 2px solid #999; 
		border-bottom: 1px solid #000; 
		border-left: 1px solid #000; 
		margin-bottom: 1px}
</style>
<script>
function getAgreement(taskid) {
	$('.spinner').show();
	$.ajax({
		url: 'admin_iaa_server.php',
		type: 'GET',
		data: {id: taskid},
		async: true,
		cache:false,
		crossDomain: false,
		success: function(response) {
			$('.spinner').hide();
			$("#agreement").html(response);
			
		},
  		error: function(response, xhr,err ) {
			alert("WARNING! An error occured on the server.");
		}
  	});
}
</script>
<div style='margin: 10px; padding-bottom: 30px; vertical-align: top; top: 0px; display:block'>
<form action="admin.php?section=iaa" method=GET>
<input type=hidden name=section value="iaa" />
Choose a task: <select onChange="submit()" name='id'><option value=''>
<?php


$show_disagreement=0;
if (isset($mysession)) { 
	
	$tasks = getTasksDocumentLevel($mysession["userid"]);
	
	$ttype = "";
	while (list ($tid,$tarr) = each($tasks)) {
		if ($tarr[1] != $ttype) {
			$ttype = $tarr[1];
			print "<option value='' disabled='disabled'>--- ".$taskTypes[$ttype] ." tasks --- \n";
		}
		print "<option value='$tid'";
		if (isset($id) && $id == $tid) {
			print " selected";
		} 
		print "> &nbsp;".$tarr[0]."\n";
	}	
}
?>
</select>
</form>

<?php
$wordaligner_color = array(
"04B404", "0080FF","DBA901","DF013A","BF00FF",
"04B404", "0080FF","DBA901","DF013A","BF00FF");
$wordaligner_lightcolor =array(
"01DF01","81BEF7","F7D358","F7819F","D0A9F5",
"01DF01","81BEF7","F7D358","F7819F","D0A9F5");

$anntot=array();
$annid="";
$annotators=array();
$statsTable = "";
$done=0;
if (isset($id)) {
	$taskinfo = getTaskInfo($id);
	
	if (count($taskinfo) > 0) {
		
		//call js to get the page
		?>
		<div id="agreement"></div>
		<script type="text/javascript">
  		  getAgreement(<?php echo $id; ?>);
		</script>
		<?php
	} else {
		print "WARNING! This task is not valid.";
	}	
}
?>
</div>

<script>
function colorSourceTokens(sentenceId, tokens) {
	var tokenArray = tokens.split(/(\s+)/);
	for(var i=0; i < tokenArray.length; i++){
		el = document.getElementById("s."+sentenceId+"."+tokenArray[i]);
    	if (el != null) {
    		el.style.backgroundColor = "#F78181";
    	}
    }
}

function cleanSourceBackground(sentenceId, tokencounter) {
	for(var c=0; c<tokencounter; c++) {
		el = document.getElementById("s."+sentenceId+"."+c);
      	if (el != null) {
      		el.style.backgroundColor = "#fff";
      	}
	}
}
</script>
